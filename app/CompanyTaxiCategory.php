<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CompanyTaxiCategory extends Model {
  protected $table = 'company_taxi_category';
  protected $fillable = ['company_Id', 'category_id'];
}
