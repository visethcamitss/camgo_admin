<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class WalletLog extends Model {
  protected $table = 'wallet_log';
  protected $fillable = ['id','invoice_id','taxi_id','update_value','remain_value','actor','created_at','updated_at','is_delete'];
}	

