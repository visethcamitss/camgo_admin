<?php
/**
 * Created by PhpStorm.
 * User: nsp
 * Date: 9/28/2017
 * Time: 11:33 AM
 */

namespace App\Traits;


trait HasRoles
{

    /**
     * A user may have multiple roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role','user_roles','user_id','role_id');
    }
    public function hasRole($name)
    {
        $rs = $this->roles;
        if(is_array($name)){
            foreach($name as $roleName){
                $hasRole = $this->hasMyRole($roleName,$rs);
                if($hasRole){
                    return true;
                }
            }
            return false;
        }
        return $this->hasMyRole($name,$rs);
    }

    public function routePermission($routeName)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($routeName)) {
                return true;
            }
        }
        return false;
    }

    private function hasMyRole($slug,$roles = array())
    {
        foreach($roles as $role){
            if($role->name == $slug){
                return true;
            }
        }
        return false;
    }
}