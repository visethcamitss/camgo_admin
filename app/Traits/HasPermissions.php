<?php
/**
 * Created by PhpStorm.
 * User: nsp
 * Date: 9/28/2017
 * Time: 12:59 PM
 */

namespace App\Traits;


trait HasPermissions
{
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_roles','role_id','user_id');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'role_permissions');
    }

    public function hasPermission($name,$permissions = array())
    {
        if(count($permissions) == 0){
            $permissions = $this->permissions;
        }
        if(is_array($name)){
            foreach($name as $pName){
                $hasPermission = $this->hasPermission($pName,$permissions);
                if($hasPermission){
                    return true;
                }
            }
        }else{
            foreach($permissions as $p){
                if($p->name == $name){
                    return true;
                }
            }
        }
        return false;
    }
    public function givePermissionTo(...$permissions)
    {
        $permissions = collect($permissions)
            ->flatten()
            ->all();
        $this->permissions()->saveMany($permissions);

        return $this;
    }
    public function revokePermission()
    {
        $this->permissions()->detach();

        return $this;
    }

    public function syncPermissions(...$permissions)
    {
        $this->permissions()->detach();

        return $this->givePermissionTo($permissions);
    }
}