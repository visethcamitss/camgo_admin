<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiCategoryFaire extends Model
{
    protected $table = 'taxi_category_fare';
    protected $fillable = ['category_id','location','title', 'initial_fare', 'per_minute_fee', 'per_km_fee', 'flag_down_fee','pt_start','pt_end','pt_start_two','pt_end_two','pt_initial_fare', 'pt_per_minute_fee', 'pt_per_km_fee', 'pt_flag_down_fee','distance','center_lat','center_long'];
    public $timestamps = false;
}
