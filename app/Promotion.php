<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model{
    protected $table = 'promotion';
    protected $fillable = ['name','code','active','value','description','start_date','expire_date'];
}


 ?>
