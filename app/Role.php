<?php
/**
 * Created by PhpStorm.
 * User: nsp
 * Date: 9/28/2017
 * Time: 11:36 AM
 */

namespace App;


use App\Traits\HasPermissions;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasPermissions;

    protected $table = 'roles';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];
}