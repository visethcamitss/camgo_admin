<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Userx extends Model {
  protected $table = 'user';
  protected $fillable = ['name','company_id','user_type','avatar', 'phone', 'status', 'verify_status', 'email', 'travel_status', 'request_taxi', 'last_lat', 'last_long', 'fail_counter',  'score', 'pass_code'];
}
