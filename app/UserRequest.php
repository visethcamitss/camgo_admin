<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model {
  protected $table = 'user_request_data';

  public function invoice()
  {
	return $this->hasOne('App\Invoice', 'request_id', 'request_id');
  }

  public function user()
  {
	return $this->belongsTo('App\Userx', 'user_id', 'user_id');
  }

  public function taxi()
  {
	return $this->hasOne('App\Taxi', 'taxi_id', 'taxi_id');
  }

  public function taxi_category()
  {
  	return $this->hasOne('App\TaxiCategory','id','category_id');
  }

  public function travel_log()
  {
  	return $this->hasMany('App\TravelLog','request_id','request_id');
  }
}
