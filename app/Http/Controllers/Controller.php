<?php
namespace App\Http\Controllers;

error_reporting(0);
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
    function getWallet($params, $url){
        $url = curl_init('http://localhost:8089/api/userx/'.$url);
		curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url, CURLOPT_POSTFIELDS, $params);
		curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($url);
		return $result;
	}
}
