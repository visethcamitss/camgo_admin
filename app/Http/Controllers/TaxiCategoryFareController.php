<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\TaxiCategoryFaire;
use Redirect;
use Session;

class TaxiCategoryFareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function edit($id){
        $data['res'] = TaxiCategoryFaire::where('id', $id)->first();
		return view('taxi_categories/edit_fare', $data);
    }
	public function update($id){
		$db = TaxiCategoryFaire::find($id);
		$data = Request::except(['_token']);
		$db->fill($data)->save();
		return  redirect()->route('categories.edit', $db->category_id);
	}
    public function create($id){
        $data['category_id'] = $id;
        return  view('taxi_categories/create_fare',$data);
    }
    public function store(){
        $data = Request::except(['_token']);
        TaxiCategoryFaire::create($data);
        Session::flash('flash_message', 'Successfully added!');
        return  redirect()->route('categories.edit', $data['category_id']);
    }

}
