<?php

namespace App\Http\Controllers;

use Request;
use App\Userx;
use Session;
use Validator;
use Redirect;
use Auth;
use App\UserRequest;
use DB;
use App\Invoice;
use App\Company;

class UserxController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $res = Userx::where('id', '>', 0)->orderBy('id', 'DESC');
    	$data['keyword'] = $keyword = Request::get('keyword');
    	if($keyword){
            $res->where('name', 'LIKE', '%'.$keyword.'%');
            $res->orWhere('phone', 'LIKE', '%'.$keyword.'%');
            if(is_numeric($keyword)){
                $res->orWhere('id',$keyword);
            }
        }
		$user_status = Request::input('user_status');
		if($user_status > 0){
			if($user_status == 1){
				$res -> whereRaw('travel_status = 1 or travel_status = 3 ');
			}else if($user_status == 2){
				$res-> where('travel_status',2);
			}
		}
    	$data['res'] = $res->paginate(100);
        return view('userxs/index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $data['id'] = $id;
        $data['res'] = Userx::where('id', $id)->first();
        $requests = UserRequest::with('user')->with('taxi')->where('user_id', $data['res']->user_id)->with('taxi_category')->with('invoice')->orderBy('id', 'DESC')->get();
        $data['req_status'] = array(
                                0=>array("N/A"),
                                1=> array("Ignored", 'red'),
                                2=> array("others accepted", 'red'),
                                3=>array("Picking up", 'blue'),
                                4=>array("User canceled", 'red'),
                                5=>array("Driver canceled", 'red'),
                                6=>array("On ride", 'blue'),
                                7=>array("Completed", 'blue'),
                                8=>array("Waiting user", 'blue'),
                                9=>array("Rejected", 'red'));
        $invoice = [];
        if(count($requests) > 0) {
            $invoice = $requests->filter(function ($req, $key) {
                return !empty($req->invoice);
            });
        }
        $data['user_requests'] = $requests;
        $data['invoices'] = $invoice;

        //$data['wallet'] = json_decode($this->getWallet(['user_id' => $data['res']->user_id], 'taxi_get_balance'));
        return view('userxs/show', $data);
    }

    function logout(){
        Auth::logout();
        return redirect('/login');
    }

    public function edit($id) {
        $data['res'] = Userx::find($id);
		$data['company'] = Company::all();
        return view('userxs/edit', $data);
    }

    public function update($id) {
        $db = Userx::find($id);
        $data = Request::except(['_token']);
        if (Request::file('avatar') && Request::file('avatar') -> isValid()) {
            $destinationPath = 'uploads';
            // upload path
            $extension = Request::file('avatar') -> getClientOriginalExtension();
            // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension;
            // renameing image
            Request::file('avatar') -> move($destinationPath, $fileName);
            // uploading file to given path
            $data['avatar'] = $fileName;
        } else {
            unset($data['avatar']);
        }

        $db -> fill($data) -> save();
        Session::flash('flash_message', 'Successfully updated!');
        return Redirect::to('/userxs');
    }
}
