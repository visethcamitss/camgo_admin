<?php

namespace App\Http\Controllers;

use Request;
use App\Setting;
use Session;
use Validator;
use Redirect;

class SettingController extends Controller
{
	public function __construct()
  {
    $this->middleware('auth');
  }

	public function index()
  {
    $res = Setting::where('active', '>=',0);
    $data['res'] = $res->paginate(50);

		return view('settings/index', $data);
  }

	public function edit($id)
  {
		$data['res'] = Setting::find($id);
		return view('settings/edit', $data);
  }

	public function update($id)
	{
		$db = Setting::find($id);
		$data = Request::except(['_token']);
		$rule = [
				'name' => 'required',
				'value' => 'required'
		];
		$v = Validator::make($data, $rule);
		if ($v->fails()) {
				return redirect()->back()->withErrors($v->errors());
		}

		$db->fill($data)->save();

		Session::flash('flash_message', 'Successfully updated!');

		return Redirect::to('/settings');
	}
}
