<?php

namespace App\Http\Controllers;

use App\Setting;
use Request;
use App\Taxi;
use Session;
use Validator;
use Redirect;
use App\UserRequest;
use App\Company;
use App\TaxiCategory;
use App\Wallet;
use Image;
use Route;
use DB;

class TaxiController extends Controller {
	public function __construct() {
		$this -> middleware('auth')->except(['set_taxi_to_offline']);
	}
	public function index() {
		$res = Taxi::select('taxi.*', 'taxi_category.category_name as category');
		$res -> join('taxi_category', 'taxi_category.id', '=', 'taxi.category_id');
		// $res -> leftJoin('company','taxi.company_id', '=', 'company.id');
		$res -> orderBy('priority', 'desc');
		$data['keyword'] = $keyword = Request::get('keyword');
		if ($keyword) {
		    $res->where(function ($query) use($keyword){
                $query->where('driver_name', 'LIKE', '%' . $keyword . '%');
                $query ->orWhere('driver_phone', 'LIKE', '%' . $keyword . '%');
            });
        }
        $taxi_status = Request::input('taxi_status',0);
		$taxi_os = Request::input('taxi_os',0);
		if($taxi_status > 0){
		    if($taxi_status == 1){
                $res->where('serving_status',1);
            }elseif($taxi_status == 2){
                $res->where('serving_status','!=',1);
            }
            elseif($taxi_status == 3){
                $res->where('taxi.status',1);
            }
            elseif($taxi_status == 4){
                $res->where('taxi.status','!=',1);
            }
            elseif($taxi_status == 5){
                $res->where('reviewed',1);
            }
            elseif($taxi_status == 6){
                $res->where('reviewed','!=',1);
            }
            elseif($taxi_status == 7){
                $res->where('priority','<',0);
            }
            elseif($taxi_status == 8){
				 $res->where(function ($query){
				 	 $query->whereRaw('last_login <= DATE_SUB(NOW(), INTERVAL 5 DAY)')->orWhereNull('last_login');
				 });
            }
            elseif($taxi_status == 9){
                $res->where('job_status','!=',3);
            }
        }
        if($taxi_os > 0 ){
		    if($taxi_os == 1){
                $res->whereRaw('CHAR_LENGTH(t_taxi.uuid) > 30');
            }elseif($taxi_os == 2){
                $res->whereRaw('CHAR_LENGTH(t_taxi.uuid) < 30');
            }else{
                $setting = Setting::select(['value']);
                if($taxi_os == 3){
                    $setting->where('code','U_LATEST_IOS');
                }else{
                    $setting->where('code','U_LATEST_ANDROID');
                }
                $setting = $setting->first();
                if(!empty($setting)){
                    $app_version = $setting->value;
                    $app_data = '$.appversion';
                    if($taxi_os == 4){
                        $app_data = '$.data.appversion';
                    }
                    $res->whereRaw('CAST(JSON_EXTRACT(`device_info`,"'.$app_data.'") as DECIMAl(3,1)) <'.$app_version);
                }
            }
        }
		$data['res'] = $res -> paginate(100);

		return view('taxis/index', $data);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		
		$this->createWallet('','driver_register');
		$data['id'] = $id;
		$data['res'] = Taxi::where('id', $id)->first();

		 $data['wallet'] = json_decode($this->getWallet(['user_id' => $data['res']->taxi_id], 'taxi_get_balance'));

		// $data['company'] = Company::find($data['res'] -> company_id) -> name;
		$data['category'] = TaxiCategory::find($data['res'] -> category_id) -> category_name;

		$requests = UserRequest::with('user') -> where('taxi_id', $data['res'] -> taxi_id) -> with('taxi_category') -> with('invoice') -> orderBy('id', 'DESC') -> get();
		
		$data['req_status'] = array(0 => array("N/A"), 1 => array("Ignored", 'red'), 2 => array("others accepted", 'red'), 3 => array("Picking up", 'blue'), 4 => array("User canceled", 'red'), 5 => array("Driver canceled", 'red'), 6 => array("On ride", 'blue'), 7 => array("Completed", 'blue'), 8 => array("Waiting user", 'blue'), 9 => array("Rejected", 'red'));
		$invoice = [];
		if(count($requests) > 0) {
			$invoice = $requests->filter(function ($req, $key) {
				return !empty($req->invoice);
			});
		}
		$data['invoices'] = $invoice;
		$data['user_requests'] = $requests;
		return view('taxis/show', $data);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		/// $data['companies'] = Company::where('active', 1) -> get();
		$data['categories'] = TaxiCategory::where('is_delete',2) -> get();
		$milli = explode(" ", microtime())[1];

		$id = sprintf('%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
		//$data['default_passcode'] = "5107c594902c5a5cb39e61379f602d902a0471be";

		$data['taxi_id'] = $id . $milli;
		return view('taxis/create', $data);
	}

	public function store() {

		$data = Request::except(['_token','driver_avatar','taxi_image','driver_social_id','driver_licence']);
		$rule = ['category_id' => 'required', 'taxi_id' => 'required', 'driver_phone' => 'required|unique:taxi'];
		$v = Validator::make($data, $rule);

		if ($v->fails()) {
			return redirect()->back()->withErrors($v->errors());
		}
		$data['job_status'] = 1;
		$data['serving_status'] = 1;
		$data['status'] = 1;
		// register taxi
		$id = Taxi::create($data) -> id;
		
		if($id > 0){
			$data = [];
			$uploadPath = env('UPLOAD_PATH');
			$current_date = date('Ym');
			$uploadPath = env('UPLOAD_PATH');
			@mkdir($uploadPath.'/'.$current_date);
			$current_date .= '/'.$id;
			@mkdir($uploadPath.'/'.$current_date);
			$uploadPath = $uploadPath.'/'.$current_date; 

			if (Request::file('driver_avatar') && Request::file('driver_avatar') -> isValid()) {
				// upload path
				$extension = Request::file('driver_avatar') -> getClientOriginalExtension();
				// getting image extension
				$driver_avatar = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_avatar') -> move($uploadPath, $driver_avatar);
				// uploading file to given path
			}
			if($driver_avatar==null){
				$data['driver_avatar'] = 'images/noimage.gif';
			}else{
				$data['driver_avatar'] = $current_date.'/'.$driver_avatar;
			}
	
			if (Request::file('taxi_image') && Request::file('taxi_image') -> isValid()) {
				// upload path
				$extension = Request::file('taxi_image') -> getClientOriginalExtension();
				// getting image extension
				$taxi_image = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('taxi_image') -> move($uploadPath, $taxi_image);
				// uploading file to given path
			}
			if($taxi_image==null){
				$data['taxi_image'] = 'images/default_taxi.png';
			}else{
				$data['taxi_image'] = $current_date.'/'.$taxi_image;
			}
	
			if (Request::file('driver_social_id') && Request::file('driver_social_id') -> isValid()) {
		
				$extension = Request::file('driver_social_id') -> getClientOriginalExtension();
				// getting image extension
				$driver_social = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_social_id') -> move($uploadPath, $driver_social);
				// uploading file to given path
			}
			if($driver_social==null){
				$data['driver_social_id'] = 'images/no_id_card.jpg';
			}else{
				$data['driver_social_id'] = $current_date.'/'.$driver_social;
			}
	
			if (Request::file('driver_licence') && Request::file('driver_licence') -> isValid()) {
				$extension = Request::file('driver_licence') -> getClientOriginalExtension();
				// getting image extension
				$driver_licence = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_licence') -> move($uploadPath, $driver_licence);
				// uploading file to given path
			}
			if($driver_licence==null){
				$data['driver_licence'] = 'images/no_driving_licence.jpg';
			}else{
				$data['driver_licence'] =  $current_date.'/'.$driver_licence;
			}

			if(count($data) > 0){
				$db = Taxi::find($id);
				$db -> fill($data) -> save();
			}
			Session::flash('flash_message', 'Successfully added!');
		}

		$this->createWallet($db, 'driver_register');
		
		return Redirect::to('/taxis');
	}

	function createWallet($param_data,$url){
		$url = "http://localhost:8089/api/userx/driver_register";
        $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Basic d2FsbGV0X3VzZXI6d2FsbGV0X3Bhc3N3b3Jk']]);
        $res = $client->request('POST', $url, [
                'form_params' => [
                         'user_id' => $param_data->taxi_id,
                                        'phone' => $param_data->driver_phone,
                                        'fullname' => $param_data->driver_name,
                                        'uuid' => '123456',
                                        'password' => 'itsumo@www',
                                        'email' => "",
                                        'language' => "english",
                                        'balance' => 0,
                                        'user_type' => 1
   		 ]
	]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		/// $data['companies'] = Company::all();
		$data['categories'] = TaxiCategory::all();
		$data['res'] = Taxi::select('taxi.*') -> where('taxi.id', $id) -> first();
		//$data['res'] = Taxi::find($id)->with('wallet');

		return view('taxis/edit', $data);
	}

	public function update($id) {
		$db = Taxi::find($id);
		$data = Request::except(['_token']);
		$create_at = $db->created_at;

		if(!empty($db)){
			$uploadPath = env('UPLOAD_PATH');
			$current_date = date('Ym', strtotime($create_at));
			$uploadPath = env('UPLOAD_PATH');
			@mkdir($uploadPath.'/'.$current_date);
			$current_date .= '/'.$id;
			@mkdir($uploadPath.'/'.$current_date);
			$uploadPath = $uploadPath.'/'.$current_date;

			if (Request::file('driver_avatar') && Request::file('driver_avatar') -> isValid()) {
				// upload path
				$extension = Request::file('driver_avatar') -> getClientOriginalExtension();
				// getting image extension
				$driver_avatar = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_avatar') -> move($uploadPath, $driver_avatar);
				// uploading file to given path
				$data['driver_avatar'] = $current_date.'/'.$driver_avatar;

				$image = Image::make($uploadPath . '/' . $driver_avatar) -> resize(256, 256, function($constraint) {
					$constraint -> upsize();
				});
				$image -> save($uploadPath . '/' . $driver_avatar);

			} else {
				unset($data['driver_avatar']);
			}
			if (Request::file('taxi_image') && Request::file('taxi_image') -> isValid()) {
				// upload path
				$extension = Request::file('taxi_image') -> getClientOriginalExtension();
				// getting image extension
				$fileName = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('taxi_image') -> move($uploadPath, $fileName);
				// uploading file to given path
				$data['taxi_image'] = $current_date.'/'.$fileName;
				$image = Image::make($uploadPath . '/' . $fileName) -> resize(825, 510, function($constraint) {
					$constraint -> upsize();
				});
				$image -> save($uploadPath . '/' . $fileName);
			} else {
				unset($data['taxi_image']);
			}
			if (Request::file('driver_social_id') && Request::file('driver_social_id') -> isValid()) {
				// upload path
				$extension = Request::file('driver_social_id') -> getClientOriginalExtension();
				// getting image extension
				$fileName = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_social_id') -> move($uploadPath, $fileName);
				// uploading file to given path
				$data['driver_social_id'] = $current_date.'/'.$fileName;
				$image = Image::make($uploadPath . '/' . $fileName) -> resize(825, 510, function($constraint) {
					$constraint -> upsize();
				});
				$image -> save($uploadPath . '/' . $fileName);
			} else {
				unset($data['driver_social_id']);
			}
			if (Request::file('driver_licence') && Request::file('driver_licence') -> isValid()) {
				// upload path
				$extension = Request::file('driver_licence') -> getClientOriginalExtension();
				// getting image extension
				$fileName = rand(11111, 99999) . '.' . $extension;
				// renameing image
				Request::file('driver_licence') -> move($uploadPath, $fileName);
				// uploading file to given path
				$data['driver_licence'] = $current_date.'/'.$fileName;
				$image = Image::make($uploadPath . '/' . $fileName) -> resize(825, 510, function($constraint) {
					$constraint -> upsize();
				});
				$image -> save($uploadPath . '/' . $fileName);
			} else {
				unset($data['driver_licence']);
			}

			// $data['service_charge'] = 	$data['service_charge'] / 100;
			$db -> fill($data) -> save();
			Session::flash('flash_message', 'Successfully updated!');
		}
		return Redirect::to('/taxis');
	 }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$db = Taxi::find($id);
		$db -> status = 2;
		$db -> save();
		return redirect() -> back();
	}

	public function live() {
		$res = Taxi::select('taxi.*');
		$res -> orderBy('taxi.id', 'desc');
		$res -> where('taxi.status', '=', 1);

		$action = Request::get('status');
		if ($action == 1) {
			$res -> where('serving_status', 1);
			$res -> where('priority', '>', 0);
			$res -> where('reviewed', 1);
			$res -> where('job_status', '!=', 3);
		}elseif ($action == 2) {
			$res -> whereIn('serving_status', [2,3]);
		}elseif ($action == 3) {
			$res -> where('job_status', '=', 3);
		}elseif ($action == 4) {
			$res -> where('priority', '<', 0);
		}

		$data['res'] = $res -> get();

		$rest1 = Taxi::select('id');
		$rest1 -> where('taxi.job_status', '!=', 3);
		$rest1 -> where('taxi.status', '=', 1);
		$rest1 -> where('serving_status', '=', 1);
		$rest1 -> where('reviewed', '=', 1);
		$rest1 -> where('offline_request', 1);
		$rest1 -> whereRaw('TIME_TO_SEC(TIMEDIFF(NOW() ,t_taxi.updated_at)) <= 600');
		$rest1 -> whereRaw('t_taxi.priority > 0 OR ( TIME_TO_SEC(TIMEDIFF(NOW() ,t_taxi.last_login)) <= 600 AND priority <= 0 ) ');
		$data['res1'] = $rest1 -> get() -> count();

		$rest2 = Taxi::select('id');
		$rest2 -> where('taxi.job_status', '!=', 3);
		$rest2 -> where(function($query){
				$query->where('serving_status','=',2)
				->orWhere('serving_status','=',3);
		})->get();
		$data['res2'] = $rest2 ;
		$data['number_busy']=$data['res2']-> count();

		$rest3 = Taxi::select('id');
		$rest3 -> where('taxi.status', '=', 1);
		$rest3 -> where('taxi.job_status', '=', 3);
		$data['res3'] = $rest3 -> get() -> count();

		$rest4 = Taxi::select('id');
		$data['res4'] = $rest4 -> get() -> count();

		$data['res5'] = $data['res4'] - ($data['res1'] + $data['number_busy'] + $data['res3']);

		return view('taxis/live', $data);
	}

	public function set_taxi_to_offline()
    {
        Taxi::where('priority','>',0)
            ->update(['priority' => DB::raw('priority * -1')]);
	}
}
