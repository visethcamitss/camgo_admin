<?php
namespace App\Http\Controllers;
use Request;
use App\UserRequest;
use Session;
use Validator;
use Redirect;
use App\Userx;
use App\Taxi;
use App\TaxiCategory;

class UserRequestController extends Controller {
	public function __construct() {
		$this -> middleware('auth');
	}
	public function index() {
		$data['keyword'] = $keyword = Request::get('keyword');
		$data['request_status'] = $request_satus = Request::get('request_satus');
		$res = UserRequest::select('user_request_data.*', 'company.name as company_name') -> where('is_deleted', '>', 1) -> orderBy('id', 'DESC') -> with('user') -> with('taxi') -> with('invoice') -> with('taxi_category');
		$res -> leftJoin('company', 'user_request_data.is_company_pay', '=', 'company.id');
		$res1 = clone $res;
		$data['res1'] = $res1->whereDate('request_date',date('Y-m-d'))->whereIn('request_status',[3,6,8])->limit(100)->get();
		if ($request_satus){
			$res -> where('request_status', $request_satus);
		}
		if ($keyword) {
			$res -> where(function($res) use ($keyword) {
				$res -> where('taxi_id', $keyword);
				$res -> orWhereHas('taxi', function($query) use ($keyword) {
					$query -> where('driver_name', 'LIKE', '%' . $keyword . '%');
				});
				$res -> orWhereHas('user', function($query) use ($keyword) {
					$query -> where('name', 'LIKE', '%' . $keyword . '%');
				});
			});
		}
		$data['req_status'] = array(0 => array("N/A"), 3 => array("Picking up", 'blue'), 4 => array("User canceled", 'red'), 5 => array("Driver canceled", 'red'), 6 => array("On ride", 'blue'), 7 => array("Completed", 'blue'), 8 => array("Waiting user", 'blue'),);
		$data['res'] = $res -> paginate(50);
		$date = date_create(date('Y-m-d'),timezone_open('Asia/Phnom_Penh'));
		$data['daily_report'] = $this->getDailyReport($date);
		$data['categories'] = TaxiCategory::get(['id','category_name']);
		return view('user_requests/index', $data);
	}
	public function generateReport(){
		$startDate = Request::input('start_date',date('Y-m-d'));
		$endDate = Request::input('end_date');
		$data['categories'] = TaxiCategory::get(['id','category_name']);
		$data['daily_report'] = $this->getDailyReport($startDate,$endDate,false);
		return view('user_requests.report',$data)->render();
	}
	private function getDailyReport($date,$date2 = null,$current = true)	{
		$total = UserRequest::select(['category_id','user_id','created_at'])->groupBy(['created_at','user_id','category_id']);
	//	$completed = UserRequest::select(['category_id','user_id','created_at']);
		if($current){
			$total->whereDate('created_at',$date);
		}elseif(!empty($date) && !empty($date2)){
			$total->whereDate('created_at','>=',$date)->whereDate('created_at','<=',$date2);
		}elseif(!empty($date)){
			$total->whereDate('created_at','>=',$date);
		}elseif(!empty($date2)){
			$total->whereDate('created_at','<=',$date2);
		}
		$completed = clone $total;
	    $total = $total->get();
		$completed = $completed->where('request_status',7)->get();
		return [$total,$completed];
	}
	function status() {
		$id = Request::get('id');
		if (Request::get('action') == 1) {
			$status = 5;
		} else {
			$status = 4;
		}
		$r = UserRequest::find($id);
		$r -> cancel_date = date('Y-m-d H:i:s');
		$r -> request_status = $status;
		$r -> save();
		Taxi::where('taxi_id', $r -> taxi_id) -> update(['serving_status' => 1]);
		Userx::where('user_id', $r -> user_id) -> update(['travel_status' => 1]);
		return Redirect::to('/user_requests');
	}

	public function show($id) {
		$res0 = UserRequest::select('user_request_data.*') -> where('id', $id) -> with('user') -> with('taxi') -> with('invoice')->with('travel_log');
		$res = $data['res'] = $res0 -> first();
		$data['ck'] = Request::get('ck');
		$req_status = array(0 =>array("N/A"),
							1 => array("Ignored", 'red'),
							2 => array("others accepted", 'red'),
							3 => array("Picking up", 'blue'),
							4 => array("User canceled", 'red'),
							5 => array("Driver canceled", 'red'),
							6 => array("On ride", 'blue'),
							7 => array("Completed", 'blue'),
							8 => array("Waiting user", 'blue'),
							9 => array("Rejected", 'red'));
						
		if (!empty($res -> invoice) && $res -> invoice-> created_at) {
			if($res->accept_date != null) {
				$date1 = date_create($res -> accept_date);
				$date2 = date_create($res -> invoice -> created_at);
				$diff = date_diff($date1, $date2);
				$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
			}
			
		}
		elseif ( $res->accept_date != null && ($res->request_status == 4 || $res->request_status == 5) ) {
			$date1 = date_create($res -> accept_date);
			$date2 = date_create($res -> cancel_date);
			$diff = date_diff($date1, $date2);
			$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
		}
		elseif ($res->request_status == 3 || $res->request_status == 8) {
			$date1 = date_create($res -> accept_date);
			$date2 = date_create(NULL);
			$diff = date_diff($date1, $date2);
			$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
		}else {
			$t0 = '-';
		}
		// $t0 = $diff -> h . ':' . $diff -> i . ':' . $diff -> s;

		$distance0 = $res -> target_distance ? $res -> target_distance : '-';
		$duration0 = $res -> target_duration ? $res -> target_duration : '-';
		$fare0 = $res -> estimate_fare ? $res -> estimate_fare : '-';
		$total0 = 0;
		$data['distance'] = '';
		$data['duration'] = '';
		if( !empty($res -> invoice) ){
			$total0 = $res -> invoice -> total ? $res -> invoice -> total : '-';
			$data['distance'] = $distance0 . ' (' . number_format($res -> invoice -> distance / 1000, 2) . ' km)';
			$time = $res ->invoice -> duration;
			$hours = $time  / 3600;
            $minutes = ($time  % 3600) / 60;
            $seconds = $time  % 60;
            $duration_ = sprintf("%02d:%02d:%02d", $hours,$minutes,$seconds);
			$data['duration'] = $duration0 . ' (' . $duration_ . ')';
		}
		$data['fare'] = $fare0 . ' (' . $total0 . ')';
		$data['status'] = $req_status[$res -> request_status][0];
		$data['pickup'] = $t0;

		$serving_status = $res -> taxi -> serving_status;
		if ($serving_status == 2) {
			$u_lat = $res -> taxi -> latitude;
			$u_lot = $res -> taxi -> longitude;
		} else {
			$u_lat = $res -> user_lat ? $res -> user_lat : 11.557531;
			$u_lot = $res -> user_long ? $res -> user_long : 104.908337;
		}
		$data['u_lat'] = $u_lat;
		$data['u_lot'] = $u_lot;
		$data['taxi'] = $res -> taxi;
		if($res -> user == null){

			$user = new Userx();
			$user -> name = 'Ghost';
			$user -> userId = '11111ghost';
			$user -> avatar = '';
			$user -> phone = '';
			$user -> verify_status = '';
			$data['user'] = $user;
		}
		else{
			$data['user'] = $res -> user;
		}
		$data['invoice'] = $res -> invoice;
		$data['travel_log'] = $res -> travel_log;
		return (json_encode($data));

		//return view('user_requests/show', $data);
	}

}
