<?php

namespace App\Http\Controllers;

use Request;
use App\Company;
use Session;
use Validator;
use Redirect;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$data['keyword'] = $keyword = Request::get('keyword');
    	$res = Company::where('active', 1);
    	if($keyword) $res->where('name', 'LIKE', '%'.$keyword.'%');
    	$data['res'] = $res->paginate(100);
        return view('companies/index', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('companies/create');
    }
    public function store()
	{
      	$data = Request::except(['_token']);
        $rule = [
            'name' => 'required',
            'code' => 'required',
			'email' => 'required |unique:company',
            'password' => 'required',
            'logo'=> 'required',
        ];
        $v = Validator::make($data, $rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
	    if (Request::file('logo') && Request::file('logo')->isValid()) {
		    $destinationPath = 'uploads'; // upload path
		    $extension = Request::file('logo')->getClientOriginalExtension(); // getting image extension
		    $fileName = rand(11111,99999).'.'.$extension; // renameing image
		    Request::file('logo')->move($destinationPath, $fileName); // uploading file to given path
		}
		$data['logo'] = $fileName;
		$data['password'] = Hash::make($data['password']);
	    Company::create($data);
	    Session::flash('flash_message', 'Successfully added!');
	    return Redirect::to('/companies');
	}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    	$data['res'] = Company::find($id);
    	return view('companies/edit', $data);
    }
    public function update($id)	{
		$db = Company::find($id);
      	$data = Request::except(['_token']);
		$rule = [
            'name' => 'required',
            'code' => 'required'
        ];
        $v = Validator::make($data, $rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
	    if (Request::file('logo') && Request::file('logo')->isValid()) {
		    $destinationPath = 'uploads'; // upload path
		    $extension = Request::file('logo')->getClientOriginalExtension(); // getting image extension
		    $fileName = rand(11111,99999).'.'.$extension; // renameing image
		    Request::file('logo')->move($destinationPath, $fileName); // uploading file to given path
		    $data['logo'] = $fileName;
		}else{
			unset($data['logo']);
		}
		$db->fill($data)->save();
	    //TaxiCategory::update($data);
	    Session::flash('flash_message', 'Successfully updated!');
	    return Redirect::to('/companies');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $db = Company::where('id',$id)->delete();
        return redirect()->back();
    }

}
