<?php

namespace App\Http\Controllers;


use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WalletLog;
use App\Taxi;

class DriveriTsumoLuyController extends Controller
{
    public function __construct() {
        $this -> middleware('auth');
        $this -> listMonth = array();
        $this -> listMonth['01'] = "January";
        $this -> listMonth['02'] = "February";
        $this -> listMonth['03'] = "March";
        $this -> listMonth['04'] = "April";
        $this -> listMonth['05'] = "May";
        $this -> listMonth['06'] = "June";
        $this -> listMonth['07'] = "July";
        $this -> listMonth['08'] = "August";
        $this -> listMonth['09'] = "September";
        $this -> listMonth['10'] = "October";
        $this -> listMonth['11'] = "November";
        $this -> listMonth['12'] = "December";
    }

    public function index(Request $request) {

      $date = $request->date;
      if (!isset($date) || $date === "") {
          $date = date("Y-m");
      }
      $cp_date = $date;
      $month_id = explode('-', $cp_date);

      $commision = WalletLog::selectRaw('t_wallet_log.*, t_taxi.driver_name, t_taxi.id as tid');
      $commision -> join('taxi', 'taxi.taxi_id', '=', 'wallet_log.taxi_id');
    //$commision -> where('wallet_log.update_value', '<',0); => commision fee
      $commision -> where('actor','commision fee');
      $commision -> where('wallet_log.created_at', 'LIKE', '%' . $date . '%');
      $commision -> orderBy('wallet_log.id', 'DESC');
      $com_result = $commision->get();
      $promotion = WalletLog::selectRaw('t_wallet_log.*, t_taxi.driver_name,t_taxi.id as tid');
      $promotion -> join('taxi', 'taxi.taxi_id', '=', 'wallet_log.taxi_id');
      $promotion -> where('actor', 'itsumo promotion');
      //$promotion -> where('wallet_log.update_value', 5000); => itsumo promotion
      $promotion -> where('wallet_log.created_at', 'LIKE', '%' . $date . '%');
      $promotion -> orderBy('wallet_log.id', 'DESC');
      $p_result = $promotion->get();
      $fare = WalletLog::selectRaw('t_wallet_log.*, t_taxi.driver_name, t_taxi.id as tid');
      $fare -> join('taxi', 'taxi.taxi_id', '=', 'wallet_log.taxi_id');
      $fare -> where('wallet_log.update_value', '>',0);
      $fare -> where('actor','taxi fare');
      // $fare -> where('wallet_log.update_value', '!=',5000); => taxi fare
      $fare -> where('wallet_log.created_at', 'LIKE', '%' . $date . '%');
      $fare -> orderBy('wallet_log.id', 'DESC');
      $f_result = $fare->get();
      $totalFare = count($f_result) > 0 ? $f_result->sum('update_value') : 0;
      $totalCommision = count($com_result) > 0 ? $com_result->sum('update_value') : 0;
      $totalPromotion = count($p_result) > 0 ? $p_result->sum('update_value') : 0;
      $data = array(
          'totalPromotion'=> number_format($totalPromotion),
          'totalFare' =>  number_format($totalFare),
          'totalCommision'=> number_format($totalCommision),
          'commision' => $com_result,
          'promotion' => $p_result,
          'ridingFare' => $f_result,
          'date' => $date,
          'month' => $this -> listMonth[$month_id[1]]
      );
      return view('driver_itsumo_luy/index', $data);
    }
}
