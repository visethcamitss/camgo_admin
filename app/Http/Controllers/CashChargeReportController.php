<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WalletLog;
use App\Taxi;
use App\Userx;
use Maatwebsite\Excel\Facades\Excel;

class CashChargeReportController extends Controller{
    public function __construct() {
        $this -> middleware('auth');
        $this -> listMonth = array();
        $this -> listMonth['01'] = "January";
        $this -> listMonth['02'] = "February";
        $this -> listMonth['03'] = "March";
        $this -> listMonth['04'] = "April";
        $this -> listMonth['05'] = "May";
        $this -> listMonth['06'] = "June";
        $this -> listMonth['07'] = "July";
        $this -> listMonth['08'] = "August";
        $this -> listMonth['09'] = "September";
        $this -> listMonth['10'] = "October";
        $this -> listMonth['11'] = "November";
        $this -> listMonth['12'] = "December";
    }
    public function index(Request $request) {
        $date = $request->date;
        if (!isset($date) || $date === "") {
            $date = date("Y-m");
        }
        $cp_date = $date;
        $month_id = explode('-', $cp_date);
        $data = array(
            'total'=> 0,
            'date' => $date,
            'month' => $this -> listMonth[$month_id[1]]
        );

        if($month_id[0] >= 2018 || $month_id[1] >= 4){
           $param = ['month' => $date];
           $url = "get_topup_log";
           $result = $this->getWallet($param, $url);
           if(!empty($result)){
              $jsonData = json_decode($result);
              if(!empty($jsonData->data)){
                $taxiIds = [];
                $userIds = [];
                foreach($jsonData->data as $js){
                    if($js->user_type == 1){
                        $taxiIds[] = $js->user_id;
                    }
                    // else{
                    //     $userIds[] = $js->user_id;
                    // }
                }
                if(count($taxiIds) > 0){
                    $data['taxi'] = Taxi::whereIn('taxi_id', $taxiIds)->get(['id','taxi_id','driver_name','driver_phone']);
                }
                // if(count($userIds) > 0){
                //     $data['user'] = Userx::whereIn('user_id',$userIds)->get(['id','user_id','name','phone']);
                // }
                $data["result"] = $jsonData->data;
             }
           }
        }
        if($month_id[0] < 2018 ||  ($month_id[0] == 2018 && $month_id[1] <= 4)){
            $res = WalletLog::selectRaw('t_wallet_log.*,t_taxi.driver_name,t_taxi.id as tid,t_taxi.driver_phone as phone');
            $res -> join('taxi', 'taxi.taxi_id', '=', 'wallet_log.taxi_id');
            $res -> where('wallet_log.update_value', '>',0);
            $res->where(function($q){
                $q->where('actor','qr code')->orWhere('actor','admin mobile')->orWhere('actor','WingSDK');
            });
            $res -> where('wallet_log.created_at', 'LIKE', '%' . $date . '%');
            $res -> orderBy('wallet_log.id', 'DESC');
            $data['res']=$result  = $res->get();
        }
        return view('cash_charge/index', $data);
    }
    public function export(Request $request) {
        $date = $request->date;
        if (!isset($date) || $date === "") {
            $date = date("Y-m");
        }
        $cp_date = $date;
        $month_id = explode('-', $cp_date);
        $data = array(
            'total'=> 0,
            'date' => $date,
            'month' => $this -> listMonth[$month_id[1]]
        );

        if($month_id[0] >= 2018 || $month_id[1] >= 4){
           $param = ['month' => $date];
           $url = "get_topup_log";
           $result = $this->getWallet($param, $url);
           if(!empty($result)){
              $jsonData = json_decode($result);
              if(!empty($jsonData->data)){
                $taxiIds = [];
                $userIds = [];
                foreach($jsonData->data as $js){
                    if($js->user_type == 1){
                        $taxiIds[] = $js->user_id;
                    }
                }
                if(count($taxiIds) > 0){
                    $data['taxi'] = Taxi::whereIn('taxi_id', $taxiIds)->get(['id','taxi_id','driver_name','driver_phone']);
                }
                $data["result"] = $jsonData->data;
             }
           }
        }
        if($month_id[0] < 2018 ||  ($month_id[0] == 2018 && $month_id[1] <= 4)){
            $res = WalletLog::selectRaw('t_wallet_log.*,t_taxi.driver_name,t_taxi.id as tid,t_taxi.driver_phone as phone');
            $res -> join('taxi', 'taxi.taxi_id', '=', 'wallet_log.taxi_id');
            $res -> where('wallet_log.update_value', '>',0);
            $res->where(function($q){
                $q->where('actor','qr code')->orWhere('actor','admin mobile')->orWhere('actor','WingSDK');
            });
            $res -> where('wallet_log.created_at', 'LIKE', '%' . $date . '%');
            $res -> orderBy('wallet_log.id', 'DESC');
            $data['res']=$result  = $res->get();
        }
        Excel::create('Taxi Riding History', function($excel) use ($data,$total_ride,$total_price){
            $this->excelTemplate($excel,$data,$total_ride,$total_price);
        })->download('xls');

        return view('cash_charge/index', $data);
    }


    public function excelTemplate($excel, $data,$total_ride,$total_price)
    {
    $row =1;
    $excel->sheet('mysheet', function ($sheet) use ($data,$row,$total_ride,$total_price) {
        $sheet->setFontFamily('Khmer MEF1');
        $sheet->setFontSize(12);
        $sheet->setHeight(1, 50);
        $sheet->freezeFirstRow();
        $sheet->cell('A1', 'Name');
        $sheet->cell('B1', 'Phone');
        $sheet->cell('C1', 'Update Amount');
        $sheet->cell('D1', 'New Balance');
        $sheet->cell('E1', 'Charge By');
        $sheet->cell('F1', 'Memo');
        $sheet->cell('G1', 'Date');
        // header style
        $sheet->cells('A1:G1', function ($cells) {
            $cells->setFontColor('#5B9BD5');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontSize(14);
            $cells->setFontWeight('bold');
        });
        $column_headers = ["B", "C", "K", "L", "M", "N", "O"];
        foreach ($column_headers as $column_header){
            $sheet->setWidth($column_header, 25);
        }
        $sheet->setWidth('A', 25);
        $sheet->setWidth('D', 30);
        $sheet->setWidth('E', 30);
        $sheet->setWidth('F', 30);
        $sheet->setWidth('G', 30);
        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
        if($data['result']){
            foreach ($data['result'] as $index => $val) {
                $name = 'N/A';
                $phone = '';
                if($val->user_type == 1 && !empty($data['taxi'])){
                    foreach($data['taxi'] as $t){
                        if($t->taxi_id == $val->user_id){
                            $name = $t->driver_name;
                            $phone = $t->driver_phone;
                            break;
                        }
                    }
                }
                $total_amount += $val->amount;
                $sheet->cell('A' . ($index + 2), $name);
                $sheet->cell('B' . ($index + 2), $phone);
                $sheet->cell('C' . ($index + 2), $val->amount);
                $sheet->cell('D' . ($index + 2), $val->remain_balance);
                switch ($val->action){
                    case 1:
                        $action = "Admin Mobile";
                        break;
                    case 2:
                        $action = "Wing SDK";
                        break;
                    case 3:
                        $action = "Pi Pay SDK";
                        break;
                    default:
                        $action = "Admin Mobile";
                }
                $sheet->cell('E' . ($index + 2), $action);
                $sheet->cell('F' . ($index + 2), $val->user_comment);
                $sheet->cell('G' . ($index + 2), $val->time);
                $row++;
            }
        }else {
            foreach ($data['res'] as $index => $val) {
                $total_amount += $val->amount;
                $sheet->cell('A' . ($index + 2), $val->name);
                $sheet->cell('B' . ($index + 2), $val->phone);
                $sheet->cell('C' . ($index + 2), $val->amount);
                $sheet->cell('D' . ($index + 2), $val->remain_balance);
                $sheet->cell('E' . ($index + 2), $val->action);
                $sheet->cell('F' . ($index + 2), $val->user_comment);
                $sheet->cell('G' . ($index + 2), $val->time);
                $row++;
            }
        }
        $sheet->cell('A' . ($row +1), 'Total Charge for:'.$data['month']);
        $sheet->cell('B' . ($row +1), $total_amount);
        $sheet->cells('A2:G' .$row, function ($cells){
            $cells->setValignment('top');
        });
    });
}


}
