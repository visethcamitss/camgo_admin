<?php

namespace App\Http\Controllers;

use App\Company;
use Request;
use Session;
use Validator;
use Redirect;
use App\UserRequest;
use Illuminate\Support\Facades\DB;

use App\Userx;
use App\Taxi;

class ReportController extends Controller
{
	public function __construct()
    { 
        $this->middleware('auth')->except(['admin_driver_request','admin_driver_request_map','completed_request','company_list']);
    }

    public function index()
    {   
        //$data['keyword'] = $keyword = Request::get('keyword');

		$riding = DB::table('invoice')
                     ->select(DB::raw('count(invoice_number) as riding'))
                     ->groupBy('month')
					 ->where('is_delete',2)
                     ->get();
		foreach($riding as $r){
			$data['riding'][] = $r->riding;
		}
		
		$months = DB::table('invoice')
                     ->select(DB::raw('month'))
                     ->groupBy('month')
					 ->where('is_delete',2)
                     ->get();
		
		foreach($months as $m){
			$data['months'][] = $m->month;
		}
		
		$charge = DB::table('invoice')
                     ->select(DB::raw('TRUNCATE(sum(service_charge),0) as service_charge'))
                     ->groupBy('month')
					 ->where('is_delete',2)
                     ->get();
		foreach($charge as $c){
			$data['charge'][] = $c->service_charge/4000;
		}
		
		$total = DB::table('invoice')
                     ->select(DB::raw('count(invoice_number) as total_riding, TRUNCATE(sum(service_charge),0) as total_charge'))
					 ->where('is_delete',2)
                     ->get();
		$aa[0]['name'] = 'riding';
		$aa[0]['y'] = $total[0]->total_riding;
		$aa[1]['name'] = 'serviec charge';
		$aa[1]['y'] = $total[0]->total_charge/4000;
		
    	$data['total'] = $aa;
		//return $data;		
        return view('reports/index', $data);
    }

    public function admin_driver_request()
    {
        $type = Request::input('type');
        $company = Request::input('company',0);
        if($type < 4){
            $res = UserRequest::select('user_request.*','taxi_category.category_name')->join('taxi_category','taxi_category.id', '=', 'user_request.category_id') -> where('is_deleted', '>', 1) -> orderBy('id', 'DESC') -> with(['user'=> function($q){
                $q->select('id','user_id','name','avatar','phone');
            },'taxi' => function($q){
                $q->select('id','taxi_id','driver_name','driver_avatar','company_id');
            }]);
            if($company > 0){
                $res->whereHas('taxi', function ($query) use($company) {
                    $query->where('company_id', $company);
                });
            }
            if($type == 1){
                $res->where('request_status',3);
            }elseif($type == 2){
                $res->where('request_status',8);
            }elseif($type == 3){
                $res->where('request_status',6);
            }
            return $res->paginate(100);
        }
        $result = Taxi::select('taxi.*', 'taxi_category.category_name as category', 'company.name as company');
        $result -> join('taxi_category', 'taxi_category.id', '=', 'taxi.category_id');
        $result -> join('company', 'company.id', '=', 'taxi.company_id');
        $result -> where('taxi.status', '=', 1);
        if($type == 4){
            $result -> where(function($q){
                $q->where('priority', '>', 0)->whereRaw('TIME_TO_SEC(TIMEDIFF(NOW(),t_taxi.updated_at)) <= 1800'); //20 mn
            });
        }elseif($type == 5){
            $result -> where(function($q){
                $q->where('priority', '<=', 0)->orWhereRaw('TIME_TO_SEC(TIMEDIFF(NOW(),t_taxi.updated_at)) > 1800'); //20 mn
            });
        }
        if($company > 0){
            $result -> where('taxi.company_id', $company);
        }
        return $result->paginate(100);
    }
    public function admin_driver_request_map($id) {
        $res0 = UserRequest::select('user_request.*') -> where('id', $id) -> with('user') -> with('taxi') -> with('invoice')->with('travel_log');
        $res = $data['res'] = $res0 -> first();
        $data['ck'] = Request::get('ck');
        $req_status = array(0 =>array("N/A"),
            1 => array("Ignored", 'red'),
            2 => array("others accepted", 'red'),
            3 => array("Picking up", 'blue'),
            4 => array("User canceled", 'red'),
            5 => array("Driver canceled", 'red'),
            6 => array("On ride", 'blue'),
            7 => array("Completed", 'blue'),
            8 => array("Waiting user", 'blue'),
            9 => array("Rejected", 'red'));

        if ($res -> request_status != 4 && $res -> request_status != 5) {
            $date1 = date_create($res -> accept_date,timezone_open('Asia/Phnom_Penh'));
            $date2 = date_create(null,timezone_open('Asia/Phnom_Penh'));
            $diff = date_diff($date1, $date2);
        }elseif (!empty($res -> invoice) && $res -> invoice-> created_at) {
            $date1 = date_create($res -> accept_date,timezone_open('Asia/Phnom_Penh'));
            $date2 = date_create($res -> invoice -> created_at,timezone_open('Asia/Phnom_Penh'));
            $diff = date_diff($date1, $date2);
        } else{
            $date1 = date_create($res -> accept_date ? $res -> accept_date : $res -> request_date,timezone_open('Asia/Phnom_Penh'));
            $date2 = date_create($res -> cancel_date,timezone_open('Asia/Phnom_Penh'));
            $diff = date_diff($date1, $date2);
        }
        $t0 = $diff -> h . ':' . $diff -> i . ':' . $diff -> s;

        $distance0 = $res -> target_distance ? $res -> target_distance : '-';
        $duration0 = $res -> target_duration ? $res -> target_duration : '-';
        $fare0 = $res -> estimate_fare ? $res -> estimate_fare : '-';
        $total0 = 0;
        $data['distance'] = '';
        $data['duration'] = '';
        if( !empty($res -> invoice) ){
            $total0 = $res -> invoice -> total ? $res -> invoice -> total : '-';
            $data['distance'] = $distance0 . ' (' . number_format($res -> invoice -> distance / 1000, 2) . ' km)';
            $data['duration'] = $duration0 . ' (' . date('H:i:s', $res -> invoice -> duration) . ')';
        }
        $data['fare'] = $fare0 . ' (' . $total0 . ')';
        $data['status'] = $req_status[$res -> request_status][0];
        $data['pickup'] = $t0;

        $serving_status = $res -> taxi -> serving_status;
        if ($serving_status == 2) {
            $u_lat = $res -> taxi -> latitude;
            $u_lot = $res -> taxi -> longitude;
        } else {
            $u_lat = $res -> user_lat ? $res -> user_lat : 11.557531;
            $u_lot = $res -> user_long ? $res -> user_long : 104.908337;
        }
        $data['u_lat'] = $u_lat;
        $data['u_lot'] = $u_lot;
        $data['taxi'] = $res -> taxi;
        if($res -> user == null){

            $user = new Userx();
            $user -> name = 'Ghost';
            $user -> userId = '11111ghost';
            $user -> avatar = '';
            $user -> phone = '';
            $user -> verify_status = '';
            $data['user'] = $user;
        }
        else{
            $data['user'] = $res -> user;
        }
        $data['invoice'] = $res -> invoice;
        $data['travel_log'] = $res -> travel_log;
        return (json_encode($data));
    }

    public function completed_request()
    {
        $company = Request::input('company',0);
        $res = UserRequest::select('id','taxi_id','')-> with(['taxi' => function($q){
            $q->select('id','taxi_id','company_id');
        }]);
        $res->where('request_status',7);
        if($company > 0){
            $res->whereHas('taxi', function ($query) use($company) {
                $query->where('company_id', $company);
            });
        }
        $res->whereMonth('complete_date', '=', date('m'));
        $res->whereYear('complete_date', '=', date('Y'));
        return $res->count();
    }

    public function company_list()
    {
        $company = Company::all(['id','name']);
        return $company;
    }
}
