<?php

namespace App\Http\Controllers;

use Request;
use App\Notification;
use App\Taxi;
use App\Userx;
use App\TaxiCategory;
use Session;
use Validator;
use Redirect;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller {
	public function __construct() {
		$this -> middleware('auth');
	}
	public function taxi($id = null) {
		$res = Taxi::select('taxi.id','taxi.taxi_id', 'taxi.driver_name', 'taxi.driver_phone', 'taxi.taxi_image', 'taxi.device_info'); 
		$res -> orderBy('driver_name', 'asc');
		$data['keyword'] = $keyword = Request::get('keyword');
		if ($keyword){
			if($keyword=='no money'){
				$res -> where('taxi.job_status', '=',3);
			}else{
				$res -> where('driver_name', 'LIKE', '%' . $keyword . '%');
			}
			$res -> orWhere('device_info', 'LIKE', '%' . $keyword . '%');
		}
		if(empty($id)){
			$res -> where('reviewed', 1);
			$res -> where('status', 1);
			$res -> where('job_status', 1);
		}else{
			$res -> where('taxi.id',$id);
			$data['taxi_id'] = $id;
		}
		$data['res'] = $res -> paginate(500);
		$data['categories'] = TaxiCategory::select('id','category_name')->get();
		return view('notifications/taxi', $data);
	}
	public function user($id = null) {
		$res = Userx::where('id', '>', 0)->orderBy('id', 'DESC');
		if(!empty($id)){
			$res -> where('id',$id);
			$data['user_id'] = $id;
		}
    	$data['keyword'] = $keyword = Request::get('keyword');
    	if($keyword){
            $res->where('name', 'LIKE', '%'.$keyword.'%');
            $res->orWhere('phone', 'LIKE', '%'.$keyword.'%');
			$res->orWhere('device_info', 'LIKE', '%'.$keyword.'%');
        }
		$data['res'] = $res -> paginate(500);
		return view('notifications/user', $data);
	}
}
