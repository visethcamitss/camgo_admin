<?php

namespace App\Http\Controllers;
use Request;
use App\TaxiCategory;
use Session;
use Validator;
use Redirect;
use App\Company;
use App\CompanyTaxiCategory;
use App\TaxiCategoryFaire;
use Illuminate\Support\Facades\Storage;
use DB;

class TaxiCategoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data['keyword'] = $keyword = Request::get('keyword');
    	$res = TaxiCategory::where('is_delete','>', 0);
    	if($keyword) $res->where('category_name', 'LIKE', '%'.$keyword.'%');
    	$data['res'] = $res->paginate(100);
        return view('taxi_categories/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['companies'] = Company::all();
        return view('taxi_categories/create', $data);
    }

    public function store()
	{
      	$data = Request::except(['_token']);
        $rule = [
            'category_name' => 'required',
            'seat_number' => 'required',
            'image' => 'required',
            /// 'company_id'=> 'required',
        ];
        $v = Validator::make($data, $rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $uploadPath = env('UPLOAD_PATH')."/categories";

	    if (Request::file('image') && Request::file('image')->isValid()) {
		    $extension = Request::file('image')->getClientOriginalExtension(); // getting image extension
		    $fileName = strtotime("now").rand(11111,99999).'.'.$extension; // renameing image
		    Request::file('image')->move($uploadPath, $fileName); // uploading file to given path
            $data['image'] = $fileName;
		}
        if (Request::file('icon') && Request::file('icon')->isValid()) {
            $extension = Request::file('icon')->getClientOriginalExtension(); // getting image extension
            $fileName = strtotime("now").rand(11111,99999).'.'.$extension; // renameing image
            Request::file('icon')->move($uploadPath, $fileName); // uploading file to given path
            $data['icon'] = $fileName;
        }
        $company_id = $data['company_id'];

        /// unset($data['company_id']);
		$data['service_charge'] = 	$data['service_charge'] / 100;
		//$data['rural_charge'] = 	$data['rural_charge'] / 100;
	    $id = TaxiCategory::create($data)->id;

        /// $cc = new CompanyTaxiCategory();
        /// $cc->company_id = $company_id;
        /// $cc->category_id = $id;
        /// $cc->save();

	    Session::flash('flash_message', 'Successfully added!');

	    return Redirect::to('/categories');
	}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['companies'] = Company::all();
    	$res = TaxiCategory::find($id);
		$data['res'] = $res;
		$data['res1'] = TaxiCategoryFaire::where('category_id', $id)->get();
        $data['company_id'] = CompanyTaxiCategory::where('category_id', $id)->first()->company_id;
    	return view('taxi_categories/edit', $data);
    }
    public function update($id)
	{
		$db = TaxiCategory::find($id);
      	$data = Request::except(['_token']);
        $rule = [
            'category_name' => 'required',
            'seat_number' => 'required',
            /// 'company_id'=> 'required',
        ];
		$data['visible_on'] =  implode(",",$data['location']);
		unset($data['location']);
        $v = Validator::make($data, $rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $uploadPath = env('UPLOAD_PATH')."/categories";
	    if (Request::file('image') && Request::file('image')->isValid()) {
		    $extension = Request::file('image')->getClientOriginalExtension(); // getting image extension
		    $fileName = strtotime("now").rand(11111,99999).'.'.$extension; // renameing image
		    Request::file('image')->move($uploadPath, $fileName); // uploading file to given path
		    $data['image'] = "categories/".$fileName;
		}else{
			unset($data['image']);
		}
        if (Request::file('icon') && Request::file('icon')->isValid()) {
            $extension = Request::file('icon')->getClientOriginalExtension(); // getting image extension
            $fileName = strtotime("now").rand(11111,99999).'.'.$extension; // renameing image
            Request::file('icon')->move($uploadPath, $fileName); // uploading file to given path
            $data['icon'] = "categories/".$fileName;
        }else{
            unset($data['icon']);
        }
		/// $company_id = $data['company_id'];
		/// unset($data['company_id']);
		$db->is_delete = $data["is_delete"];
	//	unset($data['is_delete']);
		$data['service_charge'] = 	$data['service_charge'] / 100;
		//$data['rural_charge'] = 	$data['rural_charge'] / 100;

		$db->fill($data)->save();

		// $cc = CompanyTaxiCategory::where('category_id', $id)->first();
		// $cc->company_id = $company_id;
		// $cc->save();

		Session::flash('flash_message', 'Successfully updated!');
		// write category info to file for itsumo calculator
		$categoryData = TaxiCategory::where('is_delete','>', 0)->get();
		$file_path = 'category.json';
		Storage::put($file_path, $categoryData, "public");

		return Redirect::to('/categories');
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $db = TaxiCategory::find($id);
		$db -> is_delete = 1;
        $db->save();
        return redirect()->back();
    }

}
