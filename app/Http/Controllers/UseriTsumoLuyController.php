<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UseriTsumoLuyController extends Controller
{
    public function __construct() {
        $this -> middleware('auth');
    }

    public function index(Request $request) {

        if (!isset($request->date) || $request->date === "") {
            $data = array(
                'res' => 'give all data'
            );
        } else {
            $date = $request->date;

            $data = array(
                'res' => 'only data',
                'date' => $date
            );
        }

        return view('user_itsumo_luy/index', $data);
    }
}
