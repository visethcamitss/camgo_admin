<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use Redirect;
use Validator;

class PromotionController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        $res = Promotion:: all();
        $data['res'] =$res;
        return view('promotion/index',$data);
    }
    public function create(){
        return view('/promotion/create');
    }
    public function store(){
        $data = Request::except(['_token']);
        $rule = [
            'value' => 'required | numeric'
        ];
        $v = Validator::make($data,$rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $active = Request::input('active');
        if($active == 0){
            $data['active'] =1;
        }else if($active == 1) {
            $data['active'] = 2;
        }
        $data['code'] = strtoupper($data['code']);
        Promotion::create($data);
        return Redirect::to('/promotion');
    }
    public function edit($id){
        $res = Promotion::find($id);
        $data['res'] = $res;
        return view('promotion/edit',$data);
    }
    public function update($id){
        $db = Promotion::find($id);
        $data = Request::except(['_token']);
        $rule = [
            'active' => 'required | numeric',
            'value' => 'required | numeric'
        ];
        $v = Validator::make($data,$rule);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $active = Request::input('active');
        if($active == 0){
            $data['active'] =1;
        }else if($active == 1) {
            $data['active'] = 2;
        }
        $db->fill($data)->save();
		return Redirect::to('/promotion');
    }
}
