<?php

namespace App\Http\Controllers;

use Request;
use App\Invoice;
use Session;
use Validator;
use Redirect;
use App\UserRequest;

use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth')->except(['show_spin','spin_json','give_reward']);
    }

    public function index()
    {
        $data['keyword'] = $keyword = Request::get('keyword');
    	$res = Invoice::where('invoice_number', '>', 0)->orderBy('invoice_number', 'DESC');

    	if($keyword) $res->where('invoice_number', $keyword);
    	$data['res'] = $res->paginate(100);
        return view('invoices/index', $data);
    }

    function email($id){
        $res = Invoice::where('invoice_number', '=', $id)->first();
        $data['res'] = UserRequest::where('request_id', '=', $res->request_id)->with('user')->with('taxi')->with('invoice')->first();
        return view('invoices/email', $data);
    }

	public function send_invoice()
	{
		$invoices = Request::input('invoiceId');
		$email = Request::input('email');
		$user_name = Request::input('name');
		$data = [invoiceId => $invoices, email => $email,name => $user_name] ;
        $data_string = json_encode($data);
        $ch = curl_init('http://localhost:3010/send_invoice_admin');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result =curl_exec($ch);
        return redirect()->back();
    }

    public function show_spin($requestId, $type = null)
    {
        if($type == "wing_topup") {
            $user_id = $requestId;
            $data = ['userId' => $user_id];
            $data_string = json_encode($data);
            $ch = curl_init('http://localhost:3010/get_last_topup');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            $json_data = json_decode($result);
            $amount = $json_data->data->amount;
            $invoice_id = $json_data->data->invoiceId;

		    return view('invoices.spin',compact('user_id', 'type', 'amount', "invoice_id"));

		} else {
			$req = UserRequest::where('request_id',$requestId)->first(['user_id']);
            if(empty($req)){
                return redirect()->back();
            }
            $user_id = $req->user_id;
            return view('invoices.spin',compact('requestId','user_id', 'type'));
		}
        // return view('invoices.spin',compact('requestId','user_id', 'type'));
    }

    public function spin_json($requestId,$user_id,$amount,$type = null)
    {

        // $reward = array(5,10,15,30,50,70);
        // shuffle($reward);

        // $invoice = Invoice::where('request_id',$requestId)->first(['invoice_id','total']);
        /*
        if(empty($invoice)){
            return [];
        }
        $rewardIndex = 0;
        //calculate spin reward
        $rewardValue = 5;
        if($invoice->total<= 5000){
            $rewardValue = 15;
        }elseif($invoice->total <= 10000 ){
            $rewardValue = 10;
        }
        $countLess = 0;
        foreach($reward as $k => $v){
            if($v == $rewardValue){
                $rewardIndex = $k;
            }
            if($v <= $rewardValue){
                $countLess++;
            }
        }
        $pro = array_fill(0,6,0);
        $reward_pro = 100 / $countLess;
        foreach($reward as $k => $v){
            // if($k == $rewardIndex){
            //     $pro[$k] = $reward_pro * 2;
            // }
            if($v <= $reward[$rewardIndex]){
                $pro[$k] = floor($reward_pro);
            }
        }
        $segmentValuesArray = [];
        foreach($reward as $k => $rw){
            $segmentValuesArray[] = array(
                "probability" => $pro[$k],
                "type" => "string",
                "value" => $rw ."%^Bonus",
                "win" => false,
                "resultText" => "YOU WON {$rw}% BONUS!",
                "userData" => array("win" => $rw * $invoice->total/100.0, 'id' => $invoice->invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            );
        }*/

        $win = 0;
        $invoice_id = "";
        if($type == "wing_topup") {
            $invoice_id = $requestId;
            $win = $amount;
        } else {
            $invoice = Invoice::where('request_id',$requestId)->first(['invoice_id','total']);
            $invoice_id = $invoice->invoice_id;
            $win = $invoice->total;
        }

        $segmentValuesArray = [
            array(
                "probability" => 1,
                "type" => "string",
                "value" => "70%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 70% BONUS!",
                "userData" => array("win" => 70 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            ),
            array(
                "probability" => 2,
                "type" => "string",
                "value" => "50%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 50% BONUS!",
                "userData" => array("win" => 50 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            ),
            array(
                "probability" => 5,
                "type" => "string",
                "value" => "30%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 30% BONUS!",
                "userData" => array("win" => 30 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            ),
            array(
                "probability" => 12,
                "type" => "string",
                "value" => "15%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 15% BONUS!",
                "userData" => array("win" => 15 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
                ),
            array(
                "probability" => 60,
                "type" => "string",
                "value" => "5%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 5% BONUS!",
                "userData" => array("win" => 5 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            ),
            array(
                "probability" => 20,
                "type" => "string",
                "value" => "1%^Bonus",
                "win" => false,
                "resultText" => "YOU WON 1% BONUS!",
                "userData" => array("win" => 1 * $win/100.0, 'id' => $invoice_id, 'uid'=>$user_id, "u" => url('send_json'))
            ),
        ];

        $data = array(
            "colorArray" => array("#990000", "#660033", "#E67E22", "#ff0066", "#0099ff", "#33cc33", "#16A085", "#27AE60", "#2980B9", "#8E44AD", "#2C3E50", "#F39C12", "#D35400", "#C0392B", "#BDC3C7","#1ABC9C", "#2ECC71", "#E87AC2", "#3498DB", "#9B59B6", "#7F8C8D"),

            "segmentValuesArray" => $segmentValuesArray,
            "svgWidth" => 1024,
            "svgHeight" => 768,
            "wheelStrokeColor" => "#fff",
            "wheelStrokeWidth" => 18,
            "wheelSize" => 900,
            "wheelTextOffsetY" => 110,
            "wheelTextColor" => "#EDEDED",
            "wheelTextSize" => "4em",
            "wheelImageOffsetY" => 40,
            "wheelImageSize" => 50,
            "centerCircleSize" => 360,
            "centerCircleStrokeColor" => "#8BC34A",
            "centerCircleStrokeWidth" => 14,
            "centerCircleFillColor" => "#EDEDED",
            "segmentStrokeColor" => "#E2E2E2",
            "segmentStrokeWidth" => 4,
            "centerX" => 512,
            "centerY" => 384,
            "hasShadows" => false,
            "numSpins" => 1,
            "spinDestinationArray" => array(),
            "minSpinDuration" => 6,
            "gameOverText" => "Thank You For Riding Taxi Phumi!",
            "invalidSpinText" =>"INVALID SPIN. PLEASE SPIN AGAIN!",
            "introText" => "SPIN TO GET<br>BONUS!",
            "hasSound" => true,
            "gameId" => "9a0232ec06bc431114e2a7f3aea03bbe2164f1aa",
            "clickToSpin" => true,
            "spinDirection" => "cw"
            );

            return $data;
    }

    public function give_reward($requestId,$user_id,$amount)
    {
		$data = ['invoiceId' => $requestId, 'userId'=>$user_id,'cashBackReward'=> $amount] ;
        $data_string = json_encode($data);
        $ch = curl_init('http://localhost:3010/cash_back_wheel');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result =curl_exec($ch);
        if($result){
            return [];
        }
    }
}
