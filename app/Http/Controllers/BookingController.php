<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Booking;

class BookingController extends Controller
{
    public function index(){
        $booking = Booking::with('user')->with('taxi')->with('taxiCategory')->whereIn('booking_status',[1,2,3,4]);
        $data['booking_status'] = $booking_status = Request::get('booking_status');
        $cancel_date = Request::get('cancel_date');
        $data['booking'] = $booking -> limit(200) ->get();
        $res = clone $booking;
        if($cancel_date){
            $booking = $booking-> where('booking_status', 1)->whereNotNull('taxi_id');
        }
        if($booking_status){
            $booking = $booking -> where('booking_status', $booking_status);
        }
		$data['bookings'] = $booking->orderBy('id','desc') ->paginate(20);
        $data['nb_d_cancel_booking'] = $res -> where('booking_status', 1)->whereNotNull('taxi_id')->get()->count();
        return view('booking/index',$data);
    }
}
