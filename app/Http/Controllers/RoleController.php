<?php
/**
 * Created by PhpStorm.
 * User: nsp
 * Date: 9/28/2017
 * Time: 2:22 PM
 */

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use App\UserRole;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{

    public function __construct() {
        $this -> middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $permissions = Permission::all();
        return view('roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
                'name'=>'required|unique:roles|max:10',
                'permissions' =>'required',
            ]
        );

        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
                'Role'. $role->name.' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        return redirect('roles');
    }

    public function editUser($id){

        $user = User::with('roles')->find($id);
        if(empty($user)){
            return redirect()->back();
        }
        $edit_role = Role::all();
        return view('roles.edit_user_role',compact('user','edit_role'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();
        $role->revokePermission();

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form permission in db
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.index')
            ->with('flash_message',
                'Role'. $role->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')
            ->with('flash_message',
                'Role deleted!');
    }
   
    public function showUpdateUserRoleForm($id){

        $user = User::with('roles')->find($id);
        if(empty($user)){
            return redirect()->back();
        }
        $edit_role = Role::all();
        return view('roles.edit_user_role',compact('user','edit_role'));
    }

    public function post_updateUserRole(Request $request,$id)
    {
        $user = User::find($id);
       
        if(empty($user)){
            return redirect()->back();
        }
        $email_validate = 'required|email|max:255|unique:users';
        if($user->email == $request->input('email')){
            $email_validate = 'required|email|max:255';
        }

        $validator =  Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => $email_validate,
        ]);

         if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        $userRole = UserRole::where('user_id',$id)->first();
        if($userRole == null){
            $userRole = new UserRole();
            $userRole->user_id = $id;
        }
        $userRole->role_id = $request->input('role',$userRole->role_id);
        $userRole->save();

        return redirect('list_user');
    }

    public function list_user(Request $request){

        $res = User::with('roles');
        $data['keyword'] = $keyword = $request->input('keyword');

        if($keyword){
            $res->where('name', 'LIKE', '%'.$keyword.'%'); 
        }
        $data['res'] = $res->orderBy('id', 'DESC')->paginate(100);
        return view('roles/list_user', $data);
    }
 
}