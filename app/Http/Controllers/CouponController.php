<?php

namespace App\Http\Controllers;

use Request;
use App\Coupon;
use Session;
use Validator;
use Redirect;
use Illuminate\Support\Facades\Auth;
use PDF;

class CouponController extends Controller {
	public function __construct() {
		$this -> middleware('auth');
	}

	public function index() {
		$values = Request::get('values');
		$status = Request::get('status');

		$res = Coupon::where('is_delete', '>', 1);
		if ($values > 0 && $value != 'None')
			$res -> where('value', $values);
		if ($status > 0)
			$res -> where('status', $status);

		$data['res'] = $res -> paginate(100);
		$data['selected_value'] = $values;
		$data['selected_status'] = $status;
		$values_coupon = ['None', '10000', '20000', '40000', '80000'];
		$values_status = ['None', 'Available', 'Printed', 'Used'];
		$data['values'] = $values_coupon;
		$data['status'] = $values_status;
		return view('coupons/index', $data);
	}

	public function custom_print_preview() {
		$value = Request::get('printValues');
		$number = Request::get('printQuantity');

		$res = Coupon::where('status', '<', 2);
		$res -> where('value', $value);
		$res->update([
			'value' => $value,
			'status' => 1
		]);
		$data['coupons'] = $res -> paginate($number-1);
		$pdf = PDF::loadView('coupons.print', $data) -> setPaper('a7', 'portrait');

		return $pdf -> stream();
	}

	public function printone($id) {

		$res = Coupon::where('status', '<', 2);
		$res -> where('coupon_id', $id);
		$res->update([
			'status' => 2,
			'coupon_id'=> $id
		]);
		$data['coupons'] = $res -> paginate(1);
		$pdf = PDF::loadView('coupons.print', $data) -> setPaper('a7', 'portrait');

		return $pdf -> stream();
	}
}
