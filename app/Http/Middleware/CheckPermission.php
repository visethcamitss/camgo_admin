<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*if (Auth::check()){
            $user = Auth::user();
            if($user->hasRole('Admin')){
                return $next($request);
            }
            $routeName = $request->route()->getName();
            if($routeName != null ){
                if($user->routePermission($routeName)){
                    return $next($request);
                }
                abort('401');
            }elseif($user->roles()->count() == 0){
                abort('401');
            }
        }*/
        return $next($request);
    }
}
