<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model {
  protected $table = 'wallet';
  protected $fillable = ['id','wallet_id','is_delete','currency','value'];
}