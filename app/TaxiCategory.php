<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TaxiCategory extends Model {
  protected $table = 'taxi_category';
  protected $fillable = ['category_name','visible_on', 'seat_number','service_charge_fix','service_charge', 'image','icon','is_delete', 'currency', 'status'];
}
