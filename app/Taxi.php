<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Taxi extends Model {
  protected $table = 'taxi';
  protected $fillable = ['company_id','priority_gift','company_name', 'category_id','allow_classic', 'taxi_id', 'taxi_model', 'plate_number', 'driver_phone', 'driver_name','driver_social_id', 'driver_licence', 'driver_age', 'driver_avatar',
  'status', 'reviewed','serving_status','job_status','taxi_image','wallet_id', 'driver_phone_one', 'driver_phone_two','note'];
}
