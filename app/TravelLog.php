<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TravelLog extends Authenticatable
{
     protected $table = 'travel_log';
}
