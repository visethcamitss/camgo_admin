<?php
/**
 * Created by PhpStorm.
 * User: nsp
 * Date: 9/28/2017
 * Time: 11:36 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $primaryKey = 'id';
}