<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'user_booking';
    function user(){
        return $this->belongsTo('App\Userx', 'user_id', 'user_id');
    }
    function taxi(){
        return $this->belongsTo('App\Taxi', 'taxi_id', 'taxi_id');
    }
    function taxiCategory(){
        return $this->belongsTo('App\TaxiCategory','category_id','id');
    }

}
