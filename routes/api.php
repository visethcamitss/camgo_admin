<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');
Route::get('/admin/driver','ReportController@admin_driver_request');
Route::get('/admin/driver/map/{id}','ReportController@admin_driver_request_map');
Route::get('/admin/driver/completed/request','ReportController@completed_request');
Route::get('/admin/driver/company','ReportController@company_list');