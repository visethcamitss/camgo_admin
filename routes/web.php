<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'TaxiController@index');
Route::get('/home', 'TaxiController@index');
Route::get('/applink', 'ApplinkController@index');

Auth::routes();

// Route::get('/update_user_role/{id}','RoleController@showUpdateUserRoleForm');
Route::post('/post_update_user_role/{id}','RoleController@post_updateUserRole')->name('updateUserRole');
Route::get('/list_user','RoleController@list_user')->name('listUser');
Route::get('/edit_user/{id}', 'RoleController@editUser');

Route::get('/promotion','PromotionController@index')->name('promotion');
Route::get('/promotion/create','PromotionController@create');
Route::post('/promotion/store','PromotionController@store');
Route::get('/promotion/edit/{id}','PromotionController@edit')->name('promotion.edit');
Route::post('/promotion/update/{id}','PromotionController@update');

Route::get('/logout', 'UserxController@logout');
Route::get('/categories', 'TaxiCategoryController@index')->name('categories');
Route::get('/categories/create', 'TaxiCategoryController@create')->name('categories.create');
Route::post('/categories/store', 'TaxiCategoryController@store')->name('categories.store');
Route::get('/categories/edit/{id}', 'TaxiCategoryController@edit')->name('categories.edit');
Route::post('/categories/update/{id}', 'TaxiCategoryController@update')->name('categories.update');
Route::get('/categories/destroy/{id}', 'TaxiCategoryController@destroy')->name('categories.destroy');
Route::get('/category_fare/edit/{id}','TaxiCategoryFareController@edit')->name('category.edit');
Route::post('/category_fare/update/{id}/','TaxiCategoryFareController@update')->name('category.update');
Route::get('/category_fare/create/{id}','TaxiCategoryFareController@create')->name('category.create');
Route::post('/category_fare/store','TaxiCategoryFareController@store')->name('category.store');

Route::get('/userxs', 'UserxController@index')->name('users');
Route::get('/userxs/{id}', 'UserxController@show')->name('users.show');
Route::get('/userxs/edit/{id}', 'UserxController@edit')->name('users.edit');
Route::post('/userxs/update/{id}', 'UserxController@update')->name('users.update');

// Route::get('/user_system','UserSystemController@index');

Route::get('/user_requests', 'UserRequestController@index')->name('user_request');
Route::get('/user_requests/status', 'UserRequestController@status')->name('user_request.status');
Route::get('/user_requests/{id}', 'UserRequestController@show')->name('user_request.show');
Route::get('/generate_reqport','UserRequestController@generateReport');

Route::get('/user_booking','BookingController@index')->name('user_booking');

Route::get('/invoices', 'InvoiceController@index')->name('invoices');
Route::get('/invoices/email/{id}', 'InvoiceController@email')->name('invoices.email');
Route::post('/send_invoice','InvoiceController@send_invoice');

Route::get('/companies', 'CompanyController@index')->name('companies');
Route::get('/companies/create', 'CompanyController@create')->name('companies.create');
Route::post('/companies/store', 'CompanyController@store')->name('companies.store');
Route::get('/companies/edit/{id}', 'CompanyController@edit')->name('companies.edit');
Route::post('/companies/update/{id}', 'CompanyController@update')->name('companies.update');
Route::get('/companies/destroy/{id}', 'CompanyController@destroy')->name('companies.destroy');

Route::get('/taxis/live', 'TaxiController@live')->name('taxis.live');
Route::get('/taxis/ajax_live', 'TaxiController@ajax_live')->name('taxis.ajax_live');
Route::get('/taxis/create', 'TaxiController@create')->name('taxis.create');
Route::post('/taxis/store', 'TaxiController@store')->name('taxis.store');
Route::get('/taxis/edit/{id}', 'TaxiController@edit')->name('taxis.edit');
Route::post('/taxis/update/{id}', 'TaxiController@update')->name('taxis.update');
Route::get('/taxis/destroy/{id}', 'TaxiController@destroy')->name('taxis.destroy');
Route::get('/taxis', 'TaxiController@index')->name('taxis');
Route::get('/taxis/{id}', 'TaxiController@show')->name('taxis.show');

Route::get('/coupon/print/','CouponController@custom_print_preview')->name('coupons.custom_print_preview');
Route::get('/coupon/printone/{id}','CouponController@printone')->name('coupons.printone');
Route::get('/coupons', 'CouponController@index')->name('coupons');

Route::get('/chart','ReportController@index')->name('chart');
Route::get('/notification/taxi','NotificationController@taxi')->name('notification.taxi');
Route::get('/notification/user','NotificationController@user')->name('notification.user');
Route::get('/notification/user/{id}','NotificationController@user')->name('notification.userNotification');
Route::get('/notification/taxi/{id}','NotificationController@taxi')->name('notification.taxiNotification');
Route::get('/get_taxi','NotificationController@getTaxi')->name('notification.all_taxi');
Route::get('/settings','SettingController@index')->name('setting.index');
Route::get('/settings/edit/{id}','SettingController@edit')->name('setting.edit');
Route::post('/settings/update/{id}','SettingController@update');
Route::get('/charge_report', 'CashChargeReportController@index');
Route::get('/export', 'CashChargeReportController@export');

Route::get('/driver_itsumo_luy', 'DriveriTsumoLuyController@index');
Route::get('/user_itsumo_luy', 'UseriTsumoLuyController@index');


Route::resource('roles', 'RoleController');

Route::get('/offline_taxi','TaxiController@set_taxi_to_offline');

Route::get('/spin_wheel/{id}/{type?}','InvoiceController@show_spin');
Route::get('/spin_json/{id}/{uid}/{am}/{type?}','InvoiceController@spin_json');
Route::post('/send_json/{id}/{uid}/{am}','InvoiceController@give_reward');
