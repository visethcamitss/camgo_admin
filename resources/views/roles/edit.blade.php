@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading"> <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1></div>
            <div class="panel-body">
                <form action="{{ route('roles.update',$role->id) }}" method="POST">
                    <input name="_method" type="hidden" value="PUT">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $role->name }}">
                    </div>
                    <h5><b>Assign Permissions</b></h5>
                    <?php $permissionNames = $role->permissions->pluck('id')->toArray(); ?>
                    @foreach ($permissions as $permission)
                        <div class="form-group">
                            <input name="permissions[]" type="checkbox" value="{{ $permission->id }}" @if(in_array($permission->id,$permissionNames)) checked="checked" @endif >
                            <label>{{  ucfirst($permission->name) }}</label>
                        </div>
                    @endforeach
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection