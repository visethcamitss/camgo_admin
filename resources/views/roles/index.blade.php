@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading"> <h1><i class="fa fa-key"></i> Roles</h1></div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Role</th>
                            <th>Permissions</th>
                            <th>Operation</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td> @if($role->name != 'Admin') {{  $role->permissions()->pluck('name')->implode(' ') }} @endif</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                            <td>
                                @if($role->name != 'Admin')
                                    <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>
                                    <form action="{{ route('roles.destroy',$role->id) }}" method="DELETE">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="{{ url('roles/create') }}" class="btn btn-success pull-right">Add Role</a>
            </div>
        </div>
    </div>
@endsection