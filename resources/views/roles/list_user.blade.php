@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Developer !</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ route('listUser') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Name" />
				    <button type="submit" class="btn btn-primary">Submit</button>
				</div>
        	</form>

        	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Role</th>
						<th>Update_at</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<!-- <td><a href="/roles/{{$r->id}}">{{$i}}</a></td>
							<td><a href="/roles/{{$r->id}}">{{$r->name}}</a></td> -->
							<td>{{$r->id}}</td>
							<td>{{$r->name}}</td>
							<td>{{$r->email}}</td>
							<td>@if(count($r->roles ) >0 ) {{ $r->roles->first()->name }} @endif</td>
							<td>{{$r->updated_at}}</td>
							<td>
								<a href="/edit_user/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
							</td>

						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="9">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
