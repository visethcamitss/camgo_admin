
@extends('layouts.app')
@section('js')
    <link rel="stylesheet" href="{{ asset('css/wallet-style.css') }}">
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $('.date-own').datepicker({
            minViewMode: 1,
            format: 'yyyy-mm'
        });
    </script>
@endsection

@section('content')
    <div class="container-fluid">
      <div class="panel panel-primary" id="top">
          <div class="panel-heading">Driver iTsumo Luy Report!</div>

          <form action="{{ url('/driver_itsumo_luy/') }}" method="get" id="date-query">
              <div class="form-group" style="margin: 30px">
                  <div class="input-group" style="width: 300px;">
                      <input name="date" class="date-own form-control" value="{{$date}}" style="width: 100%; margin-bottom: 0" placeholder="Pick month">
                      <span onclick="document.getElementById('date-query').submit()" type="submit" class="input-group-addon btn btn-send">Go</span>
                  </div>
              </div>
          </form>

          <div class="btn btn-info" style="margin-left: 30px">
            <a href="#promotion"><b>GoTo Promotion report >> </b></a>
          </div>

          <div class="panel-total" id="taxi_fare">
              <div class="total-container">
                  <div class="total-label">
                      Total riding paid with itsumo Luy for {{$month}} = {{$totalFare}} KHR
                  </div>
              </div>
          </div>

          <div class="panel-body">
              <table class="table">
                <thead>
                  <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Driver</th>
                      <th>Update Amount (KHR)</th>
                      <th>New Balance (KHR)</th>
                      <th>Action</th>
                      <th>Memo</th>
                  </tr>
                </thead>

                <tbody>
                  @foreach($ridingFare as $rf)
                      <?php $i++?>
                      <tr>
                          <td><a href="/taxis/{{$rf->tid}}">{{$i}}</a></td>
                          <td>{{$rf->created_at}}</td>
                          <td><u><a href="/taxis/{{$rf->tid}}">{{$rf->driver_name}}</a></u></td>
                          <td>{{$rf->update_value}}</td>
                          <td>{{$rf->remain_value}}</td>
                          <td>{{$rf->actor}}</td>
                          <td></td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-total" id="five_person">
              <div class="total-container">
                  <div class="total-label">
                      Total 5% for {{$month}} = {{$totalCommision}} KHR
                  </div>
              </div>
          </div>
          <div class="panel-body">
              <table class="table">
                  <thead>
                  <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Driver</th>
                      <th>Update Amount (KHR)</th>
                      <th>New Balance (KHR)</th>
                      <th>Action</th>
                      <th>Memo</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($commision as $c)
                      <?php $i++?>
                      <tr>
                          <td><a href="/taxis/{{$c->tid}}">{{$i}}</a></td>
                          <td>{{$c->created_at}}</td>
                          <td><u><a href="/taxis/{{$c->tid}}">{{$c->driver_name}}</a></u></td>
                          <td>{{$c->update_value}}</td>
                          <td>{{$c->remain_value}}</td>
                          <td>{{$c->actor}}</td>
                          <td></td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
          </div>
      </div>
      <div class="panel panel-default">
          <div class="panel-total" id="promotion">
              <div class="total-container">
                  <div class="total-label" title="sum update amount">
                      Total promotion for {{$month}} = {{$totalPromotion}} KHR
                  </div>
              </div>
          </div>
          <div class="btn btn-info" style="margin-left: 30px">
            <a href="#taxi_fare"><b>GoTo 5% fare report >> </b></a>
          </div>
          <div class="panel-body">
              <table class="table">
                  <thead>
                  <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Driver</th>
                      <th>Update Amount (KHR)</th>
                      <th>New Balance (KHR)</th>
                      <th>Action</th>
                      <th>Memo</th>
                  </tr>
                  </thead>

                  <tbody>
                  @foreach($promotion as $r)
                      <?php $i++?>
                      <tr>
                          <td><a href="/taxis/{{$r->tid}}">{{$i}}</a></td>
                          <td>{{$r->created_at}}</td>
                          <td><u><a href="/taxis/{{$r->tid}}">{{$r->driver_name}}</a></u></td>
                          <td>{{$r->update_value}}</td>
                          <td>{{$r->remain_value}}</td>
                          <td>{{$r->actor}}</td>
                          <td></td>
                      </tr>
                  @endforeach
                  </tbody>

                  <tfoot>
                  <td colspan="1" >
                    <div class="btn btn-danger">
                      <a href="#top"><b>Back to Top</b></a>
                    </div>
                  </td>
                  </tfoot>
              </table>
          </div>
      </div>
    </div>
@endsection
