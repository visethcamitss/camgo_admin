@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Summary!</div>
        <div class="panel-body" id="charts"></div>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!-- <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div> -->
<script type="text/javascript">

Highcharts.chart('charts', {
    title: {
        text: 'Monthly Report'
    },
    xAxis: {
        categories: <?php echo json_encode($months); ?>
    },
    labels: {
        items: [{
            html: 'Total',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Riding',
        data: <?php echo json_encode($riding) ?>
    }, {
        type: 'column',
        name: 'Service Charge',
        data: <?php echo json_encode($charge) ?>
    }, {
        type: 'pie',
        name: 'Total',
        data: <?php echo json_encode($total) ?>,
        center: [80, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});
</script>
@endsection
