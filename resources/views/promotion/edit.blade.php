<?php
    $active = [ 1,2];
    if($res->active == 2)$old_active = 1 ;
    else $old_active = 0; 
?>
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
            <div class="panel panel-heading">
                Edit Promotion
            </div>
            <div class="panel panel-body">
                <form class="form-horizontal" method="POST" action = "{{ url('/promotion/update/') }}/{{$res->id}}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('name') ? 'has-error': ''}}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{old('name',$res->name)}}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-md-4 control-label">Code</label>
                        <div class="col-md-6">
                            <input id="code" type="text" class="form-control" name="code" value="{{ old('code', $res->code) }}" disabled>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                        <label for="active" class="col-md-4 control-label">Active</label>
                        <div class="col-md-6">
                            <select class="form-control" name="active">
                                @foreach($active as $k => $v)
                                    <option  value="{{$k}}" @if($old_active == $k)selected @endif >{{  $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                        <label for="value" class="col-md-4 control-label">Value</label>
                        <div class="col-md-6">
                            <input id="value" type="text" class="form-control" name="value" value="{{ old('value', $res->value) }}" required autofocus>
                        </div>
                        @if($errors->has('value'))
                            <span class="help-block"> <strong>{{$errors->first('value')}}</strong> </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-4 control-label">Description</label>
                        <div class="col-md-6">
                            <input id="description" type="text" class="form-control" name="description" value="{{ old('description', $res->description) }}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                        <label for="start_date" class="col-md-4 control-label">Start Date</label>
                        <div class="col-md-6 input-group date " data-provide="datepicker" data-date-format="yyyy-mm-dd">
                          <input type="text" id="start_date" name="start_date" class="form-control " style="margin:0;" value="{{ old('start_date', $res->start_date) }}" placeholder="Start date" required autofocus>
                          <div class="input-group-addon" >
                              <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('expire_date') ? ' has-error' : '' }}">
                        <label for="expire_date" class="col-md-4 control-label">Expire Date</label>
                        <div class="col-md-6 input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                          <input type="text" id="expire_date" name="expire_date" class="form-control " style="margin:0;" value="{{ old('expire_date', $res->expire_date) }}" placeholder="Expire Date" required autofocus>
                          <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th" ></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary" onclick="sentMessage()">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
