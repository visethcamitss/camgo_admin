@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Promotion</div>
        <div class="panel-body">
            <a href="/promotion/create" class="btn btn-primary">Add Promotion</a>
        <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Value</th>
                        <th>Active</th>
                        <th>Description</th>
                        <th>Start date</th>
                        <th>Expire Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($res as $r)
                        <?php $i++?>
                        <tr>
                            <td>{{$r->id}}</td>
                            <td>{{$r->name}}</td>
                            <td>{{$r->code}}</td>
                            <td>{{$r->value}}</td>
                            <td>{{$r->active}}</td>
                            <td>{{$r->description}}</td>
                            <td>{{$r->start_date}}</td>
                            <td>{{$r->expire_date}}</td>
                            <td><a href="/promotion/edit/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
