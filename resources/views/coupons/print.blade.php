<style>
	.page-break {
		page-break-after: always;
		background-color: #e8ffe8;
		border: 2px dotted #88aa88;
		padding: 10px;
		margin-left: -15px;
		margin-right: -15px;
		margin-top: -10px;
	}

	.title {
		font-size: 12px;
		font-style: bold;
	}
	.note {
		font-size: 13px;
	}
	.trim {
		border: 1px solid #e8f8e8;
	}
</style>

<div class="containter">
	@foreach($coupons as $c)
	<div class='page-break' align="center">
		<br/>
		<img src="https://mbr.itsumotaxi.com/public/img/itsumo_.png" width="35" align="center" />
		<p class="title">
			ITSUMO TAXI
		</p>
		<p>
			{{$c->value. $c->currency}}
		</p>
		<?php
			$p = "https://mbr.itsumotaxi.com/public/cu/";
			$i++;
		?>
		<img src="{{$p.$c->image}}" width="110" />
		<br/>
		<p class="note">
			This coupon is for driver only.
		</p>
		<br/>
	</div>
	@endforeach
</div>
