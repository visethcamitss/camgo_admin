@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Coupons!
		</div>
		<div class="panel-body">

			<form class="form-inline" role="form" method="POST" action="https://mbr.itsumotaxi.com/create_coupon">
				<!-- <div class="form-group"> -->
					<span>Create Coupon:</span>
					<input type="text" class="form-control" name="createQuantity" value="{{$keyword}}" placeholder="Quantity" />
					<select class="form-control" name="couponValue">
						<option value="10000">10,000 KHR</option>
						<option value="20000">20,000 KHR</option>
						<option value="40000">40,000 KHR</option>
						<option value="80000">80,000 KHR</option>
					</select>
					<button type="submit" class="btn btn-primary">
						Create Coupon
					</button>
				<!-- </div> -->
			</form>

			<form class="form-inline" role="form" method="GET" action="{{ url('/coupons/') }}">
				<div class="form-group">
					<span>Filter By:</span>
					<select class="form-control" name="status">
						<?php $i=0; ?>
						@foreach($status as $s)
							<?php if($i == $selected_status){  ?>
								<option value="{{$i}}" selected="selected">{{$s}}</option>
							<?php }else {?>
								<option value="{{$i}}">{{$s}}</option>
							<?php } $i++; ?>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<select class="form-control" name="values">
						@foreach($values as $v)
							<?php if($v == $selected_value){  ?>
								<option value="{{$v}}" selected="selected">{{$v}} KHR</option>
							<?php }else {?>
								<option value="{{$v}}">{{$v}} KHR</option>
							<?php } ?>
						@endforeach
					</select>
				<button type="submit" class="btn btn-primary">GO</button>
				</div>
			</form>

			<form class="form-inline" role="form" method="GET" action="{{ url('/coupon/print/') }}">
				<span>Print Option:</span>
				<select class="form-control" name="printValues">
						<option value="10000">10,000 KHR</option>
						<option value="20000">20,000 KHR</option>
						<option value="40000">40,000 KHR</option>
						<option value="80000">80,000 KHR</option>
				</select>
				<input type="text" class="form-control" name="printQuantity" placeholder="Print Quantity" />
				<button type="submit" class="btn btn-primary">
					print preview
				</button>
			</form>

			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>coupon id</th>
						<th>value</th>
						<th>currency</th>
						<th>coupon image</th>
						<th>status</th>
						<th>is delete</th>
						<th>updated at</th>
						<th>created at</th>
						<th>Print</th>
					</tr>
				</thead>

				<tbody>
					@foreach($res as $r)
					<?php $i++?>
					<tr>
						<td>{{$i}}</td>
						<td>{{$r->coupon_id}}</td>
						<td>{{$r->value}}</td>
						<td>{{$r->currency}}</td>
						<td>{{$r->image}}</td>
						<td> <?php if($r->status == 3){ ?>
							used
						<?php }else if($r->status == 2){ ?>
							printed
						<?php }else echo "available"; ?> </td>
						<td>{{$r->is_delete}}</td>
						<td>{{$r->updated_at}}</td>
						<td>{{$r->created_at}}</td>
						<td> <?php if($r->status < 2){ ?> <a href="/coupon/printone/{{$r->coupon_id}}">Print</a> <?php } ?> </td>
					</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="12">
					<div class="page pull-right">
						<?php echo $res -> appends(['keyword' => Request::get('keyword')]) -> render(); ?>
					</div></td>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@endsection
