<header class="header fixed-top clearfix">
    <!-- logo start -->
    <div class="brand">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars"></div>
        </div>
        <div class="company_name">
            <h1 class="text-center" style="color:white">CamGo</h1>
        </div>
    </div>
    <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
            @if(Auth::check() && ($user = Auth::user()))
                @if($user->role_id == 1)
                    <li><a href="{{ route('pwadmin') }}">AP</a></li>
                    <li><a href="{{ route('ip_range') }}">IP</a></li>
                    <li><a href="{{ route('user_activity') }}">Log</a></li>
                    <li><a href="{{ route('db_backup') }}">DB</a></li>
                @elseif($user->role_id == 2)
                    <li><a href="{{ route('ip_range') }}">IP</a></li>
                    <li><a href="{{ route('user_activity') }}">Log</a></li>
                    <li><a href="{{ route('db_backup') }}">DB</a></li>
                @endif
            @endif
        </ul>
    </div>
    <div class="top-nav clearfix">
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">
          
           
        </ul>
        <!--search & user info end-->
    </div>
</header>