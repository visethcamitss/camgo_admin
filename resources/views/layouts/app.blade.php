<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="FIGIX">
    <meta name="_token" id="_token" value="{{csrf_token()}}" content="{{csrf_token()}}" />
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/png" sizes="20x20">
    <title>Itsumo Admin</title>

    <!--Core CSS -->

    <link href="{{ asset('theme/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/js/bootstrap-datetimepicker/css/datetimepicker.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/style-responsive.css') }}" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>

    <script src="{{ asset('theme/js/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('theme/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript">
        function isEmpty(val){
            return (val === undefined || val == null || val.length <= 0) ? true : false;
        }
    </script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="{{ asset('theme/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    @yield('css')
</head>
<body>
<section  id="container">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!--sidebar start-->
     @include('layouts.sidebar')
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
         <!--main content goes here-->
         @yield('content')
        </section>
    </section>
    <!--main content end-->
</section>
<!--Core js-->

<script src="{{ asset('theme/js/jquery.js') }}"></script>

<script src="{{ asset('theme/bs3/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('theme/js/jquery.dcjqaccordion.2.7.js') }}"></script>

<script src="{{ asset('theme/js/jquery.scrollTo/jquery.scrollTo.min.js') }}"></script>

<script src="{{ asset('theme/js/jquery.nicescroll.js') }}" type="text/javascript"></script>

<script src="{{ asset('theme/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js') }}"></script>
<!--common script init for all pages-->
<script src="{{ asset('theme/js/scripts.js') }}"></script>

<!-- CHUCH add event tracker -->
<!-- <script src="{{ asset('js/bootbox.js') }}"></script> -->
<script type="text/javascript">
$(document).ready(function () {
    width_ = $( window ).width();
    if(width_ > 700) $('.sidebar-toggle-box .fa-bars').trigger('click');

    $(document.body).on('click', '.confirm-frm', function(){
        con = confirm("Are you sure do you want to save it?");
        if(con==false) return false;
    });
});

</script>

<!--script for this page-->
@yield('js')

</body>
</html>
