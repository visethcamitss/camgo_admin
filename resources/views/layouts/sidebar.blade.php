<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu goes here-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <!-- <li><a href="/call"><i class="fa fa-phone"></i>Call</a></li> -->
                <!-- <li><a href="/"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li> -->
                <li><a href="/taxis/live"><i class="glyphicon glyphicon-repeat"></i>Live</a></li>
                <!-- <li><a href="/promotion"><i class="fa fa-bell-o" aria-hidden="true"></i>Promotion</a></li> -->
                <!-- <li><a href="/companies"><i class="glyphicon glyphicon-briefcase"></i>Companies</a></li> -->
                <li><a href="/categories"><i class="fa fa-list"></i>Categories</a></li>
                <li><a href="/taxis"><i class="glyphicon glyphicon-plane"></i>Taxis</a></li>
                <li><a href="/userxs"><i class="glyphicon glyphicon-user"></i>Users</a></li>
                <!-- <li><a href="/user_booking"><i class="glyphicon glyphicon-user"></i>Booking</a></li> -->
                <li><a href="/user_requests"><i class="glyphicon glyphicon-user"></i>User's Request</a></li>
                <li><a href="http://camgo-topup.itsumotaxi.com/" target="_blank"><i class="glyphicon glyphicon-usd"></i>Top Up</a></li>
                <!-- <li><a href="/invoices"><i class="glyphicon glyphicon-file"></i>Invoices</a></li>
                <li><a href="/coupons"><i class="glyphicon glyphicon-file"></i>Coupon</a></li>
                <li><a href="/chart"><i class="glyphicon glyphicon-file"></i>Chart</a></li>
                <li><a href="/notification/taxi"><i class="glyphicon glyphicon-file"></i>Taxi notification</a></li>
                <li><a href="/notification/user"><i class="glyphicon glyphicon-file"></i>User notification</a></li>
                <li><a href="/settings"><i class="glyphicon glyphicon-file"></i>Setting</a></li>
                <li><a href="/charge_report"><i class="glyphicon glyphicon-usd"></i>Cash charge report</a></li>
                <li><a href="/user_itsumo_luy"><i class="glyphicon glyphicon-piggy-bank"></i>User iTsumo Luy</a></li>
                <li><a href="/driver_itsumo_luy"><i class="glyphicon glyphicon-piggy-bank"></i>Driver iTsumo Luy</a></li> -->
                <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i>Logout</a></li>
            </ul>
        </div>
    </div>
</aside>
<div id="url" style="display: none;">{{Request::segment(1)}}</div>
