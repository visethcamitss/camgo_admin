
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">Cash Charge Report!</div>
            <form action="{{ url('/charge_report/') }}" method="get" id="date-query">
                <div class="form-group" style="margin: 30px">
                    <div class="input-group" style="width: 300px;">
                        <input name="date" class="date-own form-control" value="{{$date}}" style="width: 100%; margin-bottom: 0" placeholder="Pick month">
                        <span onclick="document.getElementById('date-query').submit()" type="submit" class="input-group-addon btn btn-send">Go</span>

                    </div>
                </div>
            </form>
            <form action="{{ url('/export') }}" method="get" id="date-querys">
                <div class="form-group" style="margin: 30px">
                    <label for="" >Export as excel</label>
                    <div class="input-group" style="width: 300px;">
                        <input name="date" class="date-own form-control" value="{{$date}}" style="width: 100%; margin-bottom: 0" placeholder="Pick month">
                        <span onclick="document.getElementById('date-querys').submit()" type="submit" class="input-group-addon btn btn-send">Export</span>
                    </div>
                </div>
            </form>
            <div class="panel-total">
                <div class="total-container">
                    <div class="total-label" title="sum update amount">
                        Total charged for {{$month}} = <span id="total_value">{{$total}}</span> KHR
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?php $total_amount = 0;?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Update Amount (KHR)</th>
                        <th>New Balance (KHR)</th>
                        <th>Charged By</th>
                        <th>Memo</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($result))
                        @foreach($result as $r)
                        <?php
                                $name = 'N/A';
                                    $phone = '';
                                    $url = '';
                                    $action = '';
                                    if($r->user_type == 1 && !empty($taxi)){
                                        foreach($taxi as $t){
                                            if($t->taxi_id == $r->user_id){
                                                $name = $t->driver_name;
                                                $phone = $t->driver_phone;
                                                $url = '/taxis/'.$t->id;
                                                break;
                                            }
                                        }
                                    }else{
                                        continue;
                                    }
                                    $total_amount += $r->amount;
                                    $i++;
                                    // elseif($r->user_type == 2 && !empty($user)){
                                    //     foreach($user as $u){
                                    //         if($u->user_id == $r->user_id){
                                    //             $name = $u->name;
                                    //             $phone = $u->phone;
                                    //             $url = '/userxs/'.$u->id;
                                    //             break;
                                    //         }
                                    //     }
                                    // }

                                    switch ($r->action){
                                        case 1:
                                            $action = "Admin Mobile";
                                            break;
                                        case 2:
                                            $action = "Wing SDK";
                                            break;
                                        case 3:
                                            $action = "Pi Pay SDK";
                                            break;
                                        default:
                                            $action = "Admin Mobile";
                                    }
                            ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$r->time}}</td>
                                <td><u><a href="{{ $url}}">{{$name}}</a></u></td>
                                <td>{{$phone}}</td>
                                <td>{{$r->amount}}</td>
                                <td>{{$r->remain_balance}}</td>
                                <td>{{$action}}</td>
                                <td>{{$r->user_comment}}</td>
                            </tr>
                        @endforeach
                    @endif

                    @if(!empty($res) && count($res) > 0)
                        @foreach($res as $r)  {{--btn bg-danger--}}  {{--text-warning--}}
                            <?php $i++;
                                $total_amount += $r->update_value;
                            ?>
                            <tr>
                                <td><a href="/taxis/{{$r->tid}}">{{$i}}</a></td>
                                <td>{{$r->created_at}}</td>
                                <td><u><a href="/taxis/{{$r->tid}}">{{$r->driver_name}}</a></u></td>
                                <td>{{$r->phone}}</td>
                                <td>{{$r->update_value}}</td>
                                <td>{{$r->remain_value}}</td>
                                <td>{{$r->actor}}</td>
                                <td>{{$r->memo}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <td colspan="11">
                        <div class="page pull-right">
<!--                            --><?php //echo $res -> appends(['keyword' => Request::get('keyword')]) -> render(); ?>
                        </div>
                    </td>
                    </tfoot>
                </table>
                <input type="hidden" id="total_amount" value="{{number_format($total_amount)}}"/>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <link rel="stylesheet" href="{{ asset('css/wallet-style.css') }}">
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        $(function(){
            $("#total_value").html($("#total_amount").val());
                $('.date-own').datepicker({
                minViewMode: 1,
                format: 'yyyy-mm'
            });
        });
    </script>
@endsection
