@extends('layouts.app')
@section('content')

<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">User #{{$id}} </div>
        <div class="panel-body">
        	<table class="table">
                <th><img src="https://camgo-app.itsumotaxi.com/{{$res->avatar?$res->avatar:'noimage.gif'}}" width="64" height="64" /></th> <br>
                <!-- <tr>
                    <td><a href="/notification/user/{{$id}}" class="btn btn-success">Push Notification</a></td>
                </tr> -->
				<tr>
					<th width="150">Name</th>
					<td>{{$res->name }}</td>
				</tr>

				<tr>
					<th width="150">Phone</th>
					<td><a href="tel:{{$res->phone}}"​ <?php if($res->verify_status!=1){ echo " style='color:red'"; }?>>{{$res->phone}}</a></td>
				</tr>
				<tr>
					<th width="150">Email</th>
					<td><a href="mailto:{{$res->email}}"​>{{$res->email}}</a></td>
				</tr>
				<tr>
					<th width="150">Device Info</th>
					<td>{{$res->device_info}}</td>
				</tr>
				<!-- <tr>
					<th>Wallet</th>
					<td id="balance">{{$wallet->data->balance}} KHR</td>
				</tr> -->
			</table>

        </div>
    </div>
</div>
<div class="container-fluid">
            <div class="panel">
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1success" data-toggle="tab">User Histroy</a></li>
                        <!-- <li><a href="#tab2success" data-toggle="tab">Invoice</a></li> -->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1success">
                            <div class="panel">
                            <div class="panel-body">
                                <table class="table table-hover">
                    				<thead>
                    					<tr>
                    						<th>#</th>
                    						<th>Photo</th>
                    						<th>Name</th>
                    						<th>Request status</th>
                    						<th>Request date</th>
                    						<th>Request category</th>
                    						<th>Pickup</th>
                    						<th>Drop off</th>
                    						<th>Distance</th>
                    						<th>GO</th>
                    						<th>Estimated</th>
                    						<th>KHR</th>
                    					</tr>
                    				</thead>
                    				<tbody>
                    					@foreach($user_requests as $r)
                    						<?php
                    						$duration = '-';
                    						$distance = '-';
                    						if($r->invoice->created_at){
												if ($r->accept_date != null){
													$date1 = date_create($r->accept_date);
													$date2 = date_create($r->invoice->created_at);
													$diff = date_diff($date1,$date2);
													$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
												}
												$time = $r->invoice->duration;
												$hours = $time  / 3600;
												$minutes = ($time  % 3600) / 60;
												$seconds = $time  % 60;
												$duration = sprintf("%02d:%02d:%02d", $hours,$minutes,$seconds);
												$distance = number_format($r->invoice->distance/1000, 2) .' km';
											}elseif($r->accept_date != null && ( $r->request_status ==4 || $r->request_status ==5 ) ){
												$date1 =date_create($r->accept_date);
												$date2=date_create($r->cancel_date);
												$diff=date_diff($date1,$date2);
												$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
											}elseif ($r->request_status == 3 || $r->request_status == 8) {
												$date1 = date_create($r->accept_date);
												$date2 = date_create(NULL);
												$diff = date_diff($date1, $date2);
												$t0 = $diff->h.':'.$diff->i.':'.$diff->s;
											}else{
												$t0 = '';
											}
                    						?>
                    						<tr>
                    							<td><a href="#" class="href-id" id="{{$r->id}}" style="font-size:32px"><i class="glyphicon glyphicon-map-marker"></i></a></td>
                    							<td><a href="/taxis/{{$r->taxi->id}}"><img src="https://camgo-app.itsumotaxi.com/{{$r->taxi->driver_avatar?$r->taxi->driver_avatar:'noimage.gif'}}" width="64" height="64" /></a></td>
                    							<td><a href="/taxis/{{$r->taxi->id}}">{{$r->taxi->driver_name}}</a></td>
                    							<td><span style="color:{{$req_status[$r->request_status][1]}};">{{$req_status[$r->request_status][0]}}</span></td>
                    							<td>{{$r->request_date}}</td>
                    							<td>{{$r->taxi_category->category_name}}</td>
                    							<td>{{$t0?$t0:'-'}}</td>
                    							<td>{{$duration?$duration:'-'}}</td>
                    							<td>{{$distance?$distance:'-'}}</td>
                    							<td><a href="https://www.google.com.kh/maps/dir/{{$r->user_lat}},{{$r->user_long}}/{{$r->target_lat}},{{$r->target_long}}" target="_blank">GO</a></td>
                    							<td>{{$r->estimate_fare}}</td>
                    							<td>{{$r->invoice->total?:$r->description}}</td>
                    						</tr>
                    					@endforeach
                    				</tbody>
                    			</table>
                            </div>
                        </div>
                        </div>
                        <div class="tab-pane fade" id="tab2success"><br>
                            <div class="col-md-3">
                                <form action = "{{ url('send_invoice')}}" method="post" id="send_email_form">
                                    {{csrf_field()}}
                                    <input  class="form-control" type="text" name="email" id="email" placeholder="email" ></input>
                                    <input type="hidden" value="" name="invoiceId" id="invoice_id_lists"/>
                                    <input type="hidden" name="name" value="{{$res->name}}">
                                    <button  type="button" name="button" id="btn-sent" onclick="sentInvoice()" class="btn btn-success"> Sent Invoice</button>
                                </form>
                            </div>
                            <div class="panel">
                                <div class="panel-body">
                                    <table class="table table-hover" id="a">
                        				<thead>
                        					<tr>
                                                <th>#</th>
                                                <th>Invoice Number</th>
                        						<th>InvoiceID</th>
                        						<th>Distance</th>
                                                <th>Duration</th>
                                                <th>Initial Fare</th>
                                                <th>Service Charge</th>
                        						<th>Tips</th>
                        						<th>Total</th>
                        						<th>Payment Method</th>
                                                <th>Created Date</th>
                        					</tr>
                        				</thead>
                        				<tbody>
                                            <?php
                                                $totalAmount = 0;
                                                if(sizeof($invoices) > 0){
                                             ?>
                        					@foreach($invoices as $r)
                        						<tr >
                                                    <td> <input type="checkbox" id="invoiceId" name="checkbox-" value="{{$r->invoice->invoice_id}}"> </td>
                                                    <td>{{$r->invoice->invoice_number}}</td>
                        							<td >{{$r->invoice->invoice_id}}</td>
                        							<td >{{$r->invoice->distance}}</td>
                        							<td >{{$r->invoice->duration}}</td>
                        							<td >{{$r->invoice->initial_fare}}</td>
                        							<td >{{$r->invoice->service_charge}}</td>
                        							<td >{{$r->invoice->tip}}</td>
                        							<td >{{$r->invoice->total}}</td>
                        							<td >{{$r->invoice->payment_method}}</td>
                        							<td >{{$r->invoice->updated_at}}</td>
                        						</tr>
                                                <?php
                                                    $totalAmount = $totalAmount + $r->invoice->total
                                                ?>
                        					@endforeach
                                        <?php } ?>
                        				</tbody>
                        			</table>
                                </div>
                                <div class="panel-footer">
                                    <span style="color: red ">Total Pay Amount: <?php echo "  $totalAmount"; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<inpu type="hidden" value="" name="request_id" id="request_id" />
<button type="button" class="btn btn-primary btn-lg my-modal-btn hide" data-toggle="modal" data-target="#myModal">
</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:90%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    <script type="text/javascript">
        var invoices = [];
        function sentInvoice(){
            var checkboxList = document.getElementsByName('checkbox-');
            var l=checkboxList.length;
            for(var i =0; i<l ; i++){
                if(checkboxList[i].checked){
                    invoices.push(checkboxList[i].value);
                }
            }
            document.getElementById("invoice_id_lists").value = invoices.join();
            var form = document.getElementById("send_email_form");
            form.submit();
        }
    </script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-RREXt_zI7vIUJi3AhaT_gn6KlSMz8S0"></script>
	<script type="text/javascript">
		var gmarkers = [];
		$(document).ready(function () {
			var ck = 0;
			$(document.body).on('click', '.href-id', function(){
			   ck = 0;
			   $('#request_id').val($(this).attr('id'));
		       $('#myModal h4').text('#'+$('#request_id').val());
		       $('.my-modal-btn').trigger('click');
		       timeout(ck);
		       request_wallet();
		       return false;
		    });
		});
		function do_ajax(ck){
			var xhr;
			if(xhr){
				xhr.abort();
			}
			xhr = $.ajax({
               url:'/user_requests/'+$('#request_id').val(),
               data:'ck='+ck,
               success:function(res){
               	   obj = JSON.parse(res);
               	   if(obj.taxi.driver_avatar){
               	   		driver_avatar = obj.taxi.driver_avatar;
               	   }else{
               	   		driver_avatar = 'noimage.gif';
               	   }
               	   if(obj.user.avatar){
               	   		user_avatar = obj.user.avatar;
               	   }else{
               	   		user_avatar = 'noimage.gif';
               	   }
               	   html_ = '<div id="map" style="width: 100%; height: 600px; margin:auto;"></div>';
               	   html_ +='<div class="row third-block">';
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box text-center">';
								html_ +='<a href="/taxis/'+obj.taxi.id+'">';
									html_ +='<img src="https://camgo-app.itsumotaxi.com/'+driver_avatar+'" width="64" height="64" />';
								html_ +='</a>';
								html_ +='<p><strong>'+obj.taxi.driver_name+'</strong> - <a href="tel:'+obj.taxi.driver_phone+'">'+obj.taxi.driver_phone+'</a></p>';
							html_ +='</div>';
	               	   	html_ +='</div>';
	               	   	verity = '';
	               	   	if(obj.user.verify_status!=1){
	               	   		verity = 'style="color:red"';
	               	   	}
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box text-center">';
								html_ +='<a href="/userxs/'+obj.user.id+'">';
									html_ +='<img src="https://camgo-app.itsumotaxi.com/'+user_avatar+'" width="64" height="64" />';
								html_ +='</a>';
								html_ +='<p><strong><a href="/userxs/'+obj.user.id+'">'+obj.user.name+'</a></strong> - <a href="tel:'+obj.user.phone+'" '+verity+'>'+obj.user.phone+'</a></p>';
						    html_ +='</div>';
	               	   	html_ +='</div>';
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box">';
	               	   			html_ +='<p>Date: <strong>'+obj.res.request_date+'</strong></p>';
								html_ +='<p>Status: <strong id="res_status">'+obj.status+'</strong></p>';
								html_ +='<p>Pickup: <strong id="res_pickup">'+obj.pickup+'</strong></p>';
	               	   		html_ +='</div>';
	               	   	html_ +='</div>';
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box">';
								html_ +='<p>Distance: <strong id="res_distance">'+obj.distance+'</strong></p>';
								html_ +='<p>Time: <strong id="res_duration">'+obj.duration+'</strong></p>';
								html_ +='<p>Fare: <strong id="res_fare">'+obj.fare+'</strong></p>';
	               	   		html_ +='</div>';
	               	   	html_ +='</div>';
               	   html_ +='</div>';
               	   	if(ck==1){
               	   		$('#myModal .modal-body').html(html_);
	               	    var latlng_center = new google.maps.LatLng(obj.u_lat, obj.u_lot);
				        var mapOptions = {
				            center: latlng_center,
				            zoom: 17,
				            mapTypeId: google.maps.MapTypeId.ROADMAP
				        };
				        var el=document.getElementById("map");
				        map = new google.maps.Map(el, mapOptions);
			    	}else{
			    		for(i=0; i<gmarkers.length; i++){
					        gmarkers[i].setMap(null);
					    }
					    $('#myModal .modal-body #res_status').text(obj.status);
					    $('#myModal .modal-body #res_pickup').text(obj.pickup);
					    $('#myModal .modal-body #res_distance').text(obj.distance);
					    $('#myModal .modal-body #res_duration').text(obj.duration);
					    $('#myModal .modal-body #res_fare').text(obj.fare);
			    	}
			        if(obj.taxi.serving_status==1 && obj.taxi.priority > 0 && obj.taxi.status==1 && obj.taxi.reviewed==1){
		                $boo = 2;
		            }else if(obj.taxi.serving_status==2){
		                $boo = 1;
		            }else{
		                $boo = 0;
		            }
			        var latlng = new google.maps.LatLng(obj.taxi.latitude, obj.taxi.longitude);
			        var marker = new google.maps.Marker({
			            map: map,
			            position: latlng,
			            title: obj.taxi.driver_name+' - Tel: '+obj.taxi.driver_phone,
			            icon: 'https://camgo-app.itsumotaxi.com/images/map_'+$boo+'.png'
			        });
			        gmarkers.push(marker);
			        var latlng_ = new google.maps.LatLng(obj.res.taxi_lat, obj.res.taxi_long);
			        var marker_ = new google.maps.Marker({
			            map: map,
			            position: latlng_,
			            title: "taxi location",
			            icon: 'https://camgo-app.itsumotaxi.com/images/taxi_accept.png'
			        });
			        gmarkers.push(marker_);
			        var latlng = new google.maps.LatLng(obj.res.user_lat, obj.res.user_long);
			        var marker = new google.maps.Marker({
			            map: map,
			            position: latlng,
			            title: obj.user.name+' - Tel: '+obj.user.phone,
			            icon: 'https://camgo-app.itsumotaxi.com/images/pickup_32.png'
			        });
			        gmarkers.push(marker);
			        if(obj.user.last_lat && obj.user.last_long){
				        var latlng = new google.maps.LatLng(obj.user.last_lat, obj.user.last_long);
				        var marker = new google.maps.Marker({
				            map: map,
				            position: latlng,
				            title: obj.user.name+' - Tel: '+obj.user.phone,
				            icon: 'https://camgo-app.itsumotaxi.com/images/passenger_32.png'
				        });
			        }
			        gmarkers.push(marker);
			        if(obj.res.target_lat && obj.res.target_long){
				        var latlng = new google.maps.LatLng(obj.res.target_lat, obj.res.target_long);
				        var marker = new google.maps.Marker({
				            map: map,
				            position: latlng,
				            title: obj.user.name+' - Tel: '+obj.user.phone,
				            icon: 'https://camgo-app.itsumotaxi.com/images/dropoff_32.png'
				        });
			        }
			        gmarkers.push(marker);
					if(obj.travel_log.length > 0){
				        for(i=0; i<obj.travel_log.length; i++){
				        	console.log(obj.travel_log[i].latitude);
						    var latlng = new google.maps.LatLng(obj.travel_log[i].latitude, obj.travel_log[i].longitude);
					        var marker = new google.maps.Marker({
					            map: map,
					            position: latlng,
					            title: obj.travel_log[i].distance+' m , '+obj.travel_log[i].duration+' sec, '+obj.travel_log[i].sub_total+'riel',
					            icon: 'https://camgo-app.itsumotaxi.com/images/small_router.png'
					        });
					        gmarkers.push(marker);
						}
					}
			        google.maps.event.addListenerOnce(map, 'idle', function () {
					    google.maps.event.trigger(map, 'resize');
					});
               }
           });
		}
		function timeout(ck) {
			ck = ck + 1;
			do_ajax(ck);
		    setTimeout(function () {
		        timeout();
		    }, 5000);
		}
	</script>
@endsection
