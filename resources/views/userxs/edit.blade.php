@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Edit User!</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/userxs/update/') }}/{{$res->id}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $res->name) }}">
                            </div>
                        </div>
                        <div lass="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
                            <label for="company_id" class="col-md-4 control-label">Company Name</label>
                            <div class="col-md-6">
                                <select name="company_id" class="form-control">
                                    <option value="0">--</option>
                                    @foreach($company as $c)
                                        <option value="{{$c->id}}" @if($res->company_id == $c->id) selected @endif>{{$c->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('company_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div lass="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                            <label for="user_type" class="col-md-4 control-label">User Type</label>
                            <div class="col-md-6">
                                <input id="user_type" class="form-control" type="number" min="0" name="user_type" value="{{ old('user_type', $res->user_type) }}">
                            </div>

                        </div>
                        <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                            <label for="avatar" class="col-md-4 control-label">Photo</label>
                            <div class="col-md-6">
                                <input id="avatar" type="file" name="avatar">
                                <img src="https://camgo-app.itsumotaxi.com/{{$res->avatar?$res->avatar:images/noimage.gif}}" width="64" />
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('device_info') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Device info</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="device_info" disabled="disabled">{{ old('device_info', $res->device_info) }}</textarea>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('uuid') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Uuid</label>

                            <div class="col-md-6">
                                <input id="uuid" type="text" class="form-control" name="uuid" value="{{ old('uuid', $res->uuid) }}" disabled="disabled" />
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone', $res->phone) }}" >
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email', $res->email) }}" autofocus>

                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_lat') ? ' has-error' : '' }}">
                            <label for="last_lat" class="col-md-4 control-label">Last lat</label>

                            <div class="col-md-6">
                                <input id="last_lat" type="text" class="form-control" name="last_lat" value="{{ old('last_lat', $res->last_lat) }}" autofocus>

                                @if ($errors->has('last_lat'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_lat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_long') ? ' has-error' : '' }}">
                            <label for="last_long" class="col-md-4 control-label">Last long</label>

                            <div class="col-md-6">
                                <input id="last_long" type="text" class="form-control" name="last_long" value="{{ old('last_long', $res->last_long) }}" autofocus>

                                @if ($errors->has('last_long'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_long') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fail_counter') ? ' has-error' : '' }}">
                            <label for="fail_counter" class="col-md-4 control-label">Fail counter</label>

                            <div class="col-md-6">
                                <input id="fail_counter" type="text" class="form-control" name="fail_counter" value="{{ old('fail_counter', $res->fail_counter) }}" autofocus>

                                @if ($errors->has('fail_counter'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fail_counter') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('score') ? ' has-error' : '' }}">
                            <label for="score" class="col-md-4 control-label">Score</label>

                            <div class="col-md-6">
                                <input id="score" type="text" class="form-control" name="score" value="{{ old('score', $res->score) }}" autofocus>

                                @if ($errors->has('score'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('score') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('pass_code') ? ' has-error' : '' }}">
                            <label for="pass_code" class="col-md-4 control-label">Pass code</label>

                            <div class="col-md-6">
                                <input id="pass_code" type="text" class="form-control" name="pass_code" value="{{ old('pass_code', $res->pass_code) }}"  disabled="disabled" />
                                @if ($errors->has('pass_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pass_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('request_taxi') ? ' has-error' : '' }}">
                            <label for="request_taxi" class="col-md-4 control-label">Request taxi</label>

                            <div class="col-md-6">
                                <select name="request_taxi" class="form-control">
                                    <option value="1" @if($res->request_taxi==1) selected @endif>Yes</option>
                                    <option value="2" @if($res->request_taxi==2) selected @endif>No</option>
                                </select>

                                @if ($errors->has('request_taxi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('request_taxi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('travel_status') ? ' has-error' : '' }}">
                            <label for="travel_status" class="col-md-4 control-label">Travel status</label>

                            <div class="col-md-6">
                                <select name="travel_status" class="form-control">
                                    <option value="1" @if($res->travel_status==1) selected @endif>Yes</option>
                                    <option value="2" @if($res->travel_status==2) selected @endif>No</option>
                                </select>

                                @if ($errors->has('travel_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('travel_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('verify_status') ? ' has-error' : '' }}">
                            <label for="verify_status" class="col-md-4 control-label">Verify status</label>

                            <div class="col-md-6">
                                <select name="verify_status" class="form-control">
                                    <option value="1" @if($res->verify_status==1) selected @endif>Yes</option>
                                    <option value="2" @if($res->verify_status==2) selected @endif>No</option>
                                </select>

                                @if ($errors->has('verify_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('verify_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <select name="status" class="form-control">
                                    <option value="1" @if($res->status==1) selected @endif>Yes</option>
                                    <option value="2" @if($res->status==2) selected @endif>No</option>
                                </select>

                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary confirm-frm">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
