<?php
    $user_status = ['-- Select a type --','Free','Busy'];
    $old_user_status = Request::input('user_status',0);
?>
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Users!</div>
        <div class="panel-body">
            <form class="form-inline" role="form" method="GET" action="{{ url('/userxs/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="ID, Name , Phone" />
					<!-- <select class="form-control" name="user_status">
						@foreach($user_status as $k => $v)
							<option value="{{ $k }}" @if($old_user_status == $k) selected @endif>{{  $v }}</option>
						@endforeach
					</select> -->
				    <button type="submit" class="btn btn-primary">Submit</button>
				    <!-- <a class="btn btn-warning" href="/taxis/create">+ Add</a> -->
				</div>
        	</form>
        	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Photo</th>
						<th>Name</th>
                        <!-- <th>User Type</th> -->
						<th>Phone</th>
						<!-- <th>Travel status</th> -->
						<!-- <th>Unique ID</th> -->
						<!-- <th>status</th> -->
						<th>Updated</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res as $r)
						<tr>
							<td><a href="/userxs/{{$r->id}}">{{$r->id}}</a></td>
							<td><a href="/userxs/{{$r->id}}"><?php if($r->avatar){?><img src="https://camgo-app.itsumotaxi.com/{{$r->avatar}}" width="64" /><?php } ?></a></td>
							<td><a href="/userxs/{{$r->id}}">{{$r->name}}</a></td>
                            <!-- <td>{{$r->user_type}}</td> -->
							<td @if($r->verify_status!=1) style="color:red" @endif>{{$r->phone}}</td>
							<!-- <td>{{$r->travel_status}}</td> -->
							<!-- <td>{{$r->uuid}}</td> -->
							<!-- <td>{{$r->status}}</td> -->
							<td>{{$r->updated_at}}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<td colspan="9">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
