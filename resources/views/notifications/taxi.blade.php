@extends('layouts.app')
@section('js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

<!-- Create a tag that we will use as the editable area. -->
<!-- You can use a div tag as well. -->
<!-- Include jQuery lib. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script type="text/javascript">
	$(function() {
		$('textarea').froalaEditor({
			height:200,
			width :600
		});
		<?php if (!empty($taxi_id) ): ?>
			var taxi_id = {{ $taxi_id }};
			document.getElementById(taxi_id).checked = true;
			updateReceiverCheckBox(taxi_id);
		<?php endif; ?>

	});

	var urlServer = 'https://mb.itsumotaxi.com';
	// urlServer = 'http://192.168.1.80:3000';
	var socket = io(urlServer, {
		secure : true
	});
	var listTaxiId = [];
	socket.heartbeatTimeout = 6000;
	function sentMessage() {
		var param = {
			listId : document.getElementById("taxi_id").value,
			title_noti : document.getElementById("title").value,
			message_noti : document.getElementById("message").value
		}
		socket.emit("new_income_message", param);
	}

	function updateReceiver(id) {
		var isChecked = document.getElementById(id).checked;
		document.getElementById("taxi_id").value;
		if($res.length == 1){
			document.getElementById(id).checked = true;
		}
		if(isChecked){
			document.getElementById(id).checked = false;
		}
		else{
			document.getElementById(id).checked = true;
		}
		var taxiId = document.getElementById(id + "_val").value;
		if (!isChecked) {
			listTaxiId.push(taxiId);
			// add to list
		} else {
			// remove from list
			listTaxiId.splice(listTaxiId.indexOf(taxiId), 1);
		}
		document.getElementById("taxi_id").value = JSON.stringify(listTaxiId);
	}
	function updateReceiverCheckBox(id) {
                var isChecked = document.getElementById(id).checked;
                var taxiId = document.getElementById(id + "_val").value;
                if (isChecked) {
                        listTaxiId.push(taxiId);
                        // add to list
                }
                else {
                        // remove from list
                        listTaxiId.splice(listTaxiId.indexOf(taxiId), 1);
                }
                document.getElementById("taxi_id").value = JSON.stringify(listTaxiId);
       }
	function checkAll() {
		var isCheck = document.getElementById('checkall').checked;
		if (isCheck) {
			//document.getElementsByName('checkbox_').checked = true;
			var allT = document.getElementById('all_taxi_id').value;
			document.getElementById('taxi_id').value = allT;
			if(typeof allT ==='string'){
				listTaxiId = JSON.parse(allT);
			}else {
				listTaxiId = allT;
			}
		} else {
			listTaxiId = [];
			document.getElementById('taxi_id').value = [];
		}
		var allDoc  = document.getElementsByName('checkbox_');
		var lls = allDoc.length;
		for(var i =0; i<lls; i++){
			document.getElementsByName('checkbox_')[i].checked = isCheck;
		}
		document.getElementsByName('checkbox_')[0].checked = isCheck;
		var allId = document.getElementById('taxi_id').value;
	}
	function disableCheckBox(){
		var isChecked = document.getElementById('is_notify_ios').checked || document.getElementById('is_notify_android').checked || document.getElementById('is_notify_older').checked;
		if(isChecked){
			document.getElementById('checkall').disabled = true;
			$(".checkbox").attr('disabled',true);
		}else{
			document.getElementById('checkall').disabled = false;
			$(".checkbox").attr('disabled',false);
		}
	}
</script>
@endsection
@section('content')

<div class="container-fluid">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Add News Feed
			<?php $listId = array();?>
			@foreach($res as $rr)
				<?php
					$listId[] = $rr -> taxi_id;
				?>
			@endforeach
		</div>
		<div class="panel-body">
			<form class="form-inline" role="form" method="POST" action="https://mbr.itsumotaxi.com/push_taxi">
				<!-- <form class="form-inline" role="form" method="POST" action="http://192.168.1.80:3010/push_taxi"> -->
				<div class="form-group">
					<table class="table">
						<tr>
							<td colspan="5">
							<input type="text" maxlength="50" size="80" class="form-control" id = "title" name="title" placeholder="Title" />
							</td>
						</tr>
						<tr>
							<td colspan="5">
							<input type="text" maxlength="50" size="80" class="form-control" id="message" name="message" placeholder="short description" />
							</td>
						</tr>
						<tr>
							<td colspan="5">
							<textarea class="form-control" id="notification_body" name="notification_body"></textarea>
							</td>
						</tr>
						<tr>
							<td >
								<input type="checkbox" id="is_notify_ios" name="is_notify_ios" value="1" onclick="disableCheckBox()" /> Push All iOS<br/>
								<input type="checkbox" id="is_notify_android" name="is_notify_android" value="1" onclick="disableCheckBox()"/> Push All Android<br/>
								<input type="checkbox" id="is_notify_older" name="is_notify_older" value="1" onclick="disableCheckBox()"/> Older Version Only
								<input type="hidden" id="taxi_id" name="taxi_id" value="[]"/>
								<input type="hidden" id="all_taxi_id" name="all_taxi_id" value="{{json_encode($listId)}}"/>
							</td>
							<td>
							@foreach ($categories as $category)
								<input type="checkbox"  name="category_id" value="{{$category->id}}" /> {{$category->category_name}}<br/>
							@endforeach
							</td>

						</tr>
						<tr>
							<td colspan="5">
								<button type="sumit" class="btn btn-primary">Send</button>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<form class="form-inline" role="form" method="GET" action="{{ url('/notification/taxi') }}">
				<div class="form-group">
					<span>Send To:</span>
					<input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Name , os" />
					<button onClick="loadData()" type="submit" class="btn btn-primary">Find	</button>
				</div>
			</form>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th><input type="checkbox" id="checkall" onClick="checkAll()"></th>
						<th>Photo</th>
						<th>Driver name</th>
						<th>Driver phone</th>
						<th>Device</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res as $r)
					<?php	$i++;	

						$deviceInfo = '';
						if(!empty($r->device_info)){
							$dev = json_decode($r->device_info,true);
							if( !empty($dev['data']) ) {
								$deviceInfo = 'Android ('.$dev['data']['appversion'].')';
							}else{
								$deviceInfo = 'iOS('.$dev['appversion'].')';
							}
						}
					?>
					<tr>
						<td onclick="updateReceiver({{$r->id}})"><a href="/taxis/{{$r->id}}">{{$i}}</a></td>
						<td><input type="hidden" value = "{{$r->taxi_id}}" id="{{$r->id}}_val"><input type="checkbox" class="checkbox" name="checkbox_" id="{{$r->id}}" onclick="updateReceiverCheckBox({{$r->id}})"></td>
						<td onclick="updateReceiver({{$r->id}})"><a href="/taxis/{{$r->id}}"><img src="/uploads/{{$r->driver_avatar?$r->driver_avatar:'noimage.gif'}}" width="64" height="64" /></a></td>
						<td onclick="updateReceiver({{$r->id}})"><a href="/taxis/{{$r->id}}">{{$r->driver_name}}</a></td>
						<td onclick="updateReceiver({{$r->id}})">{{$r->driver_phone}}</td>
						<td onclick="updateReceiver({{$r->id}})">{{$deviceInfo}}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<td colspan="11">
					<div class="page pull-right">
						<?php echo $res -> appends(['keyword' => Request::get('keyword')]) -> render(); ?>
					</div></td>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@endsection
