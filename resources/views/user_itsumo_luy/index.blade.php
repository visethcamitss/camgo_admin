
@extends('layouts.app')
@section('js')
    <link rel="stylesheet" href="{{ asset('css/wallet-style.css') }}">
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $('.date-own').datepicker({
            minViewMode: 1,
            format: 'yyyy-mm'
        });
    </script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">User iTsumo Luy Account!</div>

            <form action="{{ url('/charge_report/') }}" method="get" id="date-query">
                <div class="form-group" style="margin: 30px">
                    <div class="input-group" style="width: 300px;">
                        <input name="date" class="date-own form-control" style="width: 100%; margin-bottom: 0" placeholder="Pick month">
                        <span onclick="document.getElementById('date-query').submit()" type="submit" class="input-group-addon btn btn-send">Go</span>
                    </div>
                </div>
            </form>

            <div class="panel-total">
                <div class="total-container">
                    <div class="total-label">
                        Grand balance :
                    </div>
                    <div class="total-data">
                        23452345
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {{--<form class="form-inline" role="form" method="GET" action="{{ url('/taxis/') }}">--}}
                {{--<div class="form-group">--}}
                {{--<input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Name , Phone" />--}}
                {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                {{--<a class="btn btn-warning" href="/taxis/create">+ Add</a>--}}
                {{--</div>--}}
                {{--</form>--}}

                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>ID</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Balance</th>
                        <th>Memo</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($res as $r)  {{--btn bg-danger--}}  {{--text-warning--}}
                    <?php $i++?>
                    <tr>
                        <td><a href="/taxis/{{$r->id}}">{{$i}}</a></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                    </tbody>

                    <tfoot>
                    <td colspan="11">
                        <div class="page pull-right">
<!--                            --><?php //echo $res -> appends(['keyword' => Request::get('keyword')]) -> render(); ?>
                        </div>
                    </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
