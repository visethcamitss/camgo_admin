@extends('layouts.app')
@section('content')
<div class="container-fluid">
<div class="panel panel-primary">
    <div class="panel-heading">Booking</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <form class="form-inline" role="form" method="GET" action="{{ url('/user_booking/') }}">
                    <div class="form-group">
                        <?php
                            $nb_progress_booking = $booking->where('booking_status','=',1)->count();
                            $nb_accept_booking = $booking->where('booking_status','=',3)->count();
                            $nb_u_cancel_booking = $booking->where('booking_status','=',2)->count();
                            $nb_booking_complete = $booking->where('booking_status','=',4)->count();
                         ?>
                        <button type="submit" class="btn btn-primary" >All Booking</button >
                        <button name = "booking_status" type="submit" class="btn btn-primary" value="1">Inprogress Booking (<?php echo $nb_progress_booking ;?>)</button >
                        <button name = "booking_status" type="submit" class="btn btn-primary" value="3"/>Accepted Booking (<?php echo $nb_accept_booking ;?>)</button >
                        <button name = "booking_status" type="submit" class="btn btn-primary" value="2"/>User Canceled Booking (<?php echo $nb_u_cancel_booking ;?>)</button >
                        <button name = "cancel_date" type="submit" class="btn btn-primary" value="1"/>Driver Canceled Booking (<?php echo $nb_d_cancel_booking ;?>)</button >
                        <button name = "booking_status" type="submit" class="btn btn-primary" value="4"/>Completed Booking (<?php echo $nb_booking_complete ;?>)</button >
                    </div>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top:40px">
            <table class="table">
                <thead>
                    <tr>
                        <th>User photo</th>
                        <th>User name</th>
                        <th>Driver photo</th>
                        <th>Driver name</th>
                        <th>Booking category</th>
                        <th>Booking status</th>
                        <th>Cancel Date</th>
                        <th>Booking date</th>
                        <th>Pickup Time</th>
                        <th>Pickup Location</th>
                        <th>Dropoff Location</th>
                        <th>Estimated</th>
                        <th>Tip</th>
                        <th>Map</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bookings as $b)
                        <?php $i++ ?>
                        <tr>
                            <td><a href="/userxs/{{$b->user->id}}"><img src="/uploads/{{$b->user->avatar?$b->user->avatar:'noimage.gif'}}" width="64" height="64" /></a></td>
                            <td>
                                <a href="/userxs/{{$b->user->id}}">{{$b->user->name}}</a>
                            </td>
                            @if($b->taxi)
                                <td><a href="/taxis/{{$b->taxi->id}}"><img src="/uploads/{{$b->taxi->driver_avatar?$b->taxi->driver_avatar:'noimage.gif'}}" width="64" height="64" /></a></td>
                                <td>{{$b->taxi->driver_name}}</td>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                            <td>{{$b->taxiCategory->category_name}}</td>
                            @if($b->booking_status == 3)
                                <td style="color:#42f4ee">Accepted
                                    <input type="hidden" id="uid{{$i}}" value="{{$b->user_id}}">
                                    <input type="hidden" id="bid{{$i}}" value="{{$b->booking_id}}">
                                    <span id="btn_cancel{{$i}}" class="btn btn-warning btn-xs" onClick=cancelProgressBooking({{$i}})>Cancel</span>
                                </td>
                            @elseif($b->booking_status == 1)
                                <td style="color:#41f45f">Available
                                    <input type="hidden" id="uid{{$i}}" value="{{$b->user_id}}">
                                    <input type="hidden" id="bid{{$i}}" value="{{$b->booking_id}}">
                                    <span id="btn_cancel{{$i}}" class="btn btn-warning btn-xs" onClick=cancelProgressBooking({{$i}})>Cancel</span>
                                 </td>
                            @elseif($b->booking_status == 2)
                                <td style="color:red">Canceled</td>
                            @elseif($b->booking_status == 4)
                                <td style="color: #00ffff">Completed</td>
                            @endif
                            <td style="color:red">{{$b->cancel_date}}</td>
                            <td>{{$b->request_date}}</td>
                            <td>{{$b->pickup_time}}</td>
                            <td>{{$b->pickup_location_name}}</td>
                            <td>{{$b->booking_location_name}}</td>
                            <td>{{$b->estimate_fare}}</td>
                            <td>{{$b->tip}}</td>
                            <td>
                                <a href="#myModal" class="btn btn-xs btn-success action" onclick="showMap({{$i}})"  data-toggle="modal" >
                                            <i class="fa fa-map-marker"></i> map</a>
                                            <input type="hidden" id="pLat{{$i}}" value="{{ $b->pickup_lat}}">
                                            <input type="hidden" id="pLong{{$i}}" value="{{ $b->pickup_long}}">
                                            <input type="hidden" id="tLat{{$i}}" value="{{ $b->booking_lat}}">
                                            <input type="hidden" id="tLong{{$i}}" value="{{ $b->booking_long}}">
                                <!-- <a href="#" class="href-id" id="{{$r->id}}" style="font-size:32px"><i class="glyphicon glyphicon-map-marker"></i></a> -->
                            </td>
                        </tr>
                    @endforeach

                </tbody>
                <tfoot>
                    <td colspan="12">
                        <div class="page pull-right">
                            <?php echo $bookings->appends([ 'keyword' => Request::get('keyword'), 'bookings' => Request::get('bookings') ])->render(); ?>
                        </div>
                    </td>
                </tfoot>
            </table>
    </div>
</div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <div id="map" style="width: 100%; height: 500px;"></div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-RREXt_zI7vIUJi3AhaT_gn6KlSMz8S0"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script type="text/javascript">
    function showMap(id){
        var pLat = $("#pLat"+id).val();
        var pLong = $("#pLong"+id).val();
        var tLat = $("#tLat"+id).val();
        var tLong = $("#tLong"+id).val();console.log(pLat,pLong,tLat,tLong);
        var address = 'Cambodia';
        var latlng_center = new google.maps.LatLng(pLat, pLong);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng_center,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
          'address': address
        },
        function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
                new google.maps.Marker({
                    position: creatLatObj(tLat, tLong),
                    map: map,
                    title: 'Drop Off Location',
                    icon: 'https://adm.itsumotaxi.com/images/dropoff_32.png',
                    animation: google.maps.Animation.BOUNCE
                });
                new google.maps.Marker({
                    position: creatLatObj(pLat, pLong),
                    map: map,
                    title: 'Pickup Location',
                    icon: 'https://adm.itsumotaxi.com/images/pickup_32.png',
                    animation: google.maps.Animation.BOUNCE
                });
                google.maps.event.trigger(map, 'resize');
                map.setCenter(creatLatObj(tLat, tLong));
            }
        });
    }
    function creatLatObj(lat,long){
        return new google.maps.LatLng(lat,long);
    }
</script>
@endsection
