@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Companies!</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ url('/companies/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" />
				    <button type="submit" class="btn btn-primary">Submit</button>
				    <a class="btn btn-warning" href="/companies/create">+ Add</a>
				</div>
        	</form>

        	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Logo</th>
						<th>Name</th>
                        <th>Email</th>
						<th>Code</th>
						<th>Password</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<td>{{$i}}</td>
							<td><img src="/uploads/{{$r->logo}}" width="64px" /></td>
							<td>{{$r->name}}</td>
                            <td>{{$r->email}}</td>
							<td>{{$r->code}}</td>
							<td>{{$r->password}}</td>
							<td>
								<a href="/companies/edit/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="/companies/destroy/{{$r->id}}" class="delete"><i class="glyphicon glyphicon-remove label-danger pull-right"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="10">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
