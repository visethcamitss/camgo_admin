@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Edit </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/category_fare/update/') }}/{{$res->id}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group{{ $errors->has('initial_fare') ? ' has-error' : '' }}">
                        <label for="initial_fare" class="col-md-4 control-label">Initial fare</label>
                        <div class="col-md-6">
                            <input id="initial_fare" type="number" min="0" step="any" class="form-control" name="initial_fare" value="{{ old('initial_fare', $res->initial_fare) }}" required>
                            @if ($errors->has('initial_fare'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('initial_fare') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('per_minute_fee') ? ' has-error' : '' }}">
                        <label for="per_minute_fee" class="col-md-4 control-label">Per minute fee</label>
                        <div class="col-md-6">
                            <input id="per_minute_fee" type="number" min="0"  class="form-control" name="per_minute_fee" value="{{ old('per_minute_fee', $res->per_minute_fee) }}" required>
                            @if ($errors->has('per_minute_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('per_minute_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('per_minute_fee') ? ' has-error' : '' }}">
                        <label for="per_minute_fee" class="col-md-4 control-label">Per km fee</label>

                        <div class="col-md-6">
                            <input id="per_km_fee" type="number" min="0"  class="form-control" name="per_km_fee" value="{{ old('per_km_fee', $res->per_km_fee) }}" required>

                            @if ($errors->has('per_km_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('per_km_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('flag_down_fee') ? ' has-error' : '' }}">
                        <label for="flag_down_fee" class="col-md-4 control-label">Flag down fee</label>

                        <div class="col-md-6">
                            <input id="flag_down_fee" type="number" min="0"  class="form-control" name="flag_down_fee" value="{{ old('flag_down_fee', $res->flag_down_fee) }}" required>

                            @if ($errors->has('flag_down_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('flag_down_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('pt_start') ? ' has-error' : '' }}">
                        <label for="pt_start" class="col-md-4 control-label">Pickup Time Start</label>
                        <div class="col-md-6">
                            <input id="pt_start" class="timepicker form-control pt-column" name="pt_start" value="{{ old('pt_start', $res->pt_start) }}" required>
                            @if ($errors->has('pt_start'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_start') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pt_end') ? ' has-error' : '' }}">
                        <label for="pt_end" class="col-md-4 control-label">Pickup Time End </label>
                        <div class="col-md-6">
                            <input id="pt_end" type="text" class="timepicker form-control pt-column" name="pt_end" value="{{ old('pt_end', $res->pt_end) }}" required>

                            @if ($errors->has('pt_end'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_end') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pt_start_two') ? ' has-error' : '' }}">
                        <label for="pt_start_two" class="col-md-4 control-label">Pickup Time Start Two</label>
                        <div class="col-md-6">

                            <input id="pt_start_two" type="text" class="timepicker form-control pt-column" name="pt_start_two" value="{{ old('pt_start_two', $res->pt_start_two) }}" required>

                            @if ($errors->has('pt_start_two'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_start_two') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pt_end_two') ? ' has-error' : '' }}">
                        <label for="pt_end_two" class="col-md-4 control-label">Pickup Time End Two</label>

                        <div class="col-md-6">
                            <input id="pt_end_two" type="text" class="timepicker form-control pt-column" name="pt_end_two" value="{{ old('pt_end_two', $res->pt_end_two) }}" required>

                            @if ($errors->has('pt_end_two'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_end_two') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('pt_initial_fare') ? ' has-error' : '' }}">
                        <label for="pt_initial_fare" class="col-md-4 control-label">Initial fare (PT)</label>

                        <div class="col-md-6">
                            <input id="pt_initial_fare" type="number" min="0" class="form-control pt-column" name="pt_initial_fare" value="{{ old('pt_initial_fare', $res->pt_initial_fare) }}" required>

                            @if ($errors->has('pt_initial_fare'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_initial_fare') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('pt_per_minute_fee') ? ' has-error' : '' }}">
                        <label for="pt_per_minute_fee" class="col-md-4 control-label">Per minute fee (PT)</label>

                        <div class="col-md-6">
                            <input id="pt_per_minute_fee" type="number" min="0" class="form-control pt-column" name="pt_per_minute_fee" value="{{ old('pt_per_minute_fee', $res->pt_per_minute_fee) }}" required>

                            @if ($errors->has('pt_per_minute_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_per_minute_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('pt_per_km_fee') ? ' has-error' : '' }}">
                        <label for="pt_per_km_fee" class="col-md-4 control-label">Per km fee (PT)</label>

                        <div class="col-md-6">
                            <input id="pt_per_km_fee" type="number" min="0" class="form-control pt-column" name="pt_per_km_fee" value="{{ old('pt_per_km_fee', $res->pt_per_km_fee) }}" required>

                            @if ($errors->has('pt_per_km_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_per_km_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('pt_flag_down_fee') ? ' has-error' : '' }}">
                        <label for="pt_flag_down_fee" class="col-md-4 control-label">Flag down fee (PT)</label>

                        <div class="col-md-6">
                            <input id="pt_flag_down_fee" type="number" min="0" class="form-control pt-column" name="pt_flag_down_fee" value="{{ old('pt_flag_down_fee', $res->pt_flag_down_fee) }}" required>

                            @if ($errors->has('pt_flag_down_fee'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pt_flag_down_fee') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="location" class="col-md-4 control-label">Place</label>
                        <div class="col-sm-6">
                            <?php $locations = config('category_location'); ?>
                            <select name="location" class="form-control">
                                @foreach($locations as $key => $value)
                                    <option value="{{$key}}" @if($res->location==$key) selected @endif> {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('center_lat') ? ' has-error' : '' }}">
                        <label for="center_lat" class="col-md-4 control-label">Center Lat</label>

                        <div class="col-md-6">
                            <input id="center_lat" type="text" step="any" min="0" class="form-control pt-column" name="center_lat" value="{{ old('center_lat', $res->center_lat) }}" required>

                            @if ($errors->has('center_lat'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('center_lat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('center_long') ? ' has-error' : '' }}">
                        <label for="center_long" class="col-md-4 control-label">Center Long</label>

                        <div class="col-md-6">
                            <input id="center_long" type="text" min="0" step="any" class="form-control" name="center_long" value="{{ old('center_long', $res->center_long) }}" required>

                            @if ($errors->has('center_long'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('center_long') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('distance') ? ' has-error' : '' }}">
                        <label for="distance" class="col-md-4 control-label">Distance</label>

                        <div class="col-md-6">
                            <input id="distance" type="number" min="0" class="form-control" name="distance" value="{{ old('distance', $res->distance) }}" required>

                            @if ($errors->has('distance'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('distance') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('is_default') ? ' has-error' : '' }}">
                        <label for="is_default" class="col-md-4 control-label">Is Default</label>

                        <div class="col-md-6">
                            <input id="is_default" type="number" min="0" class="form-control" name="is_default" value="{{ old('is_default', $res->is_default) }}" required>

                            @if ($errors->has('is_default'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('is_default') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
