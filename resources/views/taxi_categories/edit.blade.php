@extends('layouts.app')
@section('css')
    <style type="text/css">
        .pt-column{
            background-color: ghostwhite;
        }
    </style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Edit category!</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/categories/update/') }}/{{$res->id}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="category_name" class="col-md-4 control-label">Company</label>
                            <div class="col-md-6">
                                <select class="form-control" name="company_id">
                                    <option value="0">-</option>
                                    @foreach($companies as $r)
                                    <option value="{{ $r->id }}" @if($r->id==$company_id) selected @endif> {{ $r->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('company_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="category_name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="category_name" type="text" class="form-control" name="category_name" value="{{ old('category_name', $res->category_name) }}" required autofocus>
                                @if ($errors->has('category_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('seat_number') ? ' has-error' : '' }}">
                            <label for="seat_number" class="col-md-4 control-label">Seat number</label>
                            <div class="col-md-6">
                                <input id="seat_number" type="seat_number" class="form-control" name="seat_number" value="{{ old('seat_number', $res->seat_number) }}" required>
                                @if ($errors->has('seat_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('seat_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_charge_fix') ? ' has-error' : '' }}">
                            <label for="service_charge_fix" class="col-md-4 control-label">Charge Fix</label>
                            <div class="col-md-6">
                                <input id="service_charge_fix" type="service_charge_fix" class="form-control" name="service_charge_fix" value="{{ old('service_charge_fix', $res->service_charge_fix) }}" required>
                                @if ($errors->has('service_charge_fix'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_charge_fix') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_charge') ? ' has-error' : '' }}">
                            <label for="service_charge" class="col-md-4 control-label">Charge (%)</label>
                            <div class="col-md-6">
                                <input id="service_charge" type="service_charge" class="form-control" name="service_charge" value="{{ old('service_charge' *100, $res->service_charge *100) }}" required>
                                @if ($errors->has('service_charge'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_charge') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Currency</label>
                            <div class="col-md-6">
                                <input id="currency" type="currency" class="form-control" name="currency" value="{{ old('currency', $res->currency) }}" required>
                                @if ($errors->has('currency'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Image</label>
                            <div class="col-md-6">
                                <input id="image" type="file" name="image">
                                <img src="https://camgo-app.itsumotaxi.com/{{$res->image?$res->image:images/noimg.png}}" width="150" />
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Icon</label>
                            <div class="col-md-6">
                                <input id="icon" type="file" name="icon">
                                <img src="https://camgo-app.itsumotaxi.com/{{$res->icon?$res->icon:images/noimg.png}}" width="150" />
                                @if ($errors->has('icon'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('is_delete') ? ' has-error' : '' }}">
                            <label for="is_delete" class="col-md-4 control-label">Is delete</label>
                            <div class="col-md-6">
                                <select name="is_delete" class="form-control">
                                    <option value="2" @if($res->is_delete==2) selected @endif>Active</option>
                                    <option value="1" @if($res->is_delete==1) selected @endif>Inctive</option>
                                </select>
                                @if ($errors->has('is_delete'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('is_delete') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="location" class="col-md-4 control-label">Visible On</label>
                            <div class="col-md-6">
                                <?php
                                    $locations = config('category_location');
                                    $current = explode(',',$res->visible_on);
                                    foreach ($locations as $key => $value){
                                        echo '<input type="checkbox" name="location[]" value="'.$key.'" '.(in_array($key,$current) ? 'checked': '').' /> '.$value .'   '; if($key%2 ==0){ echo '<br/>';}
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">{{$res->category_name}}</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ url('/categories/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" />
				    <button type="submit" class="btn btn-primary">Submit</button>
				    <a class="btn btn-warning" href="{{url('category_fare/create',$res->id)}}">+ Add</a>
				</div>
        	</form>
        	<table class="table">
				<thead>
					<tr>
                        <th>#</th>
                        <th>Place</th>
						<th>Initial fare</th>
						<th>Per minute fee</th>
						<th>Per km fee</th>
						<th>Flag down fee</th>
                        <th>PT Start</th>
                        <th>PT End</th>
                        <th>PT Start Two</th>
                        <th>PT End Two</th>
                        <th>Initial fare</th>
						<th>Per minute fee</th>
						<th>Per km fee</th>
						<th>Flag down fee</th>
                        <th>Distance</th>
                        <th>Center Lat</th>
                        <th>Center Long</th>
                        <th>Is Default</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res1 as $r)
						<?php $i++?>
						<tr>
                            <td>{{$r->id}}</td>
                            <td>{{ $r->is_default != 1 ? $locations[$r->location] : 'Default' }}</td>
							<td>{{$r->initial_fare}}</td>
							<td>{{$r->per_minute_fee}}</td>
							<td>{{$r->per_km_fee}}</td>
							<td>{{$r->flag_down_fee}}</td>
                            <td class="pt-column">{{$r->pt_start}}</td>
                            <td class="pt-column">{{$r->pt_end}}</td>
                            <td class="pt-column">{{$r->pt_start_two}}</td>
                            <td class="pt-column">{{$r->pt_end_two}}</td>
                            <td class="pt-column">{{$r->pt_initial_fare}}</td>
							<td class="pt-column">{{$r->pt_per_minute_fee}}</td>
							<td class="pt-column">{{$r->pt_per_km_fee}}</td>
							<td class="pt-column">{{$r->pt_flag_down_fee}}</td>
                            <td>{{$r->distance}}</td>
                            <td>{{$r->center_lat}}</td>
                            <td>{{$r->center_long}}</td>
                            <td>{{$r->is_default}}</td>
							<td>
								<a href="{{url('category_fare/edit/')}}/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="#" class="delete"><i class="glyphicon glyphicon-remove label-danger pull-right"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('.timepicker').datetimepicker({
        format: 'hh:ii:ss',
        minuteStep: 1,
        autoclose: true,
        minView: 0,
        maxView: 1,
        startView: 1
    });

</script>
@endsection
