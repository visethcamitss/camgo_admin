@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Create new category!</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/categories/store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="category_name" class="col-md-4 control-label">Company</label>

                            <div class="col-md-6">
                                <select class="form-control" name="company_id">
                                    <option value="0">-</option>
                                    @foreach($companies as $r)
                                    <option value="{{ $r->id }}"> {{ $r->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('company_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="category_name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="category_name" type="text" class="form-control" name="category_name" value="{{ old('category_name') }}" required autofocus>

                                @if ($errors->has('category_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('seat_number') ? ' has-error' : '' }}">
                            <label for="seat_number" class="col-md-4 control-label">Seat number</label>

                            <div class="col-md-6">
                                <input id="seat_number" type="seat_number" class="form-control" name="seat_number" value="{{ old('seat_number') }}" required>

                                @if ($errors->has('seat_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('seat_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('service_charge_fix') ? ' has-error' : '' }}">
                            <label for="service_charge_fix" class="col-md-4 control-label">Charge Fix</label>

                            <div class="col-md-6">
                                <input id="service_charge_fix" type="service_charge_fix" class="form-control" name="service_charge_fix" value="{{ old('service_charge_fix', $res->service_charge_fix) }}" required>

                                @if ($errors->has('service_charge_fix'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_charge_fix') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_charge') ? ' has-error' : '' }}">
                            <label for="service_charge" class="col-md-4 control-label">Charge (%)</label>

                            <div class="col-md-6">
                                <input id="service_charge" type="service_charge" class="form-control" name="service_charge" value="{{ old('service_charge', $res->service_charge) }}" required>

                                @if ($errors->has('service_charge'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_charge') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Currency</label>

                            <div class="col-md-6">
                                <input id="currency" type="currency" class="form-control" name="currency" required>

                                @if ($errors->has('currency'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Image</label>

                            <div class="col-md-6">
                                <input id="image" type="file" name="image" required>

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="icon" class="col-md-4 control-label">Icon</label>

                            <div class="col-md-6">
                                <input id="icon" type="file" name="icon" required>

                                @if ($errors->has('icon'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('is_delete') ? ' has-error' : '' }}">
                            <label for="is_delete" class="col-md-4 control-label">Is delete</label>

                            <div class="col-md-6">
                                <select name="is_delete" class="form-control">
                                    <option value="2">Active</option>
                                    <option value="1">Inctive</option>
                                </select>

                                @if ($errors->has('is_delete'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('is_delete') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('.timepicker').datetimepicker({

        format: 'HH:mm:ss'

    });

</script>
@endsection
