@extends('layouts.app')
@section('css')
	<style type="text/css">
		.pt-column{
			background-color: ghostwhite;
		}
	</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Categories!</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ url('/categories/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" />
				    <button type="submit" class="btn btn-primary">Submit</button>
				    <a class="btn btn-warning" href="/categories/create">+ Add</a>
				</div>
        	</form>

        	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Image</th>
						<th>Name</th>
						<th>Seat number</th>
                        <th>Charge Fix</th>
                        <th>Charge (%)</th>
						<!-- <th>Initial fare</th>
						<th>Per minute fee</th>
						<th>Per km fee</th>
						<th>Flag down fee</th>
                        <th>PT Start</th>
                        <th>PT End</th>
                        <th>PT Start Two</th>
                        <th>PT End Two</th>
                        <th>Initial fare</th>
						<th>Per minute fee</th>
						<th>Per km fee</th>
						<th>Flag down fee</th>
                        <th>Rural Distance</th> -->
                        <th>Rural Charge (%)</th>
						<th>Currency</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<td>{{$i}}</td>
							<td><img src="https://camgo-app.itsumotaxi.com/{{$r->image}}" width="100" /></td>
							<td>{{$r->category_name}}</td>
							<td>{{$r->seat_number}}</td>
                            <td>{{$r->service_charge_fix}}</td>
                            <td>{{$r->service_charge *100}}</td>
							<!-- <td>{{$r->initial_fare}}</td>
							<td>{{$r->per_minute_fee}}</td>
							<td>{{$r->per_km_fee}}</td>
							<td>{{$r->flag_down_fee}}</td>
                            <td class="pt-column">{{$r->pt_start}}</td>
                            <td class="pt-column">{{$r->pt_end}}</td>
                            <td class="pt-column">{{$r->pt_start_two}}</td>
                            <td class="pt-column">{{$r->pt_end_two}}</td>
                            <td class="pt-column">{{$r->pt_initial_fare}}</td>
							<td class="pt-column">{{$r->pt_per_minute_fee}}</td>
							<td class="pt-column">{{$r->pt_per_km_fee}}</td>
							<td class="pt-column">{{$r->pt_flag_down_fee}}</td>
                            <td>{{$r->rural_distance}}</td> -->
                            <td>{{$r->rural_charge * 100}}</td>
							<td>{{$r->currency}}</td>
							<td><?php if ($r->is_delete == 1){ ?>
										<span class="btn bg-danger">delete</span>
								<?php } else { echo "<span class='btn bg-success'>active</span>";}?>
							</td>
							<td>
								<a href="/categories/edit/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="/categories/destroy/{{$r->id}}" class="delete"><i class="glyphicon glyphicon-remove label-danger pull-right"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="10">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
