@extends('layouts.app')
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script type="text/javascript">
	$(function() {
		$('textarea').froalaEditor({
			height:200,
			width :600
		});
	});
	var proUrl = 'https://mb.itsumotaxi.com';
	proUrl = 'http://192.168.1.80:3000';
	var socket = io(proUrl, {
		secure : true
	});
	socket.heartbeatTimeout = 6000;
	function sentMessage() {
		var e = document.getElementById("reviewed");
		var reviewStatus = e.options[e.selectedIndex].value;
		var taxiId = document.getElementById("taxi_id").value;
		var param = {}

		if(reviewStatus != 1){
			param.taxiId = taxiId;
			param.reviewed = reviewStatus;
			socket.emit("update_review", param);
		}
	}
</script>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Edit Taxi!
				</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/taxis/update/') }}/{{$res->id}}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('wallet') ? ' has-error' : '' }}">
							<label for="note" class="col-md-4 control-label">Note</label>

							<div class="col-md-6">
								<input id="note" type="text" class="form-control" name="note" value="{{ old('note', $res->note) }}"/>
								@if ($errors->has('note'))
								<span class="help-block"> <strong>{{ $errors->first('note') }}</strong> </span>
								@endif
							</div>
						</div>

						<!-- <input id="wallet_id" type="hidden" class="form-control" name="wallet_id" value="{{ $res->wallet_id }}"/>
						<div class="form-group{{ $errors->has('wallet') ? ' has-error' : '' }}">
							<label for="wallet_value" class="col-md-4 control-label">Wallet</label>

							<div class="col-md-6">
								<span class="form-control">{{ $res->value }}</span>
							</div>
						</div> -->

						<!-- <div class="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
							<label for="company_id" class="col-md-4 control-label">Company</label>

							<div class="col-md-6">
								<select class="form-control" name="company_id">
									<option value="0">-</option>
									@foreach($companies as $r)
									<option value="{{ $r->id }}" @if($r->id==$res->company_id) selected @endif> {{ $r->name}}</option>
									@endforeach
								</select>

								@if ($errors->has('company_id'))
								<span class="help-block"> <strong>{{ $errors->first('company_id') }}</strong> </span>
								@endif
							</div>
						</div> -->

						<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
							<label for="category_id" class="col-md-4 control-label">Category</label>

							<div class="col-md-6">
								<select class="form-control" id="category_id" name="category_id" onChange="check_allow_classic()">
									<option value="0">-</option>
									@foreach($categories as $r)
									<option value="{{ $r->id }}" @if($r->id==$res->category_id) selected @endif> {{ $r->category_name}}</option>
									@endforeach
								</select>
								@if ($errors->has('category_id'))
								<span class="help-block"> <strong>{{ $errors->first('category_id') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('allow_classic') ? ' has-error' : '' }}">
							<label for="allow_classic" class="col-md-4 control-label">Receive Classic Request</label>

							<div class="col-md-6">
								<select name="allow_classic" class="form-control">
									<option value="1" @if($res->allow_classic==1) selected @endif>Yes</option>
									<option value="2" @if($res->allow_classic==2) selected @endif>No</option>
								</select>

								@if ($errors->has('allow_classic'))
								<span class="help-block"> <strong>{{ $errors->first('allow_classic') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('taxi_id') ? ' has-error' : '' }}">
							<label for="taxi_id" class="col-md-4 control-label"></label>

							<div class="col-md-6">
								<input id="taxi_id" type="hidden" class="form-control" name="taxi_id" value="{{ old('taxi_id', $res->taxi_id) }}">

							</div>
						</div>

						<div class="form-group{{ $errors->has('priority_gift') ? ' has-error' : '' }}">
							<label for="priority_gift" class="col-md-4 control-label">Priority Gift</label>

							<div class="col-md-6">
								<input id="priority_gift" type="text" class="form-control" name="priority_gift" value="{{ old('priority_gift', $res->priority_gift) }}" >
							</div>
						</div>
						<div class="form-group{{ $errors->has('taxi_model') ? ' has-error' : '' }}">
							<label for="taxi_model" class="col-md-4 control-label">Taxi Model</label>

							<div class="col-md-6">
								<input id="taxi_model" type="text" class="form-control" name="taxi_model" value="{{ old('taxi_model', $res->taxi_model) }}" >
							</div>
						</div>

						<div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
							<label for="plate_number" class="col-md-4 control-label">Plate number</label>
							<div class="col-md-6">
								<input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ old('plate_number', $res->plate_number) }}" >
							</div>
						</div>
						<div class="form-group{{ $errors->has('driver_phone') ? ' has-error' : '' }}">
							<label for="driver_phone" class="col-md-4 control-label">Driver phone</label>

							<div class="col-md-6">
								<input id="driver_phone" type="text" class="form-control" name="driver_phone" value="{{ old('driver_phone', $res->driver_phone) }}" required autofocus>

								@if ($errors->has('driver_phone'))
								<span class="help-block"> <strong>{{ $errors->first('driver_phone') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('driver_phone_one') ? ' has-error' : '' }}">
							<label for="driver_phone_one" class="col-md-4 control-label">Driver phone (2)</label>
							<div class="col-md-6">
								<input id="driver_phone_one" type="text" class="form-control" name="driver_phone_one" value="{{ old('driver_phone_one', $res->driver_phone_one) }}" >
							</div>
						</div>

						<div class="form-group{{ $errors->has('driver_phone_two') ? ' has-error' : '' }}">
							<label for="driver_phone_two" class="col-md-4 control-label">Driver phone (2)</label>
							<div class="col-md-6">
								<input id="driver_phone_two" type="text" class="form-control" name="driver_phone_two" value="{{ old('driver_phone_two', $res->driver_phone_two) }}" >
							</div>
						</div>

						<div class="form-group{{ $errors->has('pass_code') ? ' has-error' : '' }}">
							<label for="pass_code" class="col-md-4 control-label">Pass code</label>

							<div class="col-md-6">
								<input id="pass_code" type="text" class="form-control" name="pass_code" value="{{ old('pass_code', $res->pass_code) }}" >
							</div>
						</div>

						<div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
							<label for="driver_name" class="col-md-4 control-label">Driver name</label>

							<div class="col-md-6">
								<input id="driver_name" type="text" class="form-control" name="driver_name" value="{{ old('driver_name', $res->driver_name) }}" required autofocus>

								@if ($errors->has('driver_name'))
								<span class="help-block"> <strong>{{ $errors->first('driver_name') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('driver_age') ? ' has-error' : '' }}">
							<label for="driver_age" class="col-md-4 control-label">Driver age</label>

							<div class="col-md-6">
								<input id="driver_age" type="text" class="form-control" name="driver_age" value="{{ old('driver_age', $res->driver_age) }}" >
							</div>
						</div>
						<!--
						<div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
						<label for="latitude" class="col-md-4 control-label">Latitude</label>

						<div class="col-md-6">
						<input id="latitude" type="text" class="form-control" name="latitude" value="{{ old('latitude', $res->latitude) }}" required autofocus>

						@if ($errors->has('latitude'))
						<span class="help-block">
						<strong>{{ $errors->first('latitude') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
						<label for="longitude" class="col-md-4 control-label">Longitude</label>

						<div class="col-md-6">
						<input id="longitude" type="text" class="form-control" name="longitude" value="{{ old('longitude', $res->longitude) }}" required autofocus>

						@if ($errors->has('longitude'))
						<span class="help-block">
						<strong>{{ $errors->first('longitude') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
						<label for="longitude" class="col-md-4 control-label">Rating</label>

						<div class="col-md-6">
						<input id="rating" type="text" class="form-control" name="rating" value="{{ old('rating', $res->rating) }}" required autofocus>

						@if ($errors->has('rating'))
						<span class="help-block">
						<strong>{{ $errors->first('rating') }}</strong>
						</span>
						@endif
						</div>
						</div> -->
						<!--
						<div class="form-group{{ $errors->has('wallet') ? ' has-error' : '' }}">
						<label for="wallet" class="col-md-4 control-label">Wallet</label>

						<div class="col-md-6">
						<input id="wallet" type="text" class="form-control" name="wallet" value="{{ old('wallet', $res->wallet) }}" required autofocus>

						@if ($errors->has('wallet'))
						<span class="help-block">
						<strong>{{ $errors->first('wallet') }}</strong>
						</span>
						@endif
						</div>
						</div> -->
						<div class="form-group{{ $errors->has('driver_avatar') ? ' has-error' : '' }}">
							<label for="driver_avatar" class="col-md-4 control-label">Driver photo</label>

							<div class="col-md-6">
								<input id="driver_avatar" type="file" name="driver_avatar">
								<img src="https://camgo-app.itsumotaxi.com/{{$res->driver_avatar?$res->driver_avatar:images/noimage.gif}}" width="64" />
							</div>
						</div>

						<div class="form-group{{ $errors->has('taxi_image') ? ' has-error' : '' }}">
							<label for="taxi_image" class="col-md-4 control-label">Taxi image</label>

							<div class="col-md-6">
								<input id="taxi_image" type="file" name="taxi_image">
								<img src="https://camgo-app.itsumotaxi.com/{{$res->taxi_image?$res->taxi_image:images/noimage.gif}}" width="64" />
							</div>
						</div>

						<div class="form-group{{ $errors->has('driver_social_id') ? ' has-error' : '' }}">
								<label for="driver_social_id" class="col-md-4 control-label">Identity Card</label>

								<div class="col-md-6">
										<input id="driver_social_id" type="file" name="driver_social_id">
										<img src="https://camgo-app.itsumotaxi.com/{{$res->driver_social_id?$res->driver_social_id:images/noimage.gif}}" alt="Identity Card" width="525px" height="300px">
								</div>
						</div>

						<div class="form-group{{ $errors->has('driver_licence') ? ' has-error' : '' }}">
								<label for="driver_licence" class="col-md-4 control-label">Driving License</label>

								<div class="col-md-6">
										<input id="driver_licence" type="file" name="driver_licence">
										<img src="https://camgo-app.itsumotaxi.com/{{$res->driver_licence?$res->driver_licence:images/noimage.gif}}" alt="Driving License" width="525px" height="300px">
								</div>
						</div>

						<div class="form-group{{ $errors->has('job_status') ? ' has-error' : '' }}">
							<label for="job_status" class="col-md-4 control-label">Job status</label>

							<div class="col-md-6">
								<select name="job_status" class="form-control">
									<option value="1" @if($res->job_status==1) selected @endif>Active</option>
									<option value="2" @if($res->job_status==2) selected @endif>Inctive</option>
								</select>

								@if ($errors->has('job_status'))
								<span class="help-block"> <strong>{{ $errors->first('job_status') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('serving_status') ? ' has-error' : '' }}">
							<label for="serving_status" class="col-md-4 control-label">Serving Status</label>
							<div class="col-md-6">
								<select name="serving_status" id="serving_status" class="form-control">
									<option value="1" @if($res->serving_status==1) selected @endif>Free</option>
									<option value="2" @if($res->serving_status==2) selected @endif>Busy</option>
								</select>
								@if ($errors->has('reviewed'))
								<span class="help-block"> <strong>{{ $errors->first('reviewed') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('reviewed') ? ' has-error' : '' }}">
							<label for="reviewed" class="col-md-4 control-label">Review status</label>

							<div class="col-md-6">
								<select name="reviewed" id="reviewed" class="form-control">
									<option value="1" @if($res->reviewed==1) selected @endif>Approved</option>
									<option value="2" @if($res->reviewed==2) selected @endif>Waiting</option>
									<option value="3" @if($res->reviewed==3) selected @endif>Reject</option>
								</select>

								@if ($errors->has('reviewed'))
								<span class="help-block"> <strong>{{ $errors->first('reviewed') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
							<label for="status" class="col-md-4 control-label">Status</label>

							<div class="col-md-6">
								<select name="status" class="form-control">
									<option value="1" @if($res->status==1) selected @endif>Active</option>
									<option value="2" @if($res->status==2) selected @endif>Inctive</option>
								</select>

								@if ($errors->has('status'))
								<span class="help-block"> <strong>{{ $errors->first('status') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary" onclick="sentMessage()">
									Save
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
