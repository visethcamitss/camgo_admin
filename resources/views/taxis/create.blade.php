@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Create new taxi!</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/taxis/store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- <div class="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
                            <label for="company_id" class="col-md-4 control-label">Company</label>

                            <div class="col-md-6">
                                <select class="form-control" name="company_id">
                                    <option value="0">-</option>
                                    @foreach($companies as $r)
                                        <option value="{{ $r->id.'-'.$r->name }}"> {{ $r->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('company_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-md-4 control-label">Category</label>

                            <div class="col-md-6">
                                <select class="form-control" name="category_id">
                                    <option value="0">-</option>
                                    @foreach($categories as $r)
                                    <option value="{{ $r->id }}"> {{ $r->category_name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('taxi_id') ? ' has-error' : '' }}">
                            <label for="taxi_id" class="col-md-4 control-label">Taxi ID</label>

                            <div class="col-md-6">
                                <input id="taxi_id" type="text" class="form-control" name="taxi_id" value="{{$taxi_id}}"​ readonly>

                                @if ($errors->has('taxi_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('taxi_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('taxi_model') ? ' has-error' : '' }}">
                            <label for="taxi_model" class="col-md-4 control-label">Taxi Model</label>

                            <div class="col-md-6">
                                <input id="taxi_model" type="text" class="form-control" name="taxi_model" value="{{ old('taxi_model') }}" required autofocus>

                                @if ($errors->has('taxi_model'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('taxi_model') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('plate_number') ? ' has-error' : '' }}">
                            <label for="plate_number" class="col-md-4 control-label">Plate number</label>

                            <div class="col-md-6">
                                <input id="plate_number" type="text" class="form-control" name="plate_number" value="{{ old('plate_number') }}" required autofocus>

                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_phone') ? ' has-error' : '' }}">
                            <label for="driver_phone" class="col-md-4 control-label">Driver phone</label>

                            <div class="col-md-6">
                                <input id="driver_phone" type="text" class="form-control" name="driver_phone" value="{{ old('driver_phone') }}" autofocus>

                                @if ($errors->has('driver_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_phone_one') ? ' has-error' : '' }}">
                            <label for="driver_phone_one" class="col-md-4 control-label">Driver phone (2)</label>

                            <div class="col-md-6">
                                <input id="driver_phone_one" type="text" class="form-control" name="driver_phone_one" value="{{ old('driver_phone_one') }}" autofocus>

                                @if ($errors->has('driver_phone_one'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_phone_one') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_phone_two') ? ' has-error' : '' }}">
                            <label for="driver_phone_two" class="col-md-4 control-label">Driver phone (3)</label>

                            <div class="col-md-6">
                                <input id="driver_phone_two" type="text" class="form-control" name="driver_phone_two" value="{{ old('driver_phone_two') }}" autofocus>

                                @if ($errors->has('driver_phone_two'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_phone_two') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pass_code') ? ' has-error' : '' }}">
                            <label for="pass_code" class="col-md-4 control-label">Pass code</label>

                            <div class="col-md-6">
                            	<input id="view_pass_code" type="text" class="form-control" name="pass_code" value="12345" required autofocus readonly>
                                <input id="pass_code" type="text" class="form-control" name="pass_code" value="{{ $default_passcode }}" style="visibility: hidden">
                                @if ($errors->has('pass_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pass_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
                            <label for="driver_name" class="col-md-4 control-label">Driver name</label>

                            <div class="col-md-6">
                                <input id="driver_name" type="text" class="form-control" name="driver_name" value="{{ old('driver_name') }}" required autofocus>

                                @if ($errors->has('driver_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_age') ? ' has-error' : '' }}">
                            <label for="driver_age" class="col-md-4 control-label">Driver age</label>

                            <div class="col-md-6">
                                <input id="driver_age" type="text" class="form-control" name="driver_age" value="{{ old('driver_age') }}" required autofocus>

                                @if ($errors->has('driver_age'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_age') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<!--
                        <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                            <label for="latitude" class="col-md-4 control-label">Latitude</label>

                            <div class="col-md-6">
                                <input id="latitude" type="text" class="form-control" name="latitude" value="{{ old('latitude') }}" required autofocus>

                                @if ($errors->has('latitude'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                            <label for="longitude" class="col-md-4 control-label">Longitude</label>

                            <div class="col-md-6">
                                <input id="longitude" type="text" class="form-control" name="longitude" value="{{ old('longitude') }}" required autofocus>

                                @if ($errors->has('longitude'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('longitude') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->

                        <!-- <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                            <label for="longitude" class="col-md-4 control-label">Rating</label>

                            <div class="col-md-6">
                                <input id="rating" type="text" class="form-control" name="rating" value="{{ old('rating') }}" required autofocus>

                                @if ($errors->has('rating'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('rating') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->
<!--
                        <div class="form-group{{ $errors->has('wallet_id') ? ' has-error' : '' }}">
                            <label for="wallet_id" class="col-md-4 control-label">Wallet</label>

                            <div class="col-md-6">
                                <input id="wallet_id" type="text" class="form-control" name="wallet_id" value="{{ old('wallet_id') }}" required autofocus>

                                @if ($errors->has('wallet_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('wallet_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->
                        <div class="form-group{{ $errors->has('driver_avatar') ? ' has-error' : '' }}">
                            <label for="driver_avatar" class="col-md-4 control-label">Driver photo</label>

                            <div class="col-md-6">
                                <input id="driver_avatar" type="file" name="driver_avatar" class="form-control">

                                @if ($errors->has('driver_avatar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_avatar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('taxi_image') ? ' has-error' : '' }}">
                            <label  class="col-sm-4 control-label">Taxi image</label>

                            <div class="col-sm-6">
                                <input id="taxi_image" type="file" name="taxi_image" class="form-control">

                                @if ($errors->has('taxi_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('taxi_image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_social_id') ? ' has-error' : '' }}">
                            <label for="driver_social_id" class="col-md-4 control-label">Identity Card</label>

                            <div class="col-md-6">
                                <input id="driver_social_id" type="file" name="driver_social_id" class="form-control">

                                @if ($errors->has('driver_social_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_social_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('driver_licence') ? ' has-error' : '' }}">
                            <label for="driver_licence" class="col-md-4 control-label">Driving License</label>

                            <div class="col-md-6">
                                <input id="driver_licence" type="file" name="driver_licence" class="form-control">

                                @if ($errors->has('driver_licence'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_licence') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group{{ $errors->has('job_status') ? ' has-error' : '' }}">
                            <label for="job_status" class="col-md-4 control-label">Job status</label>

                            <div class="col-md-6">
                                <select name="job_status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inctive</option>
                                </select>

                                @if ($errors->has('job_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('job_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <select name="status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inctive</option>
                                </select>

                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
