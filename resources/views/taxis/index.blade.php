<?php
    $taxi_status = ['-- Select a type --','Free','Busy','Active Status','inActive Status','Approved','Waiting Approve','Offline','Not Used >5 days','Out Off Balance'];
    $taxi_os = ['-- Select OS --','iOS','Android','Old Version iOS','Old Version Android'];
    $old_taxi_status = Request::input('taxi_status',0);
    $old_taxi_os = Request::input('taxi_os',0);
?>
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Taxis!</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ url('/taxis/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Name , Phone" />
					<!-- <select class="form-control" name="taxi_status">
						@foreach($taxi_status as $k => $v)
							<option value="{{ $k }}" @if($old_taxi_status == $k) selected @endif>{{  $v }}</option>
						@endforeach
					</select>

					<select class="form-control" name="taxi_os">
						@foreach($taxi_os as $k => $v)
							<option value="{{ $k }}" @if($old_taxi_os == $k) selected @endif>{{  $v }}</option>
						@endforeach
					</select> -->
				    <button type="submit" class="btn btn-primary">Submit</button>
				    <a class="btn btn-warning" href="/taxis/create">+ Add</a>
				</div>
        	</form>
        	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Photo</th>
						<th>Driver name</th>
						<th>Driver phone</th>
						<th>Category</th>
						<!-- <th>Taxi model</th> -->
						<!-- <th>Plate number</th> -->
						<!-- <th>Serving status</th> -->
						<th>Status</th>
						<th>Serving Priority</th>
						<th>Priority Gift</th>
						<th>Reviewed</th>
						<th>Device</th>
						<th>Updated at</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<td><a href="/taxis/{{$r->id}}">{{$i}}</a></td>
							<td><a href="/taxis/{{$r->id}}"><img src="https://camgo-app.itsumotaxi.com//{{$r->driver_avatar?$r->driver_avatar:images/noimage.gif}}" width="64" height="64" /></a></td>
							<td><a href="/taxis/{{$r->id}}">{{$r->driver_name}}</a></td>
							<td>{{$r->driver_phone}}</td>
							<td>
								<?php
								if ($r -> category_id == 2 && $r -> allow_classic == 1) { echo "<strong><span class='text-warning'>$r->category;</span></strong>";
								} else { echo $r -> category;
								}
							?>
							</td>
							
							<!-- <td><?php
								if ($r -> serving_status == 1) { echo "<span class='btn bg-success'>Free</span>";
								} else { echo "<span class='btn bg-warning'>busy</span>";
								}
 								?>
 							</td> -->
							<td>
                                <?php
    								if ($r -> status == 1) { echo "<span class='btn bg-success'>Active</span>";
    								} else { echo "<span class='btn bg-danger'>deleted</span>";}
                                ?>
                            </td>
							<td>
                                <?php
    								if ($r -> priority <= 0) {
                                        echo "<span class='btn bg-danger'>$r->priority</span>";
    								} else {
                                         echo "<span class='btn bg-success'>$r->priority</span>";
    								}
                                ?>
                            </td>
 							<td>{{$r->priority_gift}}</td>
							<td><?php
								if ($r -> reviewed == 1) { echo "<span class='btn bg-success'>Approved</span>";
								} else if ($r -> reviewed == 2) { echo "<span class='btn bg-danger'>Waiting...</span>";
								} else { echo "<span class='btn bg-danger'>Reject</span>";
								}
							?></td>
							<?php
								$deviceInfo = '';
								if(!empty($r->device_info)){
									$dev = json_decode($r->device_info,true);
									if($dev['os'] == "Android") {
										$deviceInfo = 'Android('.$dev['appversion'].')';
									}else{
										$deviceInfo = 'iOS('.$dev['appversion'].')';
									}
								}
								
							?>
							<td>{{ $deviceInfo }}</td>
							<td>{{$r->updated_at}}</td>
							<td>
								<a href="/taxis/edit/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="/taxis/destroy/{{$r->id}}" class="delete"><i class="glyphicon glyphicon-remove label-danger pull-right"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="11">
						<div class="page pull-right">
                        <?php
								$res->appends(Request::input());
							echo $res -> render();
						?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
