
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">

        	<a href="/taxis/live">All (<?php echo $res4?>)</a> |
        	<a href="/taxis/live?status=1">Available (<?php echo $res1?>)</a> |
        	<a href="/taxis/live?status=2">Busy (<?php echo $number_busy?>)</a> |
        	<a href="/taxis/live?status=4">Offline (<?php echo $res5?>)</a> |
        	<a href="/taxis/live?status=3">Out of balance (<?php echo $res3?>)</a>

        </div>
        <div class="panel-body">
        	<div id="map" style="width: 100%; height: 750px;"></div>

        </div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-RREXt_zI7vIUJi3AhaT_gn6KlSMz8S0"></script>
    <script src="https://cdn.klokantech.com/maptilerlayer/v1/index.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
            var latlng_center = new google.maps.LatLng(12.827081, 105.026091);
            var mapOptions = {
                center: latlng_center,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var el=document.getElementById("map");
            map = new google.maps.Map(el, mapOptions);

            <?php foreach($res as $r){
                if($r->serving_status==1 && $r->priority > 0 && $r->status==1 && $r->reviewed==1 && $r->job_status != 3){
                    $boo = 2;
                }elseif($r->serving_status==2){
                    $boo = 1;
                }
                elseif($r->serving_status==3){
                    $boo = 1;
                }else{
                    $boo = 0;
                }

                if($r->category_id==3){
                    $m = 'motor_';
                }elseif($r->category_id==2){
                    $m = 'tuktuk_';
                }else{
                    $m = 'car_';
                }
            ?>
                var latlng = new google.maps.LatLng(<?php echo $r->latitude?>, <?php echo $r->longitude?>);
                var marker = new google.maps.Marker({
                    map: map,
                    position: latlng,
                    title: '<?php echo $r->driver_name?> - Tel: <?php echo $r->driver_phone?>',
                    text: 'Tel: <?php echo $r->driver_phone?>',
                    icon: '/images/<?php echo $m.$boo?>.png',
                    url: '/taxis/<?php echo $r->id?>',
                });

                google.maps.event.addListener(marker, 'click', function() {
                    window.location.href = this.url;
                });
            <?php } ?>

            var geoloccontrol = new klokantech.GeolocationControl(map, 13);
		});
	</script>
@endsection
