@extends('layouts.app')
@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Edit Setting!
				</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/settings/update/') }}/{{$res->id}}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">Name</label>

							<div class="col-md-6">
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name', $res->name) }}" required autofocus>

								@if ($errors->has('priority_gift'))
								<span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
							<label for="value" class="col-md-4 control-label">Value</label>

							<div class="col-md-6">
								<?php if($res->code == 'F_U_ANDROID' || $res->code == 'F_D_ANDROID' || $res->code == 'F_U_IOS' || $res->code == 'F_D_IOS'){ ?>
									<select name="value" id="value" class="form-control">
										<option value="1" @if($res->value==1) selected @endif>Force update</option>
										<option value="2" @if($res->value==2) selected @endif>Self update</option>
									</select>
								<?php }else { ?>
									<input id="value" type="text" class="form-control" name="value" value="{{ old('value', $res->value) }}" required autofocus>
									@if ($errors->has('value'))
									<span class="help-block"> <strong>{{ $errors->first('value') }}</strong></span>
									@endif
								<?php } ?>
							</div>
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-md-4 control-label">Description</label>

							<div class="col-md-6">
								@if($res->code != 'INFO_FOR_USER')
									<input id="description" type="text" class="form-control" name="description" value="{{ old('description', $res->description) }}" required autofocus>
								@else
									<textarea class="form-control" id="description" name="description">{{ old('description', $res->description) }}</textarea>
								@endif
								@if ($errors->has('description'))
								<span class="help-block"> <strong>{{ $errors->first('description') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
							<label for="start_date" class="col-md-4 control-label">Start_date</label>

							<div class="col-md-6">
								<input id="start_date" type="text" class="form-control" name="start_date" value="{{ old('start_date', $res->start_date) }}" required autofocus>
								<span class="text-info"> Change start date only effect promotion setting</span>
								@if ($errors->has('start_date'))
								<span class="help-block"> <strong>{{ $errors->first('start_date') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
							<label for="end_date" class="col-md-4 control-label">end_date</label>

							<div class="col-md-6">
								<input id="end_date" type="text" class="form-control" name="end_date" value="{{ old('end_date', $res->end_date) }}" required autofocus>
								<span class="text-info"> Change start date only effect promotion setting</span>
								@if ($errors->has('end_date'))
								<span class="help-block"> <strong>{{ $errors->first('end_date') }}</strong> </span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
							<label for="active" class="col-md-4 control-label">Active</label>

							<div class="col-md-6">
								<select name="active" id="active" class="form-control">
									<option value="1" @if($res->reviewed==1) selected @endif>active</option>
									<option value="2" @if($res->reviewed==2) selected @endif>inactive</option>
								</select>

								@if ($errors->has('active'))
								<span class="help-block"> <strong>{{ $errors->first('active') }}</strong> </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary" onclick="sentMessage()">
									Save
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
	<!-- Include Editor JS files. -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>
	<script>
        $(function() {
            $('#description').froalaEditor({
                height:250,
                width :700
            });
        });
	</script>
@endsection