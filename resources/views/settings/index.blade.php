@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Setting</div>
        <div class="panel-body">
        <table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Value</th>
						<th>Description</th>
						<th>Code</th>
						<th>Active</th>
						<th>Start date</th>
						<th>End date</th>
            <th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<td>{{$r->id}}</td>
							<td>{{$r->name}}</td>
							<td><?php
                  if($r->code == 'F_U_ANDROID' || $r->code == 'F_D_ANDROID' || $r->code == 'F_U_IOS' || $r->code == 'F_D_IOS'){
                    if ($r->value == 1) {
                      echo "<span class='btn bg-danger'>Force Update</span>";
                    } else if($r->value == 2){
                      echo "<span class='btn bg-primary'>Self Update</span>";
                    }
                  }else{
                  ?>
                    {{$r->value}}
                  <?php } ?>
              </td>
							<td>{{$r->description}}</td>
							<td>{{$r->code}}</td>
							<td><?php
                  if ($r->active == 1) {
                    echo "<span class='btn bg-success'>YES</span>";
								  } else {
                    echo "<span class='btn bg-danger'>NO</span>";
								  }
                  ?>
              </td>
							<td>{{$r->start_date}}</td>
							<td>{{$r->end_date}}</td>
              <td>
								<a href="/settings/edit/{{$r->id}}" class="edit"><i class="glyphicon glyphicon-pencil"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="12">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
