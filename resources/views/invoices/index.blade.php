@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">Invoices!</div>
        <div class="panel-body">
        	<form class="form-inline" role="form" method="GET" action="{{ url('/invoices/') }}">
        		<div class="form-group">
				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Invoice number" />
				    <button type="submit" class="btn btn-primary">Submit</button>
				</div>
        	</form>

        	<table class="table">
				<thead>
					<tr>
						<th>invoice_number</th>
						<th>request_id</th>
						<th>duration</th>
						<th>distance</th>
						<th>payment_method</th>
						<th>initial_fare</th>
						<th>flag_down_fee</th>
						<th>per_minute_fee</th>
						<th>per_km_fee</th>
						<th>currency</th>
						<th>total</th>
						<th>service charge</th>
					</tr>
				</thead>

				<tbody>
					@foreach($res as $r)
						<?php $i++?>
						<tr>
							<td><a href="/invoices/email/{{$r->invoice_number}}" target="_blank">{{$r->invoice_number}}</a></td>
							<td>{{$r->request_id}}</td>
							<td>{{$r->duration}}</td>
							<td>{{$r->distance}}</td>
							<td>{{$r->payment_method}}</td>
							<td>{{$r->initial_fare}}</td>
							<td>{{$r->flag_down_fee}}</td>
							<td>{{$r->per_minute_fee}}</td>
							<td>{{$r->per_km_fee}}</td>
							<td>{{$r->currency}}</td>
							<td>{{$r->total}}</td>
							<td>{{$r->service_charge}}</td>
						</tr>
					@endforeach
				</tbody>

				<tfoot>
					<td colspan="12">
						<div class="page pull-right">
                        <?php echo $res->appends([ 'keyword' => Request::get('keyword') ])->render(); ?>
                    </div>
					</td>
				</tfoot>
			</table>
        </div>
    </div>
</div>
@endsection
