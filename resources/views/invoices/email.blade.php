<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>iTSumo Taxi</title>
</head>
<body style="padding:0; margin:0; font-family:Helvetica,Arial,sans-serif; font-size:12px;">
	<table border="1" cellpadding="5" cellspacing="0" style="margin:auto; width: 620px; border-collapse: collapse;">
		<tbody>
			<tr>
				<td style="text-align: center;" width="50%">
					<a href="https://itsumotaxi.com/">
						<img src="https://itsumotaxi.com/images/itsumo_final_100.png" />
					</a>
				</td>

				<td style="text-align: right; vertical-align: top;">
					<p>Invoice No: {{$res->invoice->invoice_number}}</p>
					<p>Issue date: {{$res->invoice->created_at}}</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="background:#8ece3c">
					<h3 style="text-align: center;">
						Trip Sammary</h3>
				</td>
			</tr>

			<tr>
				<td style="background-color: rgb(204, 204, 204);">Driver</td>
				<td>{{$res->taxi->driver_name}}</td>
			</tr>
			<tr>
				<td style="background-color: rgb(204, 204, 204);">Model</td>
				<td>{{$res->taxi->taxi_model}}</td>
			</tr>
			<tr>
				<td style="background-color: rgb(204, 204, 204);">Distance</td>
				<td>{{$res->invoice->distance}}</td>
			</tr>
			<tr>
				<td style="background-color: rgb(204, 204, 204);">Duration</td>
				<td>{{$res->invoice->duration}} minutes</td>
			</tr>
			<tr>
				<td style="background-color: rgb(204, 204, 204);">Billed to</td>
				<td>{{$res->user->name}}</td>
			</tr>
			<tr>
				<td style="background-color: rgb(204, 204, 204);">Total</td>
				<td><strong>{{$res->invoice->total}} KHR</strong></td>
			</tr>
			<tr>
				<td colspan="2">
					<img src="https://itsumotaxi.com/images/operator.jpg" width="100%" />
				</td>
			</tr>

			<tr>
				<td style="text-align: right;">
					<a href="https://itunes.apple.com/us/app/itsumo-the-cambodia-taxi/id1182868051?mt=8"><img src="https://itsumotaxi.com/images/app-store.png" width="100" /></a>
				</td>
				<td>
					<a href="https://play.google.com/store/apps/details?id=com.camitss.itsumouser"><img src="https://itsumotaxi.com/images/play-store.png" width="100" /></a>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
