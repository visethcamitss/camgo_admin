
@extends('layouts.app')
@section('content')
<div class="container-fluid">
        <!-- <div class="panel panel-primary">
            <div class="panel-heading">Report</div>
            <div class="panel-body">
                <div class="row" style="margin-bottom: 10px;margin-top: 10px;">
                    <div class="col-md-3">
                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                          <input type="text" id="startDate" name="start_date" class="form-control" style="margin:0;" value="{{$date}}" placeholder="Start date">
                          <div class="input-group-addon" >
                              <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                          <input type="text" id="endDate" name="end_date" class="form-control" style="margin:0;" value="{{$date}}" placeholder="End date">
                          <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th" ></span>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button type="button" id="generate_report" name="button" class="generate_report btn btn-primary" >Generate Report</button>
                    </div>
                </div>
                <table class="table table-hover">
                    <th>Type</th>
                    <th>Request</th>
                    <th>failed</th>
                    <th>Meter</th>
                    <th>Completed</th>
                    <th>Completed(%)</th>
                <tbody id = "request_daily_report">
                    @include('user_requests.report',['categories' => $categories,'daily_report' =>$daily_report])
                </tbody>
                </table>
            </div>
    </div> -->

    <div class="panel panel-primary">
        <div class="panel-heading">User requests!</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <form class="form-inline" role="form" method="GET" action="{{ url('/user_requests/') }}">
                		<div class="form-group">
        				    <input type="text" class="form-control" name="keyword" value="{{$keyword}}" placeholder="Taxi ID, User, Driver" />
        				    <select class="form-control" name="request_satus">
                                <option value="0">-- Select a request status --</option>
                                @foreach($req_status as $key=>$val)
                                    <option value="{{ $key }}" @if($request_status==$key) selected @endif> {{ $val[0]}}</option>
                                @endforeach
                            </select>
        				    <button type="submit" class="btn btn-primary">Submit</button>
                                <?php
                                    $number_pickingUp = $res1->where('request_status','=',3)->count();
                                    $number_wating = $res1->where('request_status','=',8)->count();
                                    $number_riding = $res1->where('request_status','=',6)->count();
                                 ?>
                                <button name = "request_satus" type="submit" class="btn btn-primary" value="3">Picking Up (<?php echo $number_pickingUp ;?>)</button >
                                <button name = "request_satus" type="submit" class="btn btn-primary" value="8"/>Waiting (<?php echo $number_wating ;?>)</button >
                                <button name = "request_satus" type="submit" class="btn btn-primary" value="6"/>On Riding (<?php echo $number_riding ;?>)</button >

        				</div>
                	</form>
                </div>
            </div>
            <div class="row" style="margin-top:40px">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User photo</th>
                            <th>User name</th>
                            <!-- <th>Company</th> -->
                            <th>Driver photo</th>
                            <th>Driver name</th>
                            <th>Request status</th>
                            <th>Request date</th>
                            <th>Request category</th>
                            <th>Pickup</th>
                            <th>Drop off</th>
                            <th>Distance</th>
                            <th>Estimated</th>
                            <th>Tip</th>
                            <!-- <th>KHR</th> -->
                            <!-- <th>Receive Call</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($res as $r)
                            <?php
                            $i++;
                            $duration = '-';
                            $distance = '-';

                            if($r->invoice->created_at){
                                if ($r->accept_date != null){
                                    $date1 = date_create($r->accept_date);
                                    $date2 = date_create($r->invoice->created_at);
                                    $diff = date_diff($date1,$date2);
                                    $t0 = $diff->h.':'.$diff->i.':'.$diff->s;
                                }
                                $time = $r->invoice->duration;
                                $hours = $time  / 3600;
                                $minutes = ($time  % 3600) / 60;
                                $seconds = $time  % 60;
                                $duration = sprintf("%02d:%02d:%02d", $hours,$minutes,$seconds);
                                $distance = number_format($r->invoice->distance/1000, 2) .' km';
                            }elseif($r->accept_date != null && ( $r->request_status ==4 || $r->request_status ==5 ) ){
                                $date1 =date_create($r->accept_date);
                                $date2=date_create($r->cancel_date);
                                $diff=date_diff($date1,$date2);
                                $t0 = $diff->h.':'.$diff->i.':'.$diff->s;
                            }elseif ($r->request_status == 3 || $r->request_status == 8) {
                                $date1 = date_create($r->accept_date);
                                $date2 = date_create(NULL);
                                $diff = date_diff($date1, $date2);
                                $t0 = $diff->h.':'.$diff->i.':'.$diff->s;
                            }else{
                                $t0 = '';
                            }
                            ?>
                            <tr>
                                <td><a href="#" class="href-id" id="{{$r->id}}" style="font-size:32px"><i class="glyphicon glyphicon-map-marker"></i></a></td>
                                <td>
                                    <?php
                                        $name = $r->user->name;
                                        if(ends_with($r->user_id,'ghost')){
                                            $name = 'Meter';
                                            echo "<img src ='https://camgo-app.itsumotaxi.com/images/ghost_image.png'/>";
                                        }else { ?>
                                        <a href="/userxs/{{$r->user->id}}"><img src="https://camgo-app.itsumotaxi.com/{{$r->user->avatar?$r->user->avatar:images/noimage.gif}}" width="64" height="64" /></a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php
                                        if($name == null){
                                            $name = '';
                                        } 
                                        $name .= '('.$r->user->phone.')';
                                    ?>
                                    <a href="/userxs/{{$r->user->id}}">{{$name}}</a>
                                </td>
                                <!-- <td>{{$r->company_name}}</td> -->
                                <td><a href="/taxis/{{$r->taxi->id}}"><img src="https://camgo-app.itsumotaxi.com/{{$r->taxi->driver_avatar?$r->taxi->driver_avatar:images/noimage.gif}}" width="64" height="64" /></a></td>
                                <td>
                                    <a href="/taxis/{{$r->taxi->id}}">{{$r->taxi->driver_name}}</a><br/><br/>
                                    <input type="hidden" id="tid{{$i}}" value="{{$r->taxi->taxi_id}}" />
                                    <input type="hidden" id="uid{{$i}}" value="{{$r->user->user_id}}" />
                                    <input type="hidden" id="rid{{$i}}" value="{{$r->request_id}}" />
                                    <?php if($r->request_status == 3 || $r->request_status == 8){ ?>
                                        <span id="btn_cancel{{$i}}" class="btn btn-warning btn-xs" onClick=cancelRequest({{$i}})>Cancel</span>
                                        <span id="btn_start{{$i}}" class="btn btn-info btn-xs" onClick=startJourney({{$i}})>Start</span>
                                    <?php } else if($r->request_status == 6 ) {?>
                                        <span id="btn_end{{$i}}" class="btn btn-danger btn-xs" onClick=endJourney({{$i}})>End Trip</span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php
                                        if($r->request_satus == 6){
                                        }
                                     ?>
                                    <span style="color:{{$req_status[$r->request_status][1]}};">{{$req_status[$r->request_status][0]}}</span><br/><br/>
                                    <!-- <a href="/user_requests/status?id={{$r->id}}&action=1" class="btn btn-warning btn-xs confirm-frm">Driver</a>
                                    <a href="/user_requests/status?id={{$r->id}}&action=2" class="btn btn-danger btn-xs confirm-frm">User</a> -->
                                </td>
                                <td>{{$r->request_date}}</td>
                                <td>{{$r->taxi_category->category_name}}</td>
                                <td>{{ $t0 }}</td>
                                <td>{{ $duration }}</td>
                                <td>{{ $distance }}</td>
                                <td>{{$r->estimate_fare}}</td>
                                <td>{{$r->tip}}</td>
                                <!-- <td>
                                    @if($r->invoice->payment_method == 'iTsumo Luy')
                                        <span class="btn btn-success">{{$r->invoice->total?$r->invoice->total:$r->description}} </span>
                                    @else
                                        {{$r->invoice->total?$r->invoice->total:$r->description}}
                                    @endif
                                </td> -->
                                <!-- <td>
                                    @if($r->receive_call_ack == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </td> -->
                            </tr>
                        @endforeach
                    </tbody>

                </table>
                <div style="text-align:center">
                    {{ $res->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<inpu type="hidden" value="" name="request_id" id="request_id" />
<button type="button" class="btn btn-primary btn-lg my-modal-btn hide" data-toggle="modal" data-target="#myModal">
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:90%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<inpu type="hidden" value="" name="request_id" id="request_id" />
<button type="button" class="btn btn-primary btn-lg my-modal-btn hide" data-toggle="modal" data-target="#myModal">
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:90%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-RREXt_zI7vIUJi3AhaT_gn6KlSMz8S0"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script type="text/javascript">
		var gmarkers = [];
        var isCounting = true;
		var ck = 0;
		$(document).ready(function () {
            // $("#myModal").modal({
            //     backdrop: 'static',
            //     keyboard: false,
			// 	show: false
            // });
            setInterval(function(){
                if (isCounting) {
                    location.reload(1);
                }
            },30000);
			$(document.body).on('click', '.href-id', function(){
			    ck = 0;
                isCounting = false; 
			    $('#request_id').val($(this).attr('id'));
		        $('#myModal h4').text('#'+$('#request_id').val());
				$("#myModal").modal("show");
                timeout(ck);
		       return false;
		    });
			$('#myModal').on('hidden.bs.modal', function () {
  				// do something�
                isCounting = true; 
				// ck = -1;
               
                
			});
            $(".generate_report").click(function(){
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                $self = $(this);
                if($self.hasClass('disabled')) {
                    return false;
                }
                $self.addClass('disabled');
                $.ajax({
                   type: "GET",
                   url: "{{ url('generate_reqport')}}",
                   data: {
                       start_date: startDate,
                       end_date: endDate
                   },
                   success: function(result) {
                      $("#request_daily_report").html(result);
                      $self.removeClass('disabled');
                   },
                   error: function(result) {
                      $("#request_daily_report").html("");
                      $self.removeClass('disabled');
                   }
               });
            });
		});
		function do_ajax(cks){
            var url = '/user_requests/'+$('#request_id').val();
            var xhr;
			if(xhr){
				xhr.abort();
			}
			xhr = $.ajax({
               url: url,
               data:'ck='+ck,
               success:function(res){
                   var obj = JSON.parse(res);
               	   if(!isEmpty(obj.taxi) && obj.taxi.driver_avatar){
               	   		driver_avatar = obj.taxi.driver_avatar;
               	   }else{
               	   		driver_avatar = 'noimage.gif';
               	   }
               	   if(obj.user.avatar){
               	   		user_avatar = obj.user.avatar;
               	   }else{
                        if (obj.user.name == 'Ghost') {
                          user_avatar = 'ghost_image.png';
                        }else {
                          user_avatar = 'noimage.gif';
                        }
               	   }
               	   html_ = '<div id="map" style="width: 100%; height: 600px; margin:auto;"></div>';
               	   html_ +='<div class="row third-block">';
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box text-center">';
								html_ +='<a href="/taxis/'+obj.taxi.id+'">';
									html_ +='<img src="https://camgo-app.itsumotaxi.com/'+driver_avatar+'" width="64" height="64" />';
								html_ +='</a>';
								html_ +='<p><strong>'+obj.taxi.driver_name+'</strong> - <a href="tel:'+obj.taxi.driver_phone+'">'+obj.taxi.driver_phone+'</a></p>';
							html_ +='</div>';
	               	   	html_ +='</div>';

	               	   	verity = '';
	               	   	if(obj.user.verify_status!=1){
	               	   		verity = 'style="color:red"';
	               	   	}
	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box text-center">';
								html_ +='<a href="/userxs/'+obj.user.id+'">';
									html_ +='<img src="https://camgo-app.itsumotaxi.com/'+user_avatar+'" width="64" height="64" />';
								html_ +='</a>';
								html_ +='<p><strong><a href="/userxs/'+obj.user.id+'">'+obj.user.name+'</a></strong> - <a href="tel:'+obj.user.phone+'" '+verity+'>'+obj.user.phone+'</a></p>';
						    html_ +='</div>';
	               	   	html_ +='</div>';

	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box">';
	               	   			html_ +='<p>Date: <strong>'+obj.res.request_date+'</strong></p>';
								html_ +='<p>Status: <strong id="res_status">'+obj.status+'</strong></p>';
								html_ +='<p>Pickup: <strong id="res_pickup">'+obj.pickup+'</strong></p>';
	               	   		html_ +='</div>';
	               	   	html_ +='</div>';

	               	   	html_ +='<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
	               	   		html_ +='<div class="gray-box">';
								html_ +='<p>Distance: <strong id="res_distance">'+obj.distance+'</strong></p>';
								html_ +='<p>Time: <strong id="res_duration">'+obj.duration+'</strong></p>';
								html_ +='<p>Fare: <strong id="res_fare">'+obj.fare+'</strong></p>';
	               	   		html_ +='</div>';
	               	   	html_ +='</div>';
               	   html_ +='</div>';

               	   	if(cks == 1){
               	   		$('#myModal .modal-body').html(html_);
	               	    var latlng_center = new google.maps.LatLng(obj.u_lat, obj.u_lot);
				        var mapOptions = {
				            center: latlng_center,
				            zoom: 17,
				            mapTypeId: google.maps.MapTypeId.ROADMAP
				        };
				        var el=document.getElementById("map");
				        map = new google.maps.Map(el, mapOptions);
			    	}else{
			    		for(i=0; i<gmarkers.length; i++){
					        gmarkers[i].setMap(null);
					    }
					    $('#myModal .modal-body #res_status').text(obj.status);
					    $('#myModal .modal-body #res_pickup').text(obj.pickup);
					    $('#myModal .modal-body #res_distance').text(obj.distance);
					    $('#myModal .modal-body #res_duration').text(obj.duration);
					    $('#myModal .modal-body #res_fare').text(obj.fare);
			    	}

			        if(obj.taxi.serving_status==1 && obj.taxi.priority > 0 && obj.taxi.status==1 && obj.taxi.reviewed==1){
		                $boo = 2;
		            }else if(obj.taxi.serving_status==2){
		                $boo = 1;
		            }else{
		                $boo = 0;
		            }

			        var latlng = new google.maps.LatLng(obj.taxi.latitude, obj.taxi.longitude);
			        var marker = new google.maps.Marker({
			            map: map,
			            position: latlng,
			            title: obj.taxi.driver_name+' - Tel: '+obj.taxi.driver_phone,
			            icon: 'https://adm.itsumotaxi.com/images/map_'+$boo+'.png'
			        });
			        gmarkers.push(marker);

			        var latlng_ = new google.maps.LatLng(obj.res.taxi_lat, obj.res.taxi_long);
			        var marker_ = new google.maps.Marker({
			            map: map,
			            position: latlng_,
			            title: "taxi location",
			            icon: 'https://adm.itsumotaxi.com/images/taxi_accept.png'
			        });
			        gmarkers.push(marker_);

			        var latlng = new google.maps.LatLng(obj.res.user_lat, obj.res.user_long);
			        var marker = new google.maps.Marker({
			            map: map,
			            position: latlng,
			            title: obj.user.name+' - Tel: '+obj.user.phone,
			            icon: 'https://adm.itsumotaxi.com/images/pickup_32.png'
			        });
			        gmarkers.push(marker);

			        if(obj.user.last_lat && obj.user.last_long && obj.user.last_lat!=0 && obj.user.last_long!=0){
				        var latlng = new google.maps.LatLng(obj.user.last_lat, obj.user.last_long);
				        var marker = new google.maps.Marker({
				            map: map,
				            position: latlng,
				            title: obj.user.name+' - Tel: '+obj.user.phone,
				            icon: 'https://adm.itsumotaxi.com/images/passenger_32.png'
				        });
			        }
			        gmarkers.push(marker);

			        if(obj.res.target_lat && obj.res.target_long && obj.res.target_lat!=0 && obj.res.target_long!=0){
				        var latlng = new google.maps.LatLng(obj.res.target_lat, obj.res.target_long);
				        var marker = new google.maps.Marker({
				            map: map,
				            position: latlng,
				            title: "final drop off",
				            icon: 'https://adm.itsumotaxi.com/images/dropoff_32.png'
				        });

				        var prelatlng = new google.maps.LatLng(obj.res.pre_target_lat, obj.res.pre_target_long);
				        var marker_ = new google.maps.Marker({
				            map: map,
				            position: prelatlng,
				            title: "estimate drop off",
				            icon: 'https://adm.itsumotaxi.com/images/pre_dropoff_32.png'
				        });
				        gmarkers.push(marker_);
			        }
			        gmarkers.push(marker);
			        if(obj.travel_log.length > 0){
				        for(i=0; i<obj.travel_log.length; i++){
				       		var latlng = new google.maps.LatLng(obj.travel_log[i].latitude, obj.travel_log[i].longitude);
						var routeIcon = 'https://adm.itsumotaxi.com/images/blue_man.png';
						if(obj.travel_log[i].is_skip == 1){
							routeIcon = 'https://adm.itsumotaxi.com/images/red_man.png';
						}else if (obj.travel_log[i].is_skip == 3){
							routeIcon = 'https://adm.itsumotaxi.com/images/green_man.png';
						}
						var latlng = new google.maps.LatLng(obj.travel_log[i].latitude, obj.travel_log[i].longitude);
					        var marker = new google.maps.Marker({
					            map: map,
					            position: latlng,
					            title: obj.travel_log[i].distance+' m , '+obj.travel_log[i].duration+' sec, '+obj.travel_log[i].sub_total+'riel',
					            icon: routeIcon
					        });
					        gmarkers.push(marker);
						}
					}

			        google.maps.event.addListenerOnce(map, 'idle', function () {
					    google.maps.event.trigger(map, 'resize');
					});
			   		if(obj.res.request_status != 3 && obj.res.request_status != 6 && obj.res.request_status != 8){
						ck = -1;
					}
               }

           });
		}

		function timeout() {
			if(ck < 0 ){
				return;
			}
			ck = ck + 1;
			do_ajax(ck);
		    setTimeout(function () {
		        timeout();
		    }, 5000);
		}

        var urlServer = 'https://mb.itsumotaxi.com';
        //urlServer = 'http://192.168.1.59:3000';
        var socket = io(urlServer, {
            secure : true
        });

        socket.heartbeatTimeout = 6000;
        function cancelRequest(id){
            var r = confirm("Are you sure to cancel this trip!");
            if (r == true) {
                document.getElementById("btn_cancel"+id).style.display = "none";
                var param = {
                        data : {
                            userId: document.getElementById("uid"+id).value,
                            taxiId: document.getElementById("tid"+id).value,
                            requestId: document.getElementById("rid"+id).value
                        }
                }
                socket.emit("admin_cancel_request",param);
            }

        }

        function startJourney(id){
            var r = confirm("Are you sure to start this trip!");
            if (r == true) {
                document.getElementById("btn_start"+id).style.display = "none";
                var param = {
                        data : {
                            userId: document.getElementById("uid"+id).value,
                            taxiId: document.getElementById("tid"+id).value,
                            requestId: document.getElementById("rid"+id).value
                        }
                }
                socket.emit("admin_start_journey",param);
            }

        }

        function endJourney(id){

            var r = confirm("Are you sure to end this trip!");
            if (r == true) {
                document.getElementById("btn_end"+id).style.display = "none";
                var param = {
                    data : {
                        userId: document.getElementById("uid"+id).value,
                        taxiId: document.getElementById("tid"+id).value,
                        requestId: document.getElementById("rid"+id).value
                    }
                }
                socket.emit("admin_end_journey",param);
            }
        }

        function parseJson(token){
            if(typeof token === 'string'){
                return JSON.parse(token);
            }
            return token;
        }

        socket.on("admin_end_journey-result",function(data){
            data = parseJson(data);
            if(data.code == 0){
                alert("End journey fail, reload page and try again");
            }else{
                alert("End journey request already send to driver");
            }
        });

        socket.on("admin_start_journey-result",function(data){
            data = parseJson(data);
            if(data.code == 0){
                alert("Start journey fail, reload page and try again");
            }else{
                alert("Start journey request already send to driver");
            }
        });

        socket.on("admin_cancel_request-result",function(data){
            data = parseJson(data);
            if(data.code == 0){
                alert("Cancel request fail, reload page and try again");
            }else{
                alert("Cancel request already send to driver");
            }
        });
        socket.on("admin_cancel_progress_booking-resul", function(data){
            data = parseJson(data);
            if(data.code == 0){
                alert("Cancel request fail, reload page and try again");
            }else{
                alert("Cancel request already send to driver");
            }
        });

</script>
@endsection
