@foreach ($categories as $c)
    <?php
        $all_request = 0;
        $cm_request = 0;
        $fail = 0;
        $meter = 0;
        if(!empty($daily_report) && count($daily_report) == 2){
            $all = $daily_report[0]->where('category_id',$c->id);
            if(count($all) > 0 ) {
                $all_request = $all->count();
                $cm = $daily_report[1]->where('category_id',$c->id);
                if(count($cm) > 0){
                    $cm_request = $cm->count();
                    $m = $cm->where('receive_call_ack', 3);
                    $meter = $m->count(); 
                }
                if($all_request > 0 ){
                    $fail = $all_request - $cm_request;
                }
                $cm_request = $cm_request - $meter;
                if($cm_request < 0) {
                    $cm_request = 0;
                }

            }
        }
    ?>
    <tr>
        <td>{{ $c->category_name }}</td>
        <td>{{ $all_request}}</td>
        <td>{{ $fail }}</td>
        <td>{{ $meter }}</td>
        <td>{{ $cm_request }}</td>
        <td><?php
            if($all_request == 0 || $cm_request == 0){
                echo "0";
            }else{
                echo round(($cm_request/($all_request - $meter))*100,1);
            }
            
            ?>
        </td>
    </tr>
@endforeach
