-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2017 at 11:07 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itsumotaxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_permissions`
--

CREATE TABLE `t_permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_permissions`
--

INSERT INTO `t_permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'login', NULL, NULL),
(2, 'logout', NULL, NULL),
(3, 'register', NULL, NULL),
(4, 'categories', NULL, NULL),
(5, 'categories.create', NULL, NULL),
(6, 'categories.store', NULL, NULL),
(7, 'categories.edit', NULL, NULL),
(8, 'categories.update', NULL, NULL),
(9, 'categories.destroy', NULL, NULL),
(10, 'users', NULL, NULL),
(11, 'users.show', NULL, NULL),
(12, 'users.edit', NULL, NULL),
(13, 'users.update', NULL, NULL),
(14, 'user_request', NULL, NULL),
(15, 'user_request.status', NULL, NULL),
(16, 'user_request.show', NULL, NULL),
(17, 'invoices', NULL, NULL),
(18, 'invoices.email', NULL, NULL),
(19, 'companies', NULL, NULL),
(20, 'companies.create', NULL, NULL),
(21, 'companies.store', NULL, NULL),
(22, 'companies.edit', NULL, NULL),
(23, 'companies.update', NULL, NULL),
(24, 'companies.destroy', NULL, NULL),
(25, 'taxis.live', NULL, NULL),
(26, 'taxis.ajax_live', NULL, NULL),
(27, 'taxis.create', NULL, NULL),
(28, 'taxis.store', NULL, NULL),
(29, 'taxis.edit', NULL, NULL),
(30, 'taxis.update', NULL, NULL),
(31, 'taxis.destroy', NULL, NULL),
(32, 'taxis', NULL, NULL),
(33, 'taxis.show', NULL, NULL),
(34, 'coupons.custom_print_preview', NULL, NULL),
(35, 'coupons.printone', NULL, NULL),
(36, 'coupons', NULL, NULL),
(37, 'chart', NULL, NULL),
(38, 'notification.taxi', NULL, NULL),
(39, 'notification.user', NULL, NULL),
(40, 'notification.all_taxi', NULL, NULL),
(41, 'roles.index', NULL, NULL),
(42, 'roles.create', NULL, NULL),
(43, 'roles.store', NULL, NULL),
(44, 'roles.show', NULL, NULL),
(45, 'roles.edit', NULL, NULL),
(46, 'roles.update', NULL, NULL),
(47, 'roles.destroy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_roles`
--

CREATE TABLE `t_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_roles`
--

INSERT INTO `t_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2017-09-27 17:00:00', '2017-09-27 17:00:00'),
(2, 'User', '2017-09-27 17:00:00', '2017-09-27 17:00:00'),
(3, 'Monitor', '2017-09-28 01:48:35', '2017-09-28 01:48:35');

-- --------------------------------------------------------

--
-- Table structure for table `t_role_permissions`
--

CREATE TABLE `t_role_permissions` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_role_permissions`
--

INSERT INTO `t_role_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_roles`
--

CREATE TABLE `t_user_roles` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user_roles`
--

INSERT INTO `t_user_roles` (`role_id`, `user_id`) VALUES
(1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_permissions`
--
ALTER TABLE `t_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_roles`
--
ALTER TABLE `t_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_role_permissions`
--
ALTER TABLE `t_role_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`);

--
-- Indexes for table `t_user_roles`
--
ALTER TABLE `t_user_roles`
  ADD PRIMARY KEY (`role_id`,`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_permissions`
--
ALTER TABLE `t_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `t_roles`
--
ALTER TABLE `t_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
