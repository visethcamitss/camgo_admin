//Usage

//load your JSON (you could jQuery if you prefer)
function loadJSON(url,callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  // var id = document.getElementById("itvalue").value;
  var id2 = document.getElementById("itvalue2").value;
  var type = document.getElementById("ittype").value;

  if(type == "wing_topup") {
    var amount = document.getElementById("amount").value;
    var invoice_id = document.getElementById("invoiceid").value;
    document.body.style.backgroundImage = "url('/images/background.jpg')";
    url = url + '/'+ invoice_id + '/'  + id2 + '/'+ amount + '/' + type;
  } else {
    var id = document.getElementById("itvalue").value;
    if(type == "wing") {
      document.body.style.backgroundImage = "url('/images/background.jpg')";
    } else if (type == "pipay") {
      document.body.style.backgroundImage = "url('/images/background.jpg')";
    }else {
      document.body.style.backgroundImage = "url('/images/background.jpg')";
    }
    url = url + '/' + id + '/' + id2 + '/0/' + type;
  }

  xobj.open('GET', url, true);
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      //Call the anonymous function (callback) passing in the response
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}



//your own function to capture the spin results
function myResult(e) {
  //e is the result object
   // console.log('Spin Count: ' + e.spinCount + ' - ' + 'Win: ' + e.win + ' - ' + 'Message: ' +  e.msg);

    // if you have defined a userData object...
    if(e.userData){
      var xobj = new XMLHttpRequest();
      xobj.overrideMimeType("application/json");
      var id = document.getElementById("itvalue3").value;
      var params = "_token="+id;
      var url = e.userData.u + '/' + e.userData.id + '/' + e.userData.uid + "/" + e.userData.win;
      xobj.open('POST', url, true);
      xobj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == "200") {
          //Call the anonymous function (callback) passing in the response
        }
      };
      xobj.send(params);

    }

  //if(e.spinCount == 3){
    //show the game progress when the spinCount is 3
    //console.log(e.target.getGameProgress());
    //restart it if you like
    //e.target.restart();
  //}

}

//your own function to capture any errors
function myError(e) {
  //e is error object
  //console.log('Spin Count: ' + e.spinCount + ' - ' + 'Message: ' +  e.msg);

}

function myGameEnd(e) {

  //e is gameResultsArray
 // console.log(e);
  TweenMax.delayedCall(5, function(){

    Spin2WinWheel.reset();

  })
}

function init(url) {
   loadJSON(url,function(response) {
    // Parse JSON string to an object
    var jsonData = JSON.parse(response);
    //console.log("res = ",response);
    //if you want to spin it using your own button, then create a reference and pass it in as spinTrigger
    var mySpinBtn = document.querySelector('.spinBtn');
    //create a new instance of Spin2Win Wheel and pass in the vars object
    var myWheel = new Spin2WinWheel();

    //WITH your own button
    myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError, spinTrigger:mySpinBtn});

  //   //WITHOUT your own button
    //myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError});
  });
}
//And finally call it
init(url);
