-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 20, 2016 at 08:48 AM
-- Server version: 5.5.50-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hi_taxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_adminstrator`
--

CREATE TABLE IF NOT EXISTS `t_adminstrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `pass_code` varchar(50) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `t_adminstrator`
--

INSERT INTO `t_adminstrator` (`id`, `admin_id`, `name`, `pass_code`, `image`) VALUES
(1, 'a7y19qwe0s1467d79880675', 'admin', 'admin', 'http://ugasigrho.org/static/website/img/brothers/default.png');

-- --------------------------------------------------------

--
-- Table structure for table `t_company`
--

CREATE TABLE IF NOT EXISTS `t_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `code` varchar(30) NOT NULL,
  `pass_code` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_company`
--

INSERT INTO `t_company` (`id`, `name`, `code`, `pass_code`, `logo`, `status`, `created_date`, `modified_date`) VALUES
(1, 'Red', '168KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'https://cdn6.f-cdn.com/contestentries/35520/8675074/5244b2ef87f0c_thumb900.jpg', 1, '2016-07-15 00:00:00', '2016-07-15 00:00:00'),
(2, 'Green', '88KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'https://cdn6.f-cdn.com/contestentries/35520/8675074/5244b2ef87f0c_thumb900.jpg', 1, '2016-07-15 00:00:00', '2016-07-15 00:00:00'),
(3, 'Yellow Cap', '201KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'http://www.tdhstrategies.com/wp-content/uploads/2014/04/Yellow-Cab-269x269.png', 1, '2016-07-20 00:00:00', '2016-07-20 00:00:00'),
(4, 'Super Cap', '888KH', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'http://www.supercabz.com/img/image-1.jpg', 1, '2016-07-20 00:00:00', '2016-07-20 00:00:00'),
(5, 'public', 'GENERAL', '', '', 1, '2016-09-22 00:00:00', '2016-09-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_company_taxi_category`
--

CREATE TABLE IF NOT EXISTS `t_company_taxi_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_Id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '2',
  `modified_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `t_company_taxi_category`
--

INSERT INTO `t_company_taxi_category` (`id`, `company_Id`, `category_id`, `is_delete`, `modified_date`, `created_date`) VALUES
(1, 1, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(2, 1, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(3, 2, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(4, 2, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(5, 3, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(6, 3, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(7, 4, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(8, 4, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(9, 5, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(10, 5, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(11, 5, 3, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_coupon`
--

CREATE TABLE IF NOT EXISTS `t_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` varchar(50) NOT NULL,
  `value` float NOT NULL DEFAULT '0',
  `image` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_id` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice`
--

CREATE TABLE IF NOT EXISTS `t_invoice` (
  `invoice_number` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(50) NOT NULL,
  `request_id` varchar(50) NOT NULL,
  `duration` float NOT NULL,
  `distance` float NOT NULL,
  `payment_method` varchar(20) NOT NULL DEFAULT 'Paid with cash',
  `initial_flare` int(11) NOT NULL,
  `flag_down_fee` int(11) NOT NULL,
  `per_minute_fee` int(11) NOT NULL,
  `per_km_fee` int(11) NOT NULL,
  `currency` varchar(11) NOT NULL DEFAULT 'KHR',
  `notes` text NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '2',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`invoice_number`),
  UNIQUE KEY `invoice_id` (`invoice_id`),
  UNIQUE KEY `request_id` (`request_id`),
  UNIQUE KEY `invoice_number_2` (`invoice_number`),
  KEY `invoice_number` (`invoice_number`),
  KEY `invoice_number_3` (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_place_category`
--

CREATE TABLE IF NOT EXISTS `t_place_category` (
  `id` int(11) NOT NULL,
  `place_category_id` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '1',
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `place_category_id` (`place_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_place_category`
--

INSERT INTO `t_place_category` (`id`, `place_category_id`, `name`, `type`, `description`, `is_delete`, `image`) VALUES
(1, 'ks5mxeewxd1567687387613', 'Restuarant', 'restaurant', 'Everything was delicious. Tried the salmon tartar as starter, magret de canard and faux fillet for main. Can''t say what was better.', 1, 'http://phnompenh.julianahotels.com/content/content_15753_1.jpg'),
(2, 'ky5mxeewxd1567687387613', 'Entertainment', 'bar', 'This Phnom Penh institution with an alluring Angkor theme has evolved more into a nightclub than a bar over the years.\r\n', 1, ''),
(3, '35ltvyj0mx1467987808072', 'Accommodation', 'cafe', 'Situated on the riverfront, Riverview Suites Phnom Penh is 300 metres from New Market. It features a business centre, a restaurant and bar and free WiFi access.', 1, ''),
(4, '37utvyj0mx1467987808072', 'Shopping', 'shopping_mall', 'Phnom Penh is a great city for shopping; its stores and markets purvey everything from handicrafts and homewares to sportswear and souvenirs.', 1, ''),
(5, 'b82o61o9ap1468205987822', 'Health', 'hospital', 'The most flexible travel insurance policy available is provided by World Nomads. They offer excellent options for those who claim Cambodia as their country of residence.', 1, ''),
(6, 'b82o61o9ap1468205987825', 'Public Service', 'bus_station', 'No description', 1, ''),
(7, 'lqfpgbk1441467779762938', 'Culture', 'natural_feature', 'Culture of Cambodia. Throughout Cambodia''s long history, religion has been a major source of cultural inspiration. Over nearly two millennia, Cambodians have developed a unique Khmer belief from the syncreticism of indigenous animistic beliefs and the Indian religions of Buddhism and Hinduism.', 1, ''),
(8, 'lqfpgbk1641467779762930', 'Travel Agency', 'bus_station', 'The most trust worthy and reliable travel agency specializing in travel to South East Asian Region in today''s travel industry and we connect travelers from Asia, Europe, United State, Australia and other parts of the world.', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_taxi`
--

CREATE TABLE IF NOT EXISTS `t_taxi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `taxi_id` varchar(30) CHARACTER SET utf8 NOT NULL,
  `taxi_code` varchar(20) NOT NULL,
  `taxi_model` varchar(30) NOT NULL,
  `plate_number` varchar(20) NOT NULL,
  `driver_phone` varchar(20) NOT NULL,
  `pass_code` varchar(100) NOT NULL,
  `driver_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `driver_age` int(11) DEFAULT NULL,
  `driver_avatar` varchar(200) DEFAULT NULL,
  `serving_status` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `job_status` int(11) NOT NULL DEFAULT '1',
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '5000',
  `company_name` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'Personal',
  `taxi_image` varchar(300) NOT NULL,
  `rating` float NOT NULL DEFAULT '3',
  `wallet` float NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taxi_id` (`taxi_id`),
  UNIQUE KEY `taxi_code` (`taxi_code`,`company_id`),
  UNIQUE KEY `taxi_code_2` (`taxi_code`,`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `t_taxi`
--

INSERT INTO `t_taxi` (`id`, `category_id`, `taxi_id`, `taxi_code`, `taxi_model`, `plate_number`, `driver_phone`, `pass_code`, `driver_name`, `driver_age`, `driver_avatar`, `serving_status`, `status`, `job_status`, `latitude`, `longitude`, `company_id`, `company_name`, `taxi_image`, `rating`, `wallet`, `created_date`, `modified_date`) VALUES
(1, 1, 'lqfpgbk1441467779762930', '1000', 'PRUIS HYPERSONIC RED', '00000', '85511222221', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sony TOM', 27, NULL, 1, 1, 1, '11.555717', '104.923398', 1, 'Red', 'lqfpgbk1441467779762930.png', 3, 0, '2016-07-06 11:36:02', '2016-07-07 03:56:14'),
(2, 1, 'o7y19qwe0s1467779770675', '1001', 'PRUIS HYPERSONIC RED', '00000', '85511222222', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Honda ROM', 27, 'o7y19qwe0s1467779770675.png', 1, 1, 1, '22', '33', 1, 'Red', 'o7y19qwe0s1467779770675.png', 3, 0, '2016-07-06 11:36:10', '2016-07-13 03:55:52'),
(3, 1, 'wgc5ik8ns71467779821120', '1002', 'PRUIS HYPERSONIC RED', '00000', '85511222223', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Toyota PUT', 27, NULL, 1, 1, 1, '11.549184', '104.928313', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-06 11:37:01', '2016-07-06 11:37:01'),
(5, 2, 'ku3sraxm5v1467857442485', '1003', 'PRUIS HYPERSONIC RED', '00000', '85511222224', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sohpeak CHENG', 30, NULL, 1, 1, 1, '11.561780', '104.919922', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:10:42', '2016-07-07 09:10:42'),
(8, 2, 'qscej9hhfo1467857470319', '1004', 'PRUIS HYPERSONIC RED', '00000', '85511222225', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sokha SIM', 30, NULL, 1, 1, 1, '11.562123', '104.914486', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:11:10', '2016-07-07 09:11:10'),
(10, 2, 'sqqpnivxwq1467857507385', '1005', 'PRUIS HYPERSONIC RED', '00000', '85511222226', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sohpal OUL', 30, NULL, 1, 1, 1, '11.560168', '104.916761', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:11:47', '2016-07-07 09:11:47'),
(12, 1, '1g524ig3ai1467857613930', '1006', 'PRUIS HYPERSONIC RED', '00000', '85511222227', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Dara BUN', 32, NULL, 1, 1, 1, '11.561177', '104.911343', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:33', '2016-07-07 09:13:33'),
(13, 1, 'sw0001od1x1467857617193', '1007', 'PRUIS HYPERSONIC RED', '00000', '85511222228', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Phearom PAN', 31, NULL, 1, 1, 1, '11.558087', '104.908389', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:37', '2016-07-07 09:13:37'),
(14, 1, '7qbx9yx9el1467857619990', '1008', 'PRUIS HYPERSONIC RED', '00000', '85511222229', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sothearith VAN', 33, NULL, 1, 1, 1, '11.553057', '104.903920', 0, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:39', '2016-07-07 09:13:39'),
(15, 1, 'ro18x8hkd21467857623342', '1009', 'PRUIS HYPERSONIC RED', '00000', '85511222230', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sambath HEAN', 33, NULL, 1, 1, 1, '11.553698', '104.909263', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:43', '2016-07-07 09:13:43'),
(16, 1, 'cffnpqkay01467857627006', '1010', 'PRUIS HYPERSONIC RED', '00000', '85511222231', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sovan THY', 35, NULL, 1, 1, 1, '11.550954', '104.904636', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:47', '2016-07-07 09:13:47'),
(17, 1, 'v34s6kbs2i1467858054979', '1010', 'Toyota Corolla Fielder', '00000', '011111122', '5107c594902c5a5cb39e61379f602d902a0471be', 'Tola SAY', 31, NULL, 1, 1, 1, '11.560842', '104.905487', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:20:54', '2016-07-15 02:47:19'),
(18, 1, '7io8dxdvgk1467858069760', '1011', 'Toyota Corolla Fielder', '00000', '85510222233', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Odum HAY', 36, NULL, 1, 1, 1, '11.541694', '104.921836', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:09', '2016-07-07 09:21:09'),
(19, 1, 'qf3j08ktdt1467858073004', '1012', 'Toyota Corolla Fielder', '00000', '85510222234', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Lada KY', 34, NULL, 1, 1, 1, '11.541631', '104.917480', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:13', '2016-07-07 09:21:13'),
(20, 2, 'nc65dullib1467858076186', '1013', 'Toyota Corolla Fielder', '00000', '85510222235', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Ratana SOK', 40, NULL, 1, 1, 1, '11.539831', '104.914153', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:16', '2016-07-07 09:21:16'),
(21, 1, '4a69uwrs8i1467858079173', '1014', 'Toyota Corolla Fielder', '00000', '85510222236', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Kimhong RATH', 44, NULL, 1, 1, 1, '11.536330', '104.917554', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:19', '2016-07-07 09:21:19'),
(22, 2, 'o936p804gk1467858081902', '1015', 'Toyota Corolla Fielder', '00000', '85510222237', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Daro VUTH', 31, NULL, 1, 1, 1, '11.536309', '104.916792', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:21', '2016-07-07 09:21:21'),
(23, 1, 'aiv9j6628h1467858084420', '1016', 'Toyota Corolla Fielder', '00000', '85510222238', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Chunleng KIM', 31, NULL, 1, 1, 1, '11.538149', '104.921985', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:24', '2016-07-07 09:21:24'),
(24, 1, '2mldbdeh721467858087208', '1017', 'Toyota Corolla Fielder', '00000', '85510222239', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sothar KONG', 38, NULL, 1, 1, 1, '11.541576', '104.924056', 2, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:27', '2016-07-07 09:21:27'),
(25, 2, 'p5nuhi33rf1467858091048', '1018', 'Toyota Corolla Fielder', '00000', '85510222240', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Pharath SONG', 43, NULL, 1, 1, 1, '11.544381', '104.928104', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:31', '2016-07-07 09:21:31'),
(26, 1, 'fq0us02elr1467858094324', '1019', 'Toyota Corolla Fielder', '00000', '85510222241', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Vibol HAK', 28, NULL, 1, 1, 1, '11.548975', '104.927103', 2, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:34', '2016-07-07 09:21:34'),
(27, 1, '95w6p1a1iu1467858097759', '1020', 'Toyota Corolla Fielder', '00000', '85510222242', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Pheaktra Rath', 40, NULL, 1, 1, 1, '11.550815', '104.923927', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:37', '2016-07-07 09:21:37'),
(37, 1, 'd2lkummgbl1476154111681', '555552ss', 'Ferrari', '11111', '85599666666', '7b21848ac9af35be0ddb2d6b9fc3851934db8420', 'Senhlys', 33, NULL, 1, 1, 1, '', '', 5, 'public', '', 3, 0, '2016-10-11 02:48:31', '2016-10-11 02:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_taxi_category`
--

CREATE TABLE IF NOT EXISTS `t_taxi_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) NOT NULL,
  `seat_number` tinyint(4) NOT NULL,
  `image` varchar(200) NOT NULL,
  `initial_fare` int(11) NOT NULL,
  `per_minute_fee` int(11) NOT NULL,
  `per_km_fee` int(11) NOT NULL,
  `flag_down_fee` int(11) NOT NULL,
  `currency` varchar(11) NOT NULL DEFAULT 'KHR',
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_taxi_category`
--

INSERT INTO `t_taxi_category` (`id`, `category_name`, `seat_number`, `image`, `initial_fare`, `per_minute_fee`, `per_km_fee`, `flag_down_fee`, `currency`, `is_delete`, `created_date`, `modified_date`) VALUES
(1, 'Cap', 4, 'http://fasttaxiofca.com/sites/5981/cab469236.jpg', 4000, 200, 2000, 500, 'KHR', 2, '2016-07-18 00:00:00', '2016-07-18 02:05:10'),
(2, 'SUV', 4, 'http://cartype.com/pics/9134/full/mazda_bongo_gl-super_van_03.jpg', 5000, 250, 2500, 500, 'KHR', 2, '2016-07-18 00:00:00', '2016-07-18 04:08:10'),
(3, 'Tuk Tuk', 4, 'http://www.fuelarc.com/images/model_images/bus/tvs/king-basic-tuk-tuk/image_gallery/img_304.jpg', 2000, 200, 1500, 500, 'KHR', 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `travel_status` int(1) NOT NULL DEFAULT '1',
  `pass_code` varchar(100) NOT NULL DEFAULT 'UsEr@tAxI*168',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `phone_2` (`phone`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `name`, `user_id`, `avatar`, `phone`, `status`, `travel_status`, `pass_code`, `created_date`, `modified_date`) VALUES
(1, 'senhly', 'ks5mxeewxd1467687387613', 'ks5mxeewxd1467687387613.png', '0967993377', 1, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-07-01 02:09:13', '2016-07-13 03:59:44'),
(2, 'darith', 'hh1vjvicra1467687401888', 'hh1vjvicra1467687401888.png', '011328777', 1, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-07-01 05:10:09', '2016-07-13 03:59:34'),
(62, 'senhly168', '35ltvyj0mx1467972808072', '35ltvyj0mx1467972808072.png', '099887766', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-07-08 10:13:28', '2016-07-13 04:01:35'),
(63, 'daro', 'b82o61o9ap1468205987822', 'b82o61o9ap1468205987822.png', '011323434', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-07-11 02:59:47', '2016-07-13 04:01:18'),
(64, 'frog', 'r12lr4bcfb1468813527728', NULL, '011232323', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-07-18 03:45:27', '2016-07-18 03:45:27'),
(65, 'Sony', 'rs0tgsatti1468911703771', NULL, 'sonyms', 1, 1, 'd1e9f13c15eababac6bd10162bf8d5ddb735d668', '2016-07-19 07:01:43', '2016-07-19 07:01:43'),
(66, 'Sony', 'pv0fufvbrs1468919470234', NULL, '3401391', 1, 1, '995c616f7e94f14e6ddac23dfe13f10abb7d197c', '2016-07-19 09:11:10', '2016-07-19 09:11:10'),
(67, 'Sony', '1g4oldxdbi1468919519720', NULL, '093401361', 1, 1, '995c616f7e94f14e6ddac23dfe13f10abb7d197c', '2016-07-19 09:11:59', '2016-07-19 09:11:59'),
(68, 'Sony', 'gj4if3kuns1468919690327', NULL, '09341361', 1, 1, '0cfbd712ab1a5c57be52900cddfca0f492ea598e', '2016-07-19 09:14:50', '2016-07-19 09:14:50'),
(69, 'Sony', 'kyn2qpc9lv1468921182368', NULL, '093701361', 1, 1, 'e273c67c0ba2e21da2c530f0d0f4f838fb5b5943', '2016-07-19 09:39:42', '2016-07-19 09:39:42'),
(70, 'Spny', 'llgs16lhsl1468922051518', NULL, '093401362', 1, 1, '3d15365471502de19ebe1bde5809d607f55cf3ab', '2016-07-19 09:54:11', '2016-07-19 09:54:11'),
(71, 'Sony', 'fpmxp5ccgp1468922270294', NULL, '093401366', 1, 1, '995c616f7e94f14e6ddac23dfe13f10abb7d197c', '2016-07-19 09:57:50', '2016-07-19 09:57:50'),
(72, 'Rin Darith', 'p045u34q5j1468982547917', NULL, '011112233', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-07-20 02:42:27', '2016-07-20 02:42:27'),
(73, 'Sony', 'cmso108esf1468996183048', NULL, '093401365', 1, 1, '995c616f7e94f14e6ddac23dfe13f10abb7d197c', '2016-07-20 06:29:43', '2016-07-20 06:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_booking`
--

CREATE TABLE IF NOT EXISTS `t_user_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(30) NOT NULL,
  `booking_id` varchar(30) NOT NULL,
  `booking_target` tinyint(4) NOT NULL,
  `target_id` varchar(30) NOT NULL,
  `taxi_id` varchar(30) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `pickup_time` datetime NOT NULL,
  `pickup_lat` varchar(20) NOT NULL,
  `pickup_long` varchar(20) NOT NULL,
  `pickup_address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `target_lat` varchar(20) DEFAULT NULL,
  `target_long` varchar(20) DEFAULT NULL,
  `booking_status` tinyint(4) NOT NULL,
  `request_date` datetime NOT NULL,
  `accept_date` datetime DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`pickup_time`,`pickup_lat`,`pickup_long`),
  KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_user_request`
--

CREATE TABLE IF NOT EXISTS `t_user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(30) NOT NULL,
  `taxi_id` varchar(30) DEFAULT NULL,
  `user_lat` varchar(20) NOT NULL,
  `user_long` varchar(20) NOT NULL,
  `taxi_lat` varchar(20) NOT NULL,
  `taxi_long` varchar(20) NOT NULL,
  `request_status` tinyint(4) NOT NULL,
  `request_id` varchar(30) NOT NULL,
  `booking_id` varchar(30) DEFAULT NULL,
  `request_date` datetime NOT NULL,
  `accept_date` datetime DEFAULT NULL,
  `target_lat` varchar(20) DEFAULT NULL,
  `target_long` varchar(20) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '2',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `request_id` (`request_id`),
  UNIQUE KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=162 ;

--
-- Dumping data for table `t_user_request`
--

INSERT INTO `t_user_request` (`id`, `user_id`, `taxi_id`, `user_lat`, `user_long`, `taxi_lat`, `taxi_long`, `request_status`, `request_id`, `booking_id`, `request_date`, `accept_date`, `target_lat`, `target_long`, `cancel_date`, `complete_date`, `is_deleted`, `description`) VALUES
(131, 'ks5mxeewxd1467687387613', '2mldbdeh721467858087208', '11.546535', '104.913631', '11', '102', 2, 'i85cfdbgvh1476870830454', NULL, '2016-10-19 09:53:50', '2016-10-19 09:53:56', NULL, NULL, NULL, NULL, 2, NULL),
(161, 'ks5mxeewxd1467687387613', 'sqqpnivxwq1467857507385', '11.546535', '104.913631', '11', '102', 2, '7gdf6slxx81476871970542', NULL, '2016-10-19 10:12:50', '2016-10-19 10:12:55', NULL, NULL, NULL, NULL, 2, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
