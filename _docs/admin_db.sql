-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2016 at 04:05 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `camitss_taxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_company`
--

CREATE TABLE IF NOT EXISTS `t_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `code` varchar(30) NOT NULL,
  `pass_code` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_company`
--

INSERT INTO `t_company` (`id`, `name`, `code`, `pass_code`, `logo`, `status`, `created_date`, `modified_date`) VALUES
(1, 'Red', '168KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'https://cdn6.f-cdn.com/contestentries/35520/8675074/5244b2ef87f0c_thumb900.jpg', 1, '2016-07-15 00:00:00', '2016-07-15 00:00:00'),
(2, 'Green', '88KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'https://cdn6.f-cdn.com/contestentries/35520/8675074/5244b2ef87f0c_thumb900.jpg', 1, '2016-07-15 00:00:00', '2016-07-15 00:00:00'),
(3, 'Yellow Cap', '201KH', '5107c594902c5a5cb39e61379f602d902a0471be', 'http://www.tdhstrategies.com/wp-content/uploads/2014/04/Yellow-Cab-269x269.png', 1, '2016-07-20 00:00:00', '2016-07-20 00:00:00'),
(4, 'Super Cap', '888KH', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'http://www.supercabz.com/img/image-1.jpg', 1, '2016-07-20 00:00:00', '2016-07-20 00:00:00'),
(5, 'public', 'GENERAL', '', '', 1, '2016-09-22 00:00:00', '2016-09-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_company_taxi_category`
--

CREATE TABLE IF NOT EXISTS `t_company_taxi_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_Id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '2',
  `modified_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `t_company_taxi_category`
--

INSERT INTO `t_company_taxi_category` (`id`, `company_Id`, `category_id`, `is_delete`, `modified_date`, `created_date`) VALUES
(1, 1, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(2, 1, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(3, 2, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(4, 2, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(5, 3, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(6, 3, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(7, 4, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(8, 4, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(9, 5, 1, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(10, 5, 2, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00'),
(11, 5, 3, 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_coupon`
--

CREATE TABLE IF NOT EXISTS `t_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` varchar(50) NOT NULL,
  `value` float NOT NULL DEFAULT '0',
  `image` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_id` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice`
--

CREATE TABLE IF NOT EXISTS `t_invoice` (
  `invoice_number` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(50) NOT NULL,
  `request_id` varchar(50) NOT NULL,
  `duration` float NOT NULL,
  `distance` float NOT NULL,
  `last_lat` varchar(20) NOT NULL,
  `last_long` varchar(20) NOT NULL,
  `payment_method` varchar(20) NOT NULL DEFAULT 'Paid with cash',
  `initial_fare` int(11) NOT NULL,
  `flag_down_fee` int(11) NOT NULL,
  `per_minute_fee` int(11) NOT NULL,
  `per_km_fee` int(11) NOT NULL,
  `currency` varchar(11) NOT NULL DEFAULT 'KHR',
  `total` float NOT NULL DEFAULT '0',
  `service_charge` float NOT NULL DEFAULT '0',
  `notes` text,
  `is_delete` tinyint(4) NOT NULL DEFAULT '2',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`invoice_number`),
  UNIQUE KEY `invoice_id` (`invoice_id`),
  UNIQUE KEY `request_id` (`request_id`),
  UNIQUE KEY `invoice_number_2` (`invoice_number`),
  KEY `invoice_number` (`invoice_number`),
  KEY `invoice_number_3` (`invoice_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `t_invoice`
--

INSERT INTO `t_invoice` (`invoice_number`, `invoice_id`, `request_id`, `duration`, `distance`, `last_lat`, `last_long`, `payment_method`, `initial_fare`, `flag_down_fee`, `per_minute_fee`, `per_km_fee`, `currency`, `total`, `service_charge`, `notes`, `is_delete`, `created_date`, `modified_date`) VALUES
(1, 'wtvfpbrijq1478253050423', 'x5jywc3jwq1478253040410', 6, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 16:50:50', '2016-11-04 19:14:44'),
(2, 'sxklvjbv3o1478253309610', '6lauh78mhd1478253280470', 7, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 16:55:09', '2016-11-04 19:14:44'),
(3, 'devwrh1kj71478253637120', 'jwx696b3tj1478249620514', 64, 26896.2, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 54492.4, 3224.62, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(4, '9wyxjggnof1478253637126', 'hk6158ksv31478249760780', 374, 26896.2, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 55492.4, 3274.62, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(5, 'uoumn7qpgy1478253637132', 'lb9tq6uc981478250160993', 13, 26896.2, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 54292.4, 3214.62, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(6, '6qr2xmda1t1478253637136', '9rcjl1p8le1478252380871', 5, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(7, 'u41m7rhjnk1478253637148', 'rq3t9mdttj1478251200562', 4, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(8, 'p471ld5sqk1478253637152', 'b73if2rqhb1478250960371', 16, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(9, '8w1mpabcgd1478253637168', '03yx57bb0k1478253620383', 11, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(10, '8lmeb71vnr1478253637171', '94gso5pw0e1478252920717', 4, 23257.9, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 47015.9, 2850.79, NULL, 2, '2016-11-04 17:00:37', '2016-11-04 19:14:44'),
(11, 'k8ce5bugbe1478254037361', 'iu2uee5ehw1478254000200', 20, 21872.6, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 44245.1, 2712.26, NULL, 2, '2016-11-04 17:07:17', '2016-11-04 19:14:44'),
(12, 'ge3oowicji1478254298186', '2fl08s3pvk1478254200508', 17, 6948.7, '11.5452297', '104.9010623', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 14397.4, 1219.87, NULL, 2, '2016-11-04 17:11:38', '2016-11-04 19:14:44'),
(13, 'yfqer4hybd1478488183727', 'dpr8testxu1478487840735', 110, 0.001, '11.5473738', '104.9127884', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 10:09:43', '2016-11-07 10:09:46'),
(14, 'ge5nh20d9l1478489499978', 'obrscv7b581478489240372', 9, 0.001, '11.5473738', '104.9127884', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 10:31:39', '2016-11-07 10:31:45'),
(15, 'tic3qh219y1478492795365', 'vi6mtoibxj1478492760359', 13, 0, '11.5473738', '104.9127884', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 500, 0, NULL, 2, '2016-11-07 11:26:35', '0000-00-00 00:00:00'),
(16, 'u489cmgabs1478492960331', 'epw14kuuwd1478492840718', 104, 0.001, '11.5473738', '104.9127884', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 11:29:20', '2016-11-07 11:29:22'),
(17, '2uwxxtsuqe1478493153097', '43d4srfdii1478493140013', 18, 0.001, '11.5473738', '104.9127884', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 11:32:33', '2016-11-07 11:32:51'),
(18, 'nhp6prm8uf1478493213572', 'w9e19rcl9c1478493200429', 8, 243.424, '11.5465298', '104.9107285', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 11:33:33', '2016-11-07 11:36:18'),
(19, 'yhfm2f1pg01478493354936', 'w7rpbx936i1478493340708', 28, 243.327, '11.5465298', '104.9107285', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000.01, 700.001, NULL, 2, '2016-11-07 11:35:54', '2016-11-07 11:36:39'),
(20, 'ewwovrpo6c1478497317191', 'via8sk83k81478497280977', 80, 2708.54, '11.5628643', '104.9187393', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 5917.08, 795.854, NULL, 2, '2016-11-07 12:41:57', '2016-11-07 12:44:26'),
(21, 'r2but820at1478497935612', 'cwi4e3vuko1478497880272', 14, 0.001, '11.5628643', '104.9187393', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 12:52:15', '2016-11-07 12:52:30'),
(22, 'r11m8nuq1q1478505143191', 'nd4eq4futo1478500500039', 1, 0.001, '11.5593615', '104.9051056', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 4000, 700, NULL, 2, '2016-11-07 14:52:23', '2016-11-07 14:52:25'),
(23, 's15msegi381478507690147', '9wr1cvw5kd1478507660061', 3, 3496.6, '11.5460803', '104.9137921', 'Pay with cash', 4000, 500, 200, 2000, 'KHR', 7493.2, 874.66, NULL, 2, '2016-11-07 15:34:50', '2016-11-07 15:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `t_place_category`
--

CREATE TABLE IF NOT EXISTS `t_place_category` (
  `id` int(11) NOT NULL,
  `place_category_id` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '1',
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `place_category_id` (`place_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_place_category`
--

INSERT INTO `t_place_category` (`id`, `place_category_id`, `name`, `type`, `description`, `is_delete`, `image`) VALUES
(1, 'ks5mxeewxd1567687387613', 'Restuarant', 'restaurant', 'Everything was delicious. Tried the salmon tartar as starter, magret de canard and faux fillet for main. Can''t say what was better.', 1, 'http://phnompenh.julianahotels.com/content/content_15753_1.jpg'),
(2, 'ky5mxeewxd1567687387613', 'Entertainment', 'bar', 'This Phnom Penh institution with an alluring Angkor theme has evolved more into a nightclub than a bar over the years.\r\n', 1, ''),
(3, '35ltvyj0mx1467987808072', 'Accommodation', 'cafe', 'Situated on the riverfront, Riverview Suites Phnom Penh is 300 metres from New Market. It features a business centre, a restaurant and bar and free WiFi access.', 1, ''),
(4, '37utvyj0mx1467987808072', 'Shopping', 'shopping_mall', 'Phnom Penh is a great city for shopping; its stores and markets purvey everything from handicrafts and homewares to sportswear and souvenirs.', 1, ''),
(5, 'b82o61o9ap1468205987822', 'Health', 'hospital', 'The most flexible travel insurance policy available is provided by World Nomads. They offer excellent options for those who claim Cambodia as their country of residence.', 1, ''),
(6, 'b82o61o9ap1468205987825', 'Public Service', 'bus_station', 'No description', 1, ''),
(7, 'lqfpgbk1441467779762938', 'Culture', 'natural_feature', 'Culture of Cambodia. Throughout Cambodia''s long history, religion has been a major source of cultural inspiration. Over nearly two millennia, Cambodians have developed a unique Khmer belief from the syncreticism of indigenous animistic beliefs and the Indian religions of Buddhism and Hinduism.', 1, ''),
(8, 'lqfpgbk1641467779762930', 'Travel Agency', 'bus_station', 'The most trust worthy and reliable travel agency specializing in travel to South East Asian Region in today''s travel industry and we connect travelers from Asia, Europe, United State, Australia and other parts of the world.', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_taxi`
--

CREATE TABLE IF NOT EXISTS `t_taxi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `taxi_id` varchar(30) CHARACTER SET utf8 NOT NULL,
  `taxi_code` varchar(20) NOT NULL,
  `taxi_model` varchar(30) NOT NULL,
  `plate_number` varchar(20) NOT NULL,
  `driver_phone` varchar(20) NOT NULL,
  `pass_code` varchar(100) NOT NULL,
  `driver_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `driver_age` int(11) DEFAULT NULL,
  `driver_avatar` varchar(200) DEFAULT NULL,
  `serving_status` int(11) NOT NULL DEFAULT '1',
  `priority` bigint(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `job_status` int(11) NOT NULL DEFAULT '1',
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '5000',
  `company_name` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'Personal',
  `taxi_image` varchar(300) NOT NULL,
  `rating` float NOT NULL DEFAULT '3',
  `wallet` float NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taxi_id` (`taxi_id`),
  UNIQUE KEY `taxi_code` (`taxi_code`,`company_id`),
  UNIQUE KEY `taxi_code_2` (`taxi_code`,`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `t_taxi`
--

INSERT INTO `t_taxi` (`id`, `category_id`, `taxi_id`, `taxi_code`, `taxi_model`, `plate_number`, `driver_phone`, `pass_code`, `driver_name`, `driver_age`, `driver_avatar`, `serving_status`, `priority`, `status`, `job_status`, `latitude`, `longitude`, `company_id`, `company_name`, `taxi_image`, `rating`, `wallet`, `created_date`, `modified_date`) VALUES
(1, 1, 'lqfpgbk1441467779762930', '1000', 'PRUIS HYPERSONIC RED', '00000', '+85511328777', '5107c594902c5a5cb39e61379f602d902a0471be', 'Sony TOM', 27, NULL, 1, 1478507693576, 1, 1, '11.5460803', '104.9137921', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-06 11:36:02', '2016-11-07 15:39:36'),
(2, 2, 'o7y19qwe0s1467779770675', '1001', 'PRUIS HYPERSONIC RED', '00000', '+85511223344', '5107c594902c5a5cb39e61379f602d902a0471be', 'Honda ROM', 27, NULL, 1, 0, 1, 1, '11.5460771', '104.9137906', 1, 'Red', 'o7y19qwe0s1467779770675.png', 3, 0, '2016-07-06 11:36:10', '2016-11-07 13:10:40'),
(3, 1, 'wgc5ik8ns71467779821120', '1002', 'PRUIS HYPERSONIC RED', '00000', '85511222223', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Toyota PUT', 27, NULL, 1, 0, 1, 1, '11.549184', '104.928313', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-06 11:37:01', '2016-07-06 11:37:01'),
(5, 2, 'ku3sraxm5v1467857442485', '1003', 'PRUIS HYPERSONIC RED', '00000', '85511222224', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sohpeak CHENG', 30, NULL, 1, 0, 1, 1, '11.561780', '104.919922', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:10:42', '2016-07-07 09:10:42'),
(8, 2, 'qscej9hhfo1467857470319', '1004', 'PRUIS HYPERSONIC RED', '00000', '85511222225', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sokha SIM', 30, NULL, 1, 0, 1, 1, '11.562123', '104.914486', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:11:10', '2016-07-07 09:11:10'),
(10, 2, 'sqqpnivxwq1467857507385', '1005', 'PRUIS HYPERSONIC RED', '00000', '85511222226', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sohpal OUL', 30, NULL, 1, 0, 1, 1, '11.560168', '104.916761', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:11:47', '2016-07-07 09:11:47'),
(12, 1, '1g524ig3ai1467857613930', '1006', 'PRUIS HYPERSONIC RED', '00000', '85511222227', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Dara BUN', 32, NULL, 1, 0, 1, 1, '11.561177', '104.911343', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:33', '2016-07-07 09:13:33'),
(13, 1, 'sw0001od1x1467857617193', '1007', 'PRUIS HYPERSONIC RED', '00000', '85511222228', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Phearom PAN', 31, NULL, 1, 0, 1, 1, '11.558087', '104.908389', 4, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:37', '2016-07-07 09:13:37'),
(14, 1, '7qbx9yx9el1467857619990', '1008', 'PRUIS HYPERSONIC RED', '00000', '85511222229', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sothearith VAN', 33, NULL, 1, 0, 1, 1, '11.553057', '104.903920', 0, 'Super Cap', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:39', '2016-07-07 09:13:39'),
(15, 1, 'ro18x8hkd21467857623342', '1009', 'PRUIS HYPERSONIC RED', '00000', '85511222230', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sambath HEAN', 33, NULL, 1, 0, 1, 1, '11.553698', '104.909263', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:43', '2016-07-07 09:13:43'),
(16, 1, 'cffnpqkay01467857627006', '1010', 'PRUIS HYPERSONIC RED', '00000', '85511222231', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sovan THY', 35, NULL, 2, 1477553528101, 1, 1, '11.550954', '104.904636', 1, 'Red', 'http://www.redwhitetaxi.com/wp-content/uploads/2013/11/slider-images23-900x520.png', 3, 0, '2016-07-07 09:13:47', '2016-07-07 09:13:47'),
(17, 1, 'v34s6kbs2i1467858054979', '1010', 'Toyota Corolla Fielder', '00000', '011111122', '5107c594902c5a5cb39e61379f602d902a0471be', 'Tola SAY', 31, NULL, 1, 0, 1, 1, '11.560842', '104.905487', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:20:54', '2016-07-15 02:47:19'),
(18, 1, '7io8dxdvgk1467858069760', '1011', 'Toyota Corolla Fielder', '00000', '85510222233', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Odum HAY', 36, NULL, 1, 1477553856168, 1, 1, '11.541694', '104.921836', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:09', '2016-07-07 09:21:09'),
(19, 1, 'qf3j08ktdt1467858073004', '1012', 'Toyota Corolla Fielder', '00000', '85510222234', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Lada KY', 34, NULL, 1, 0, 1, 1, '11.541631', '104.917480', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:13', '2016-07-07 09:21:13'),
(20, 2, 'nc65dullib1467858076186', '1013', 'Toyota Corolla Fielder', '00000', '85510222235', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Ratana SOK', 40, NULL, 1, 0, 1, 1, '11.539831', '104.914153', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:16', '2016-07-07 09:21:16'),
(21, 1, '4a69uwrs8i1467858079173', '1014', 'Toyota Corolla Fielder', '00000', '85510222236', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Kimhong RATH', 44, NULL, 1, 0, 1, 1, '11.536330', '104.917554', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:19', '2016-07-07 09:21:19'),
(22, 2, 'o936p804gk1467858081902', '1015', 'Toyota Corolla Fielder', '00000', '85510222237', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Daro VUTH', 31, NULL, 1, 0, 1, 1, '11.536309', '104.916792', 2, 'Green Taxi', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:21', '2016-07-07 09:21:21'),
(23, 1, 'aiv9j6628h1467858084420', '1016', 'Toyota Corolla Fielder', '00000', '85510222238', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Chunleng KIM', 31, NULL, 1, 0, 1, 1, '11.538149', '104.921985', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:24', '2016-07-07 09:21:24'),
(24, 1, '2mldbdeh721467858087208', '1017', 'Toyota Corolla Fielder', '00000', '85510222239', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sothar KONG', 38, NULL, 1, 0, 1, 1, '11.541576', '104.924056', 2, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:27', '2016-07-07 09:21:27'),
(25, 2, 'p5nuhi33rf1467858091048', '1018', 'Toyota Corolla Fielder', '00000', '85510222240', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Pharath SONG', 43, NULL, 1, 0, 1, 1, '11.544381', '104.928104', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:31', '2016-07-07 09:21:31'),
(26, 1, 'fq0us02elr1467858094324', '1019', 'Toyota Corolla Fielder', '00000', '85510222241', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Vibol HAK', 28, NULL, 1, 0, 1, 1, '11.548975', '104.927103', 2, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:34', '2016-07-07 09:21:34'),
(27, 1, '95w6p1a1iu1467858097759', '1020', 'Toyota Corolla Fielder', '00000', '85510222242', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Pheaktra Rath', 40, NULL, 1, 1478484854277, 1, 1, '11.550815', '104.923927', 3, 'Yello Cap', 'http://www.ideegreen.it/wp-content/uploads/2012/11/nissan-leaf-taxi.jpeg', 3, 0, '2016-07-07 09:21:37', '2016-07-07 09:21:37'),
(37, 1, 'd2lkummgbl1476154111681', '555552ss', 'Ferrari', '11111', '85599666666', '7b21848ac9af35be0ddb2d6b9fc3851934db8420', 'Senhlys', 33, NULL, 1, 0, 1, 1, '', '', 5, 'public', '', 3, 0, '2016-10-11 02:48:31', '2016-10-11 02:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `t_taxi_category`
--

CREATE TABLE IF NOT EXISTS `t_taxi_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `seat_number` tinyint(4) NOT NULL,
  `image` varchar(200) NOT NULL,
  `initial_fare` int(11) NOT NULL,
  `per_minute_fee` int(11) NOT NULL,
  `per_km_fee` int(11) NOT NULL,
  `flag_down_fee` int(11) NOT NULL,
  `currency` varchar(11) NOT NULL DEFAULT 'KHR',
  `is_delete` int(11) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `t_taxi_category`
--

INSERT INTO `t_taxi_category` (`id`, `category_name`, `seat_number`, `image`, `initial_fare`, `per_minute_fee`, `per_km_fee`, `flag_down_fee`, `currency`, `is_delete`, `created_date`, `modified_date`) VALUES
(1, 'ឡាន ធម្មតា - Cap', 4, 'http://fasttaxiofca.com/sites/5981/cab469236.jpg', 4000, 200, 2000, 500, 'KHR', 2, '2016-07-18 00:00:00', '2016-07-18 02:05:10'),
(2, 'ឡានធំ​ - SUV', 4, 'http://cartype.com/pics/9134/full/mazda_bongo_gl-super_van_03.jpg', 5000, 250, 2500, 500, 'KHR', 2, '2016-07-18 00:00:00', '2016-07-18 04:08:10'),
(3, 'Tuk Tuk', 4, 'http://www.fuelarc.com/images/model_images/bus/tvs/king-basic-tuk-tuk/image_gallery/img_304.jpg', 2000, 200, 1500, 500, 'KHR', 2, '2016-10-19 00:00:00', '2016-10-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `travel_status` int(1) NOT NULL DEFAULT '1',
  `pass_code` varchar(100) NOT NULL DEFAULT 'UsEr@tAxI*168',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `phone_2` (`phone`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `name`, `user_id`, `avatar`, `phone`, `status`, `travel_status`, `pass_code`, `created_date`, `modified_date`) VALUES
(1, 'senhly', 'ks5mxeewxd1467687387613', 'ks5mxeewxd1467687387613.png', '+855967993377', 1, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-07-01 02:09:13', '2016-07-13 03:59:44'),
(76, 'darith', 'hph9qfn2yk1477536765372', 'https://i.imgsafe.org/1a2a12f675.jpg', '+85511328777', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-10-27 02:52:45', '2016-10-27 02:52:45'),
(79, 'plorksony', 'ifw04d9wky1477995159560', 'https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAL5AAAAJDUwNzBhMTdjLWJmZDktNGMxZS1iNjcxLWIzNDFjZWUyYTM3Mw.jpg', '+85599999999', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-01 17:12:39', '2016-11-01 17:12:39'),
(80, 'Sony', 'h8hantj2mc1478498274902', NULL, '+85593401361', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-07 12:57:54', '2016-11-07 12:57:54'),
(81, 'Sony', 'gqpjo779ey1478498536797', NULL, '+85593401391', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-07 13:02:16', '2016-11-07 13:02:16'),
(82, 'Sony', 'p7qpeorev71478499880834', NULL, '+85593401362', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-07 13:24:40', '2016-11-07 13:24:40'),
(83, 'Sony', 'cdahdvwlb21478501408247', NULL, '+85593401363', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-07 13:50:08', '2016-11-07 13:50:08'),
(84, 'Sony', '6me8vdh8bt1478501518070', NULL, '+85593401364', 1, 1, '5107c594902c5a5cb39e61379f602d902a0471be', '2016-11-07 13:51:58', '2016-11-07 13:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_booking`
--

CREATE TABLE IF NOT EXISTS `t_user_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(30) NOT NULL,
  `booking_id` varchar(30) NOT NULL,
  `booking_target` tinyint(4) NOT NULL,
  `target_id` varchar(30) NOT NULL,
  `taxi_id` varchar(30) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `pickup_time` datetime NOT NULL,
  `pickup_lat` varchar(20) NOT NULL,
  `pickup_long` varchar(20) NOT NULL,
  `pickup_address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `target_lat` varchar(20) DEFAULT NULL,
  `target_long` varchar(20) DEFAULT NULL,
  `booking_status` tinyint(4) NOT NULL,
  `request_date` datetime NOT NULL,
  `accept_date` datetime DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`pickup_time`,`pickup_lat`,`pickup_long`),
  KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_user_request`
--

CREATE TABLE IF NOT EXISTS `t_user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(30) NOT NULL,
  `taxi_id` varchar(30) DEFAULT NULL,
  `user_lat` varchar(20) NOT NULL,
  `user_long` varchar(20) NOT NULL,
  `taxi_lat` varchar(20) NOT NULL,
  `taxi_long` varchar(20) NOT NULL,
  `pickup_duration` varchar(30) DEFAULT NULL,
  `pickup_distance` varchar(30) DEFAULT NULL,
  `request_status` tinyint(4) NOT NULL,
  `request_id` varchar(30) NOT NULL,
  `booking_id` varchar(30) DEFAULT NULL,
  `request_date` datetime NOT NULL,
  `accept_date` datetime DEFAULT NULL,
  `target_lat` varchar(20) DEFAULT NULL,
  `target_long` varchar(20) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '2',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `request_id` (`request_id`),
  UNIQUE KEY `booking_id` (`booking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=695 ;

--
-- Dumping data for table `t_user_request`
--

INSERT INTO `t_user_request` (`id`, `user_id`, `taxi_id`, `user_lat`, `user_long`, `taxi_lat`, `taxi_long`, `pickup_duration`, `pickup_distance`, `request_status`, `request_id`, `booking_id`, `request_date`, `accept_date`, `target_lat`, `target_long`, `cancel_date`, `complete_date`, `is_deleted`, `description`) VALUES
(26, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.546193', '104.9137998', '11.5462047', '104.9138311', '1 min', '1 m', 7, 'wlc7nl81mu1477902520093', NULL, '2016-10-31 15:28:40', '2016-10-31 15:28:48', '11.5462047', '104.9138311', NULL, '2016-10-31 15:29:02', 2, NULL),
(28, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'eqly53dvlm1477902620696', NULL, '2016-10-31 15:30:20', '2016-10-31 15:30:28', NULL, NULL, '2016-10-31 15:30:45', NULL, 2, 'User not provide any reason'),
(30, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461942', '104.9138363', '1 min', '1 m', 4, 'af3wrhwkhc1477903020824', NULL, '2016-10-31 15:37:00', '2016-10-31 15:37:07', NULL, NULL, '2016-10-31 15:37:21', NULL, 2, 'User not provide any reason'),
(32, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 's2xlgrpyyy1477903261005', NULL, '2016-10-31 15:41:01', '2016-10-31 15:41:05', NULL, NULL, '2016-10-31 15:43:20', NULL, 2, 'User not provide any reason'),
(34, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'bm0c3bi0hi1477903420243', NULL, '2016-10-31 15:43:40', '2016-10-31 15:43:42', NULL, NULL, '2016-10-31 15:43:49', NULL, 2, 'User not provide any reason'),
(36, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, 'n1ttr3dtt21477903480012', NULL, '2016-10-31 15:44:40', '2016-10-31 15:44:41', NULL, NULL, NULL, NULL, 2, NULL),
(44, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, '69bvd4rvig1477903960610', NULL, '2016-10-31 15:52:40', '2016-10-31 15:52:43', NULL, NULL, NULL, NULL, 2, NULL),
(46, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'kj3kwcfamp1477904000861', NULL, '2016-10-31 15:53:20', '2016-10-31 15:53:39', NULL, NULL, '2016-10-31 15:53:47', NULL, 2, 'User not provide any reason'),
(48, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, '5a4qsrwplf1477904060467', NULL, '2016-10-31 15:54:20', '2016-10-31 15:54:22', NULL, NULL, NULL, NULL, 2, NULL),
(51, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, '8kklvh89471477904240675', NULL, '2016-10-31 15:57:20', '2016-10-31 15:57:22', NULL, NULL, '2016-10-31 15:57:49', NULL, 2, 'User not provide any reason'),
(53, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, '3919lg40n21477904280794', NULL, '2016-10-31 15:58:00', '2016-10-31 15:58:07', NULL, NULL, NULL, NULL, 2, NULL),
(56, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'n78yts1h2m1477904500199', NULL, '2016-10-31 16:01:40', '2016-10-31 16:01:42', NULL, NULL, '2016-10-31 16:02:13', NULL, 2, 'User not provide any reason'),
(58, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462042', '104.9138363', '1 min', '1 m', 2, 'qo7se9mgfi1477904540950', NULL, '2016-10-31 16:02:20', '2016-10-31 16:02:30', NULL, NULL, NULL, NULL, 2, NULL),
(60, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462042', '104.9138363', '1 min', '1 m', 2, 'pbrng1ujff1477904840982', NULL, '2016-10-31 16:07:20', '2016-10-31 16:07:23', NULL, NULL, NULL, NULL, 2, NULL),
(65, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, '9ekcqpvdj51477906540956', NULL, '2016-10-31 16:35:40', '2016-10-31 16:35:43', NULL, NULL, NULL, NULL, 2, NULL),
(67, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, '7eh361jiw31477906640897', NULL, '2016-10-31 16:37:20', '2016-10-31 16:37:23', NULL, NULL, NULL, NULL, 2, NULL),
(70, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, 'y8y9tcy1kw1477907040015', NULL, '2016-10-31 16:44:00', '2016-10-31 16:44:04', NULL, NULL, NULL, NULL, 2, NULL),
(72, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 7, 'euaavvxjlg1477907160451', NULL, '2016-10-31 16:46:00', '2016-10-31 16:46:03', '11.5462043', '104.9138363', NULL, '2016-10-31 16:46:16', 2, NULL),
(74, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462066', '104.9138302', '1 min', '1 m', 7, 'uel2bilc3h1477907200453', NULL, '2016-10-31 16:46:40', '2016-10-31 16:46:44', '11.5462043', '104.9138363', NULL, '2016-11-01 08:20:40', 2, NULL),
(76, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461613', '104.9138399', '1 min', '5 m', 4, '2v00ijw1p11477965340499', NULL, '2016-11-01 08:55:40', '2016-11-01 08:55:43', NULL, NULL, '2016-11-01 08:55:53', NULL, 2, 'User not provide any reason'),
(78, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461903', '104.9138276', '1 min', '2 m', 4, '4rkfuk6w8o1477965380376', NULL, '2016-11-01 08:56:20', '2016-11-01 08:56:23', NULL, NULL, '2016-11-01 08:56:26', NULL, 2, 'User not provide any reason'),
(80, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461956', '104.9138309', '1 min', '1 m', 4, 'iavpiu0sq81477965400291', NULL, '2016-11-01 08:56:40', '2016-11-01 08:56:43', NULL, NULL, '2016-11-01 08:56:46', NULL, 2, 'User not provide any reason'),
(82, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 2, 'oyp41k0fy81477965520632', NULL, '2016-11-01 08:58:40', '2016-11-01 08:58:42', NULL, NULL, NULL, NULL, 2, NULL),
(84, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'dsruhspsi41477965660746', NULL, '2016-11-01 09:01:00', '2016-11-01 09:01:02', NULL, NULL, '2016-11-01 09:01:06', NULL, 2, 'User not provide any reason'),
(86, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462031', '104.913836', '1 min', '1 m', 4, '7b8oy0cmdc1477965680669', NULL, '2016-11-01 09:01:20', '2016-11-01 09:01:24', NULL, NULL, '2016-11-01 09:02:18', NULL, 2, 'User not provide any reason'),
(88, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, '08tqq2hh651477965940632', NULL, '2016-11-01 09:05:40', '2016-11-01 09:05:41', NULL, NULL, '2016-11-01 09:05:45', NULL, 2, 'User not provide any reason'),
(90, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462075', '104.9138186', '1 min', '1 m', 4, 'smq4pgs0e11477965960955', NULL, '2016-11-01 09:06:00', '2016-11-01 09:06:02', NULL, NULL, '2016-11-01 09:06:04', NULL, 2, 'User not provide any reason'),
(92, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462073', '104.9138194', '1 min', '1 m', 4, 'ro17a2d6991477966080384', NULL, '2016-11-01 09:08:00', '2016-11-01 09:08:01', NULL, NULL, '2016-11-01 09:08:07', NULL, 2, 'User not provide any reason'),
(94, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462001', '104.9138385', '1 min', '1 m', 4, 'ye8s2i337p1477966100279', NULL, '2016-11-01 09:08:20', '2016-11-01 09:08:22', NULL, NULL, '2016-11-01 09:08:54', NULL, 2, 'User not provide any reason'),
(96, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.546201', '104.9138381', '1 min', '1 m', 4, 'wee343xgal1477966220665', NULL, '2016-11-01 09:10:20', '2016-11-01 09:10:22', NULL, NULL, '2016-11-01 09:10:29', NULL, 2, 'User not provide any reason'),
(98, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.546199', '104.9138343', '1 min', '1 m', 4, 'yartlwmwj31477966240548', NULL, '2016-11-01 09:10:40', '2016-11-01 09:10:44', NULL, NULL, '2016-11-01 09:10:47', NULL, 2, 'User not provide any reason'),
(100, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461994', '104.9138345', '1 min', '1 m', 7, '02x8uhlyjy1477966260589', NULL, '2016-11-01 09:11:00', '2016-11-01 09:11:02', '11.5461994', '104.9138345', NULL, '2016-11-01 09:11:19', 2, NULL),
(102, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462009', '104.913824', '1 min', '1 m', 4, 'r393qmxu1n1477966540905', NULL, '2016-11-01 09:15:40', '2016-11-01 09:15:42', NULL, NULL, '2016-11-01 09:15:45', NULL, 2, 'User not provide any reason'),
(104, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462012', '104.9138305', '1 min', '1 m', 4, 'apeudqgll11477966560190', NULL, '2016-11-01 09:16:00', '2016-11-01 09:16:01', NULL, NULL, '2016-11-01 09:16:07', NULL, 2, 'User not provide any reason'),
(106, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 7, 'int4ub13901477966580644', NULL, '2016-11-01 09:16:20', '2016-11-01 09:16:22', '11.5462043', '104.9138363', NULL, '2016-11-01 09:16:44', 2, NULL),
(108, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'pevew1sx6s1477966620120', NULL, '2016-11-01 09:17:00', '2016-11-01 09:17:05', NULL, NULL, '2016-11-01 09:17:10', NULL, 2, 'User not provide any reason'),
(110, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 7, '9p9qliveed1477966640284', NULL, '2016-11-01 09:17:20', '2016-11-01 09:17:22', '11.5462043', '104.9138363', NULL, '2016-11-01 09:17:32', 2, NULL),
(112, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461994', '104.9138382', '1 min', '1 m', 4, '33fsra21ej1477966700999', NULL, '2016-11-01 09:18:20', '2016-11-01 09:18:24', NULL, NULL, '2016-11-01 09:18:30', NULL, 2, 'User not provide any reason'),
(114, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.546198', '104.9138371', '1 min', '1 m', 4, 'i58amr4i911477966720520', NULL, '2016-11-01 09:18:40', '2016-11-01 09:19:05', NULL, NULL, '2016-11-01 09:19:10', NULL, 2, 'User not provide any reason'),
(116, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.546194', '104.9138367', '1 min', '1 m', 4, '03ihbu29vr1477966760293', NULL, '2016-11-01 09:19:20', '2016-11-01 09:19:21', NULL, NULL, '2016-11-01 09:19:23', NULL, 2, 'User not provide any reason'),
(118, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461947', '104.9138367', '1 min', '1 m', 4, 'rim4t0j4pw1477966780928', NULL, '2016-11-01 09:19:40', '2016-11-01 09:21:08', NULL, NULL, '2016-11-01 09:21:23', NULL, 2, 'User not provide any reason'),
(122, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461984', '104.9138366', '1 min', '1 m', 4, 'crgx9rged71477967240780', NULL, '2016-11-01 09:27:20', '2016-11-01 09:27:22', NULL, NULL, '2016-11-01 09:27:33', NULL, 2, 'User not provide any reason'),
(128, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462004', '104.9138367', '1 min', '1 m', 4, 'jumi4c2hv31477967440560', NULL, '2016-11-01 09:30:40', '2016-11-01 09:30:42', NULL, NULL, '2016-11-01 09:33:14', NULL, 2, 'User not provide any reason'),
(130, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462006', '104.9138372', '1 min', '1 m', 4, '7y9k7ax8in1477968040530', NULL, '2016-11-01 09:40:40', '2016-11-01 09:40:45', NULL, NULL, '2016-11-01 09:42:32', NULL, 2, 'User not provide any reason'),
(132, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, '91v3uvcqke1477968460946', NULL, '2016-11-01 09:47:40', '2016-11-01 09:47:45', NULL, NULL, '2016-11-01 09:48:24', NULL, 2, 'User not provide any reason'),
(134, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462045', '104.9138334', '1 min', '1 m', 4, '1ybgt8yu5a1477968620032', NULL, '2016-11-01 09:50:20', '2016-11-01 09:50:23', NULL, NULL, '2016-11-01 09:51:11', NULL, 2, 'User not provide any reason'),
(136, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'e5dhp4wr7c1477968700330', NULL, '2016-11-01 09:51:40', '2016-11-01 09:51:42', NULL, NULL, '2016-11-01 09:52:02', NULL, 2, 'User not provide any reason'),
(138, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.54621', '104.9138306', '1 min', '1 m', 4, '33tj8guyeu1477968840327', NULL, '2016-11-01 09:54:00', '2016-11-01 09:54:01', NULL, NULL, '2016-11-01 09:54:38', NULL, 2, 'User not provide any reason'),
(140, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462048', '104.9138358', '1 min', '1 m', 5, 'ct17b4bf1r1477968900449', NULL, '2016-11-01 09:55:00', '2016-11-01 09:55:03', NULL, NULL, '2016-11-01 09:55:18', NULL, 2, 'Driver not provide any reason'),
(142, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462048', '104.9138358', '1 min', '1 m', 5, 'rxvi431mab1477968980815', NULL, '2016-11-01 09:56:20', '2016-11-01 09:56:23', NULL, NULL, '2016-11-01 09:56:27', NULL, 2, 'Driver not provide any reason'),
(144, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462048', '104.9138358', '1 min', '1 m', 5, 'c70jc8vkuq1477969180899', NULL, '2016-11-01 09:59:40', '2016-11-01 09:59:43', NULL, NULL, '2016-11-01 09:59:49', NULL, 2, 'Driver not provide any reason'),
(146, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462091', '104.913745', '11.5462048', '104.9138358', '1 min', '1 m', 5, 'sijr40qgvs1477969300543', NULL, '2016-11-01 10:01:40', '2016-11-01 10:01:41', NULL, NULL, '2016-11-01 10:01:43', NULL, 2, 'Driver not provide any reason'),
(150, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462091', '104.913745', '11.5462048', '104.9138358', '1 min', '1 m', 7, 'pp2k42t2oj1477969360215', NULL, '2016-11-01 10:02:40', '2016-11-01 10:02:42', '11.5462048', '104.9138358', NULL, '2016-11-01 10:04:17', 2, NULL),
(152, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462018', '104.9138372', '1 min', '1 m', 7, 'hdlih3olbu1477969500236', NULL, '2016-11-01 10:05:00', '2016-11-01 10:05:02', '11.5462018', '104.9138372', NULL, '2016-11-01 10:06:27', 2, NULL),
(154, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5461714', '104.9138164', '11.5462133', '104.9138224', '1 min', '5 m', 7, 'erh4wampt11477969600376', NULL, '2016-11-01 10:06:40', '2016-11-01 10:06:41', '11.5462133', '104.9138224', NULL, '2016-11-01 10:07:44', 2, NULL),
(156, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462127', '104.9138235', '1 min', '1 m', 7, 'jojrvvg0m81477969800525', NULL, '2016-11-01 10:10:00', '2016-11-01 10:10:02', '11.5462127', '104.9138235', NULL, '2016-11-01 10:10:18', 2, NULL),
(158, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462499', '104.9137906', '11.5462114', '104.9138254', '1 min', '4 m', 7, 'osxmehmr2g1477969900452', NULL, '2016-11-01 10:11:40', '2016-11-01 10:11:41', '11.5462114', '104.9138254', NULL, '2016-11-01 10:11:49', 2, NULL),
(160, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462107', '104.9138265', '1 min', '1 m', 7, '96jgpe3wvu1477969960566', NULL, '2016-11-01 10:12:40', '2016-11-01 10:12:42', '11.5462107', '104.9138265', NULL, '2016-11-01 10:12:51', 2, NULL),
(162, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462499', '104.9137906', '11.5462104', '104.9138269', '1 min', '4 m', 7, '4cokahvuxv1477970040507', NULL, '2016-11-01 10:14:00', '2016-11-01 10:14:01', '11.5462104', '104.9138269', NULL, '2016-11-01 10:14:17', 2, NULL),
(164, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 4, 'fwo4wd1x711477970660609', NULL, '2016-11-01 10:24:20', '2016-11-01 10:24:22', NULL, NULL, '2016-11-01 10:24:26', NULL, 2, 'User not provide any reason'),
(166, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5461998', '104.9138441', '1 min', '1 m', 4, 'q7h24hkhfv1477970740870', NULL, '2016-11-01 10:25:40', '2016-11-01 10:25:44', NULL, NULL, '2016-11-01 10:25:48', NULL, 2, 'User not provide any reason'),
(168, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462049', '104.913834', '1 min', '1 m', 7, '8baod2oep01477970780758', NULL, '2016-11-01 10:26:20', '2016-11-01 10:26:22', '11.5462049', '104.913834', NULL, '2016-11-01 10:26:36', 2, NULL),
(170, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462056', '104.9138332', '1 min', '1 m', 7, '90q3dwkxfj1477970840020', NULL, '2016-11-01 10:27:20', '2016-11-01 10:27:21', '11.4619789', '104.8742447', NULL, '2016-11-01 11:36:13', 2, NULL),
(175, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 4, 'dqh8t56tw71477986320642', NULL, '2016-11-01 14:45:20', '2016-11-01 14:45:28', NULL, NULL, '2016-11-01 14:45:31', NULL, 2, 'User not provide any reason'),
(181, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'ushngbuyll1477986420096', NULL, '2016-11-01 14:47:00', '2016-11-01 14:47:22', '11.5373859', '104.8486862', NULL, '2016-11-01 14:50:25', 2, NULL),
(184, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'mnn0rdmtmo1477986660743', NULL, '2016-11-01 14:51:00', '2016-11-01 14:51:09', '11.5373859', '104.8486862', NULL, '2016-11-01 14:53:06', 2, NULL),
(186, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'pas0or38ex1477987320548', NULL, '2016-11-01 15:02:00', '2016-11-01 15:02:08', '11.5373859', '104.8486862', NULL, '2016-11-01 15:03:18', 2, NULL),
(188, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, '9lm1o51dgr1477987420121', NULL, '2016-11-01 15:03:40', '2016-11-01 15:03:56', '11.5373859', '104.8486862', NULL, '2016-11-01 15:04:09', 2, NULL),
(190, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'drmx03dst11477987500904', NULL, '2016-11-01 15:05:00', '2016-11-01 15:05:18', '11.5373859', '104.8486862', NULL, '2016-11-01 15:05:33', 2, NULL),
(192, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, '9bvloqtpy41477991100816', NULL, '2016-11-01 16:05:00', '2016-11-01 16:05:03', '11.5373859', '104.8486862', NULL, '2016-11-01 16:05:21', 2, NULL),
(194, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'u9h8jakpst1477991200023', NULL, '2016-11-01 16:06:40', '2016-11-01 16:06:41', '11.5373859', '104.8486862', NULL, '2016-11-01 16:06:49', 2, NULL),
(196, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '1 min', '1 m', 7, 'axkawp5k1y1477991260486', NULL, '2016-11-01 16:07:40', '2016-11-01 16:07:42', '11.5373859', '104.8486862', NULL, '2016-11-01 16:07:48', 2, NULL),
(198, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'ss1m0s6rfe1477991300309', NULL, '2016-11-01 16:08:20', '2016-11-01 16:08:21', '11.5373859', '104.8486862', NULL, '2016-11-01 16:08:28', 2, NULL),
(200, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'i2mgmioyda1477991340486', NULL, '2016-11-01 16:09:00', '2016-11-01 16:09:06', '11.5373859', '104.8486862', NULL, '2016-11-01 16:09:35', 2, NULL),
(202, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, '2rxug30iga1477991440993', NULL, '2016-11-01 16:10:40', '2016-11-01 16:10:42', '11.5373859', '104.8486862', NULL, '2016-11-01 16:10:48', 2, NULL),
(204, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5373859', '104.8486862', '29 mins', '9.3 km', 7, 'g0nokkmyxw1478000440339', NULL, '2016-11-01 18:40:40', '2016-11-01 18:40:52', '11.5462052', '104.9138402', NULL, '2016-11-02 09:47:41', 2, NULL),
(206, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5462043', '104.9138363', '11.5462043', '104.9138363', '1 min', '1 m', 7, 'q70qsbyf3b1478055800125', NULL, '2016-11-02 10:03:20', '2016-11-02 10:03:23', '11.5341873', '104.8823624', NULL, '2016-11-02 10:04:36', 2, NULL),
(208, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5341873', '104.8823624', '14 mins', '4.4 km', 7, 'gvjxibetmy1478057140015', NULL, '2016-11-02 10:25:40', '2016-11-02 10:25:46', '11.5512676', '104.9163208', NULL, '2016-11-02 10:29:59', 2, NULL),
(211, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5512676', '104.9163208', '4 mins', '0.9 km', 7, 'm4sdx0v5sy1478058760997', NULL, '2016-11-02 10:52:40', '2016-11-02 10:52:43', '11.5450249', '104.9152756', NULL, '2016-11-02 11:01:25', 2, NULL),
(214, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'm6k77ap4qt1478059320500', NULL, '2016-11-02 11:02:00', '2016-11-02 11:02:02', '11.5450249', '104.9152756', NULL, '2016-11-02 11:02:46', 2, NULL),
(216, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 4, '63pi71btbg1478059400422', NULL, '2016-11-02 11:03:20', '2016-11-02 11:03:31', NULL, NULL, '2016-11-02 11:03:38', NULL, 2, 'User not provide any reason'),
(219, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 4, 'c1p163o7ww1478059460686', NULL, '2016-11-02 11:04:20', '2016-11-02 11:04:22', NULL, NULL, '2016-11-02 11:04:41', NULL, 2, 'User not provide any reason'),
(221, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'b9poaaoa7e1478059560231', NULL, '2016-11-02 11:06:00', '2016-11-02 11:06:07', '11.5450249', '104.9152756', NULL, '2016-11-02 11:07:42', 2, NULL),
(223, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'uhwykqdj6r1478059720512', NULL, '2016-11-02 11:08:40', '2016-11-02 11:08:43', '11.5450249', '104.9152756', NULL, '2016-11-02 11:11:04', 2, NULL),
(225, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 5, 'il0ylrhx271478059920262', NULL, '2016-11-02 11:12:00', '2016-11-02 11:12:03', NULL, NULL, '2016-11-02 11:14:24', NULL, 2, 'Driver not provide any reason'),
(228, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '5dkrna1e371478061140428', NULL, '2016-11-02 11:32:20', '2016-11-02 11:32:21', '11.5450249', '104.9152756', NULL, '2016-11-02 11:37:37', 2, NULL),
(230, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'ch51uh8a6u1478061580267', NULL, '2016-11-02 11:39:40', '2016-11-02 11:39:42', '11.5450249', '104.9152756', NULL, '2016-11-02 11:42:06', 2, NULL),
(234, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 4, 'o48jagb0gu1478061800229', NULL, '2016-11-02 11:43:20', '2016-11-02 11:43:21', '11.5450249', '104.9152756', '2016-11-02 11:43:55', '2016-11-02 11:43:49', 2, 'User not provide any reason'),
(236, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'er7o1193n81478061860055', NULL, '2016-11-02 11:44:20', '2016-11-02 11:44:21', '11.5450249', '104.9152756', NULL, '2016-11-02 11:50:17', 2, NULL),
(239, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'yy2a0lumxy1478062320772', NULL, '2016-11-02 11:52:00', '2016-11-02 11:52:09', '11.5450249', '104.9152756', NULL, '2016-11-02 11:52:39', 2, NULL),
(264, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '163qqq7o0e1478069220494', NULL, '2016-11-02 13:47:00', '2016-11-02 13:47:02', '11.5450249', '104.9152756', NULL, '2016-11-02 13:51:55', 2, NULL),
(267, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '2u9b5rer6h1478069560639', NULL, '2016-11-02 13:52:40', '2016-11-02 13:52:49', '11.5450249', '104.9152756', NULL, '2016-11-02 13:56:23', 2, NULL),
(270, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '46r3y71kty1478069820396', NULL, '2016-11-02 13:57:00', '2016-11-02 13:57:13', '11.5450249', '104.9152756', NULL, '2016-11-02 14:04:09', 2, NULL),
(278, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'bqfrhi9e1u1478070740350', NULL, '2016-11-02 14:12:20', '2016-11-02 14:12:25', '11.5450249', '104.9152756', NULL, '2016-11-02 14:13:21', 2, NULL),
(280, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'qqds7kiwp11478071340273', NULL, '2016-11-02 14:22:20', '2016-11-02 14:22:53', '11.5450249', '104.9152756', NULL, '2016-11-02 14:23:06', 2, NULL),
(283, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '71voo353mo1478071420510', NULL, '2016-11-02 14:23:40', '2016-11-02 14:24:05', '11.5450249', '104.9152756', NULL, '2016-11-02 14:25:27', 2, NULL),
(291, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 5, '05xn95uuie1478072080199', NULL, '2016-11-02 14:34:40', '2016-11-02 14:34:42', NULL, NULL, '2016-11-02 14:34:48', NULL, 2, 'Driver not provide any reason'),
(293, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '26utktmi501478072100215', NULL, '2016-11-02 14:35:00', '2016-11-02 14:35:01', '11.5450249', '104.9152756', NULL, '2016-11-02 14:35:23', 2, NULL),
(296, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'r0h8xv4hdq1478072200864', NULL, '2016-11-02 14:36:40', '2016-11-02 14:36:47', '11.5450249', '104.9152756', NULL, '2016-11-02 14:37:01', 2, NULL),
(299, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '7f43l1h12a1478072280081', NULL, '2016-11-02 14:38:00', '2016-11-02 14:38:01', '11.5450249', '104.9152756', NULL, '2016-11-02 14:55:38', 2, NULL),
(302, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '1rq16v7c2p1478073381022', NULL, '2016-11-02 14:56:21', '2016-11-02 14:56:23', '11.5450249', '104.9152756', NULL, '2016-11-02 14:56:36', 2, NULL),
(305, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'rg4kr34uij1478073540846', NULL, '2016-11-02 14:59:00', '2016-11-02 14:59:03', '11.5450249', '104.9152756', NULL, '2016-11-02 14:59:27', 2, NULL),
(307, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, 'jt6kdhup4v1478073680171', NULL, '2016-11-02 15:01:20', '2016-11-02 15:01:29', '11.5450249', '104.9152756', NULL, '2016-11-02 15:01:56', 2, NULL),
(320, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 5, 'qs9e5vpre21478075300491', NULL, '2016-11-02 15:28:20', '2016-11-02 15:28:22', NULL, NULL, '2016-11-02 15:28:24', NULL, 2, 'Driver not provide any reason'),
(358, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '4hv8f21pmc1478077420148', NULL, '2016-11-02 16:03:40', '2016-11-02 16:03:52', '11.5450249', '104.9152756', NULL, '2016-11-02 16:04:17', 2, NULL),
(361, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 7, '8w084a4sj61478077640047', NULL, '2016-11-02 16:07:20', '2016-11-02 16:07:29', '11.5450249', '104.9152756', NULL, '2016-11-02 16:09:41', 2, NULL),
(363, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5450249', '104.9152756', '6 mins', '1.5 km', 5, '8130rmpd831478078380274', NULL, '2016-11-02 16:19:40', '2016-11-02 16:19:50', NULL, NULL, '2016-11-02 16:19:53', NULL, 2, 'Driver not provide any reason'),
(456, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5567083', '104.9123333', '11.5462043', '104.9138363', '5 mins', '1.2 km', 7, 'knfqqkkupl1478137800449', NULL, '2016-11-03 08:50:00', '2016-11-03 08:50:17', '11.5547962', '104.9152298', NULL, '2016-11-03 08:54:38', 2, NULL),
(459, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5547962', '104.9152298', '2 mins', '0.5 km', 7, '8d3o2c3vy51478138140046', NULL, '2016-11-03 08:55:40', '2016-11-03 08:55:46', '11.5547962', '104.9152298', NULL, '2016-11-03 08:58:09', 2, NULL),
(462, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5547962', '104.9152298', '2 mins', '0.5 km', 7, 't2cwi96nj31478138340450', NULL, '2016-11-03 08:59:00', '2016-11-03 08:59:04', '11.5547962', '104.9152298', NULL, '2016-11-03 09:02:24', 2, NULL),
(465, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5547962', '104.9152298', '2 mins', '0.5 km', 7, '98wjgtrkif1478138600508', NULL, '2016-11-03 09:03:20', '2016-11-03 09:03:25', '11.5547962', '104.9152298', NULL, '2016-11-03 09:06:19', 2, NULL),
(468, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5547962', '104.9152298', '2 mins', '0.5 km', 7, '7uyeftuj441478138860027', NULL, '2016-11-03 09:07:40', '2016-11-03 09:07:42', '11.6050291', '104.8449783', NULL, '2016-11-03 09:50:07', 2, NULL),
(473, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.6050291', '104.8449783', '35 mins', '12.8 km', 4, 'mmr3f5g7fu1478141500509', NULL, '2016-11-03 09:51:40', '2016-11-03 09:51:57', '11.555852499999993', '104.91226953124996', '2016-11-03 09:52:12', NULL, 2, 'User not provide any reason'),
(484, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.6050291', '104.8449783', '35 mins', '12.8 km', 7, '7waqg05mdk1478142080814', NULL, '2016-11-03 10:01:20', '2016-11-03 10:01:36', '11.6050291', '104.8449783', NULL, '2016-11-03 10:02:04', 2, NULL),
(486, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.6050291', '104.8449783', '35 mins', '12.8 km', 5, '7cfh7qu7pn1478142220351', NULL, '2016-11-03 10:03:40', '2016-11-03 10:03:43', '0', '0', '2016-11-03 10:03:48', NULL, 2, 'Driver not provide any reason'),
(504, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5462043', '104.9138363', '5 mins', '1.2 km', 7, '4shctdf5o51478145500096', NULL, '2016-11-03 10:58:20', '2016-11-03 10:58:24', '11.5462043', '104.9138363', NULL, '2016-11-03 10:59:44', 2, NULL),
(507, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5462043', '104.9138363', '5 mins', '1.2 km', 7, '3su1drknym1478145620967', NULL, '2016-11-03 11:00:20', '2016-11-03 11:00:23', '11.5461629', '104.9138929', NULL, '2016-11-03 11:30:04', 2, NULL),
(512, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.546202', '104.9138337', '5 mins', '1.2 km', 7, 'wpi35yoi5w1478148560834', NULL, '2016-11-03 11:49:20', '2016-11-03 11:49:25', '11.546202', '104.9138337', NULL, '2016-11-03 11:49:52', 2, NULL),
(515, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5461883', '104.9138405', '5 mins', '1.2 km', 7, '9x68qsfpmd1478148620996', NULL, '2016-11-03 11:50:20', '2016-11-03 11:50:22', '11.5614719', '104.9095459', NULL, '2016-11-03 14:23:14', 2, NULL),
(529, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5462076', '104.9137122', '5 mins', '1.2 km', 7, 'w62d6acrj61478231860671', NULL, '2016-11-04 10:57:40', '2016-11-04 10:57:48', '11.5462076', '104.9137122', NULL, '2016-11-04 10:58:30', 2, NULL),
(531, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, 'xni0awlpeu1478231920331', NULL, '2016-11-04 10:58:40', '2016-11-04 10:58:43', '11.5460771', '104.9137906', NULL, '2016-11-04 10:58:53', 2, NULL),
(533, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, 'cajhq2jlyb1478231960200', NULL, '2016-11-04 10:59:20', '2016-11-04 10:59:23', '11.5460771', '104.9137906', NULL, '2016-11-04 11:04:07', 2, NULL),
(535, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, 'od3ufghe2s1478232260469', NULL, '2016-11-04 11:04:20', '2016-11-04 11:04:22', '11.5460771', '104.9137906', NULL, '2016-11-04 11:05:00', 2, NULL),
(544, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, '4oqubwkx4r1478232500258', NULL, '2016-11-04 11:08:20', '2016-11-04 11:08:36', '11.5460771', '104.9137906', NULL, '2016-11-04 11:08:53', 2, NULL),
(546, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.5566983', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, 'bboofkjplf1478232860502', NULL, '2016-11-04 11:14:20', '2016-11-04 11:14:23', '11.5460771', '104.9137906', NULL, '2016-11-04 13:31:00', 2, NULL),
(548, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5460879', '104.9137939', '5 mins', '1.2 km', 7, 'ir8pco8n0j1478241800692', NULL, '2016-11-04 13:43:20', '2016-11-04 13:43:29', '11.5652571', '104.9032745', NULL, '2016-11-04 14:35:59', 2, NULL),
(550, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5652571', '104.9032745', '7 mins', '1.8 km', 7, 'tf7rmsb3191478245060657', NULL, '2016-11-04 14:37:40', '2016-11-04 14:37:42', '11.5483732', '104.9100647', NULL, '2016-11-04 14:42:48', 2, NULL),
(552, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5483732', '104.9100647', '5 mins', '1.1 km', 7, 'wsp5afhkmb1478245400242', NULL, '2016-11-04 14:43:20', '2016-11-04 14:43:22', '11.5522079', '104.9309235', NULL, '2016-11-04 14:45:43', 2, NULL),
(554, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5522079', '104.9309235', '10 mins', '2.5 km', 7, 'qs6qvt94ga1478245560950', NULL, '2016-11-04 14:46:00', '2016-11-04 14:46:02', '11.5209913', '104.9017334', NULL, '2016-11-04 15:07:08', 2, NULL),
(564, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'bwbwbhwi7c1478247840381', NULL, '2016-11-04 15:24:00', '2016-11-04 15:24:06', '11.5209913', '104.9017334', NULL, '2016-11-04 15:24:45', 2, NULL),
(566, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'hsqna5v03j1478248060449', NULL, '2016-11-04 15:27:40', '2016-11-04 15:27:46', '11.5209913', '104.9017334', NULL, '2016-11-04 15:28:06', 2, NULL),
(568, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, '6ujk6cueeo1478248200070', NULL, '2016-11-04 15:30:00', '2016-11-04 15:30:05', '11.5209913', '104.9017334', NULL, '2016-11-04 15:30:15', 2, NULL),
(570, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'ba0um9s97h1478248320073', NULL, '2016-11-04 15:32:00', '2016-11-04 15:32:01', '11.5209913', '104.9017334', NULL, '2016-11-04 15:32:36', 2, NULL),
(572, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'u7362b67mw1478248460429', NULL, '2016-11-04 15:34:20', '2016-11-04 15:34:22', '11.5209913', '104.9017334', NULL, '2016-11-04 15:35:28', 2, NULL),
(574, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'dud63iaxoj1478248560105', NULL, '2016-11-04 15:36:00', '2016-11-04 15:36:08', '11.5209913', '104.9017334', NULL, '2016-11-04 15:36:32', 2, NULL),
(576, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'vr5f6bxqox1478248720022', NULL, '2016-11-04 15:38:40', '2016-11-04 15:38:41', '11.5209913', '104.9017334', NULL, '2016-11-04 15:38:59', 2, NULL),
(578, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'jwx696b3tj1478249620514', NULL, '2016-11-04 15:53:40', '2016-11-04 15:53:42', '11.5209913', '104.9017334', NULL, '2016-11-04 15:54:52', 2, NULL),
(580, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'hk6158ksv31478249760780', NULL, '2016-11-04 15:56:00', '2016-11-04 15:56:02', '11.5209913', '104.9017334', NULL, '2016-11-04 16:02:23', 2, NULL),
(582, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5209913', '104.9017334', '18 mins', '4.7 km', 7, 'lb9tq6uc981478250160993', NULL, '2016-11-04 16:02:40', '2016-11-04 16:02:42', '11.5515394', '104.914093', NULL, '2016-11-04 16:03:02', 2, NULL),
(584, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, 'b73if2rqhb1478250960371', NULL, '2016-11-04 16:16:00', '2016-11-04 16:16:03', '11.5515394', '104.914093', NULL, '2016-11-04 16:16:24', 2, NULL),
(586, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, 'rq3t9mdttj1478251200562', NULL, '2016-11-04 16:20:00', '2016-11-04 16:20:03', '11.5515394', '104.914093', NULL, '2016-11-04 16:20:12', 2, NULL),
(588, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, '9rcjl1p8le1478252380871', NULL, '2016-11-04 16:39:40', '2016-11-04 16:39:53', '11.5515394', '104.914093', NULL, '2016-11-04 16:40:03', 2, NULL),
(598, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, '94gso5pw0e1478252920717', NULL, '2016-11-04 16:48:40', '2016-11-04 16:48:55', '11.5515394', '104.914093', NULL, '2016-11-04 16:49:16', 2, NULL),
(600, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, 'x5jywc3jwq1478253040410', NULL, '2016-11-04 16:50:40', '2016-11-04 16:50:42', '11.5515394', '104.914093', NULL, '2016-11-04 16:50:54', 2, NULL),
(602, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, '6lauh78mhd1478253280470', NULL, '2016-11-04 16:54:40', '2016-11-04 16:54:54', '11.5515394', '104.914093', NULL, '2016-11-04 16:55:14', 2, NULL),
(604, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5515394', '104.914093', '3 mins', '0.6 km', 7, '03yx57bb0k1478253620383', NULL, '2016-11-04 17:00:20', '2016-11-04 17:00:25', '11.5615463', '104.906456', NULL, '2016-11-04 17:00:42', 2, NULL),
(606, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5615463', '104.906456', '5 mins', '1.1 km', 7, 'iu2uee5ehw1478254000200', NULL, '2016-11-04 17:06:40', '2016-11-04 17:06:44', '11.5566692', '104.9050827', NULL, '2016-11-04 17:07:35', 2, NULL),
(608, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5566692', '104.9050827', '4 mins', '1.0 km', 7, '2fl08s3pvk1478254200508', NULL, '2016-11-04 17:10:00', '2016-11-04 17:10:06', '11.5506144', '104.9140091', NULL, '2016-11-04 17:11:48', 2, NULL),
(621, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 4, 'm0ude08a311478487760752', NULL, '2016-11-07 10:02:40', '2016-11-07 10:02:42', '0', '0', '2016-11-07 10:02:47', NULL, 2, 'User not provide any reason'),
(626, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'dpr8testxu1478487840735', NULL, '2016-11-07 10:04:00', '2016-11-07 10:04:02', '11.5473738', '104.9127884', NULL, '2016-11-07 10:09:46', 2, NULL),
(636, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'obrscv7b581478489240372', NULL, '2016-11-07 10:27:20', '2016-11-07 10:27:23', '11.5473738', '104.9127884', NULL, '2016-11-07 10:31:45', 2, NULL),
(639, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'vi6mtoibxj1478492760359', NULL, '2016-11-07 11:26:00', '2016-11-07 11:26:07', '11.5473738', '104.9127884', NULL, '2016-11-07 11:26:35', 2, NULL),
(642, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'epw14kuuwd1478492840718', NULL, '2016-11-07 11:27:20', '2016-11-07 11:27:22', '11.5473738', '104.9127884', NULL, '2016-11-07 11:29:22', 2, NULL),
(651, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, '43d4srfdii1478493140013', NULL, '2016-11-07 11:32:20', '2016-11-07 11:32:23', '11.5473738', '104.9127884', NULL, '2016-11-07 11:32:51', 2, NULL),
(654, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'w9e19rcl9c1478493200429', NULL, '2016-11-07 11:33:20', '2016-11-07 11:33:22', '11.5473738', '104.9127884', NULL, '2016-11-07 11:35:02', 2, NULL),
(657, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5473738', '104.9127884', '5 mins', '1.2 km', 7, 'w7rpbx936i1478493340708', NULL, '2016-11-07 11:35:40', '2016-11-07 11:35:43', '11.5465298', '104.9107285', NULL, '2016-11-07 11:36:39', 2, NULL),
(669, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5465298', '104.9107285', '5 mins', '1.1 km', 7, 'via8sk83k81478497280977', NULL, '2016-11-07 12:41:20', '2016-11-07 12:41:26', '11.5628643', '104.9187393', NULL, '2016-11-07 12:44:25', 2, NULL),
(676, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5628643', '104.9187393', '6 mins', '1.4 km', 5, 'ctwc751cmg1478497620734', NULL, '2016-11-07 12:47:00', '2016-11-07 12:47:03', '11.554787499999996', '104.90991015624994', '2016-11-07 12:47:15', NULL, 2, 'Driver not provide any reason'),
(679, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5628643', '104.9187393', '6 mins', '1.4 km', 7, 'cwi4e3vuko1478497880272', NULL, '2016-11-07 12:51:20', '2016-11-07 12:51:29', '11.5628643', '104.9187393', NULL, '2016-11-07 12:52:30', 2, NULL),
(685, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5628643', '104.9187393', '6 mins', '1.4 km', 7, 'nd4eq4futo1478500500039', NULL, '2016-11-07 13:35:00', '2016-11-07 13:35:05', '11.5593615', '104.9051056', NULL, '2016-11-07 14:52:25', 2, NULL),
(694, 'hph9qfn2yk1477536765372', 'lqfpgbk1441467779762930', '11.557', '104.912', '11.5460771', '104.9137906', '5 mins', '1.2 km', 7, '9wr1cvw5kd1478507660061', NULL, '2016-11-07 15:34:20', '2016-11-07 15:34:31', '11.5460771', '104.9137906', NULL, '2016-11-07 15:34:53', 2, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
